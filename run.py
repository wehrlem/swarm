import os
import logging
import multiprocessing

import gunicorn.app.base

from dotenv import load_dotenv
from app import create_app, start


class Application(gunicorn.app.base.BaseApplication):
    def __init__(self, app, options=None):
        self.options = options or {}
        self.application = app
        super(Application, self).__init__()

    def load_config(self):
        # config = dict([(key, value) for key, value in iteritems(self.options)
        config = {key: value for key, value in self.options.items() if key in self.cfg.settings and value is not None}
        # for key, value in iteritems(config):
        for key, value in config.items():
            self.cfg.set(key.lower(), value)

    def load(self):
        return self.application


def init_app():
    return create_app()


def _post_fork(server=None, w=None):
    _config_logging()
    start()


def _config_logging():
    for logger in 'gunicorn.access', 'gunicorn.error':
        logging.getLogger(logger).propagate = True
        logging.getLogger(logger).handlers = []


if __name__ == '__main__':
    load_dotenv()
    app = init_app()
    env_name = os.getenv("ENV_NAME")
    default_workers = (multiprocessing.cpu_count() * 2) + 1
    opts = {
        "accesslog": os.getenv("ACCESS_LOG"),
        "access_log_format": os.getenv("ACCESS_LOG_FORMAT"),
        "bind": os.getenv("BIND"),
        "errorlog": os.getenv("ERROR_LOG"),
        "keepalive": os.getenv("KEEP_ALIVE"),
        "post_fork": _post_fork,
        "proc_name": os.getenv("APP_NAME"),
        "max_requests": os.getenv("MAX_REQUESTS"),
        "max_requests_jitter": os.getenv("MAX_REQUESTS_JITTER"),
        "worker_class": os.getenv("WORKER_CLASS"),
        "workers": os.getenv("WORKERS") or (1 if env_name == "LOCAL" else default_workers)
    }
    Application(app, opts).run()
