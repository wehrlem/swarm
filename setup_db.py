import json
import os
import app.models.helper.security as tokenHelper
import app.models.data.common.mongo_setup as mongo_setup

from app.models.data.platforms import Platform
from app.models.data.accounts import Account


if __name__ == '__main__':

    mongo_setup.global_init()

    # Import platforms fixtures
    path = 'app/fixtures/platforms.json'

    if os.path.exists(path):
        with open(path) as pfixtures:
            pdata = json.load(pfixtures)
            for x in pdata:
                pname = x['name']
                Platform.objects(name=pname).update(**x, upsert=True)
                print(f'{pname} - OK')

    username = os.getenv('ADMIN_USER')
    password = os.getenv('ADMIN_PASSWORD')
    token = os.getenv('TOKEN')

    # Create admin user
    Account.objects(username=username).update(
        password=password,
        token=tokenHelper.hash_token(token),
        is_active=True,
        is_superuser=True,
        upsert=True
    )
    print(f'Create "{username}" user - OK')
