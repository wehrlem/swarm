# falcon-boilerplate [![Build Status](https://travis-ci.org/admiralobvious/falcon-boilerplate.svg?branch=master)](https://travis-ci.org/admiralobvious/falcon-boilerplate)

A Python 3 boilerplate for the [Falcon](https://github.com/falconry/falcon) framework. Uses [gunicorn](https://github.com/benoitc/gunicorn) as the WSGI HTTP server and [meinheld](https://github.com/mopemope/meinheld) as the gunicorn worker. It also uses [Vyper](https://github.com/admiralobvious/vyper) for [12-factor](https://12factor.net/).


## Requirements
- MongoDB 4.2 Community Edition
- Python 3.6.8

## Install

```
$ git clone https://gitlab.com/wehrlem/swarm.git swarm
$ cd swarm
$ python -m venv env
$ source env/bin/activate
$ pip install -r dev_requirements.txt

/Applications/Python\ 3.6/Install\ Certificates.command

```

## Install MongoDB
* MacOS
```
>>> brew tap mongodb/brew
>>> brew install mongodb-community@4.2
```
Run MongoDB as service
```
>>> brew services start mongodb-community@4.2
```
Connect to MongoDB
```
>>> mongo
```

## Setup MongoDB
```
>>> python setup_db.py
```