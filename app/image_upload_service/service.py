import zmq
from threading import Thread
from app.adplatform.ad_manager import AdPlatformManager
from app.models.data.accounts import Account
from app.database.campaign import Campaign
from .image_msg_schema import ImageMsgSechema
from app.models.data.common import mongo_setup
from app import configure
import logging

ADDRESS = "127.0.0.1"
PORT = 5681


class ImageService(object):

    def __init__(self):

        self.context = zmq.Context()
        self.sock = self.context.socket(zmq.SUB)
        # self.setsockopt(zmq.LINGER, 0)

        # Define subscription and messages with prefix to accept.
        self.sock.setsockopt(zmq.SUBSCRIBE, b'image')

        # Setup logger
        configure()

        # Log message
        self.image_service_logger = logging.getLogger('image_service_logger')

    def processMessage(self, message):

        json0 = message.find('{')
        # topic = message[0:json0].strip()

        # Validate data
        msg = ImageMsgSechema().loads(message[json0:])

        user_id = msg.data["user"]
        campaigns = msg.data["campaigns"]
        platform = msg.data["platform"]

        user = Account.objects(id=user_id).first()
        camps = Campaign.objects(account_id=user).filter(id__in=campaigns)

        camp_ids = []
        for x in camps:
            camp_ids.append('{}.{}'.format(x.project_id, x.apartment.reference_id))

        # Log message
        self.image_service_logger.info("[{user}] {camp_ids}".format(user=user.username, camp_ids=camp_ids))

        if user:
            manager = AdPlatformManager()

            # Publish ads
            if platform:
                manager.publishToPlatform(user, platform)
            else:
                manager.publish(user)

            # Publish images
            manager.publishImages(camps, platform=platform)
        else:
            msg = "User with id: {user}".format(user=user_id)
            self.image_service_logger.error(msg)

    def run(self):

        # Connect to DB
        mongo_setup.global_init()

        address = "tcp://{}:{}".format(ADDRESS, PORT)

        self.sock.bind(address)

        print("Image upload service is running on {address}:{port}".format(address=ADDRESS, port=PORT))

        while True:
            try:
                #  Wait for next request from client
                message = self.sock.recv_string()
                # Thread(target=self.processMessage, args=[message]).start()
                self.processMessage(message)
            except Exception as ex:
                self.image_service_logger.error(ex)

        self.sock.unbind(address)
