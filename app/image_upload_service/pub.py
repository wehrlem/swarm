import zmq
import json
import time
import logging

ADDRESS = "127.0.0.1"
PORT = 5681
TOPIC = "image"


class ImagePublisher():

    def __init__(self):

        self.connected = False
        self.address = "tcp://{}:{}".format(ADDRESS, PORT)

    def run(self):
        if self.connected:
            return
        self.context = zmq.Context()
        self.sock = self.context.socket(zmq.PUB)
        self.sock.connect(self.address)
        self.connected = True

    def send(self, user, campaigns):
        try:
            # time.sleep(1)
            ids = []
            camp_ids = []
            for x in campaigns:
                ids.append(str(x.id))
                camp_ids.append('{}.{}'.format(x.project_id, x.apartment.reference_id))

            json_msg = {
                "user": str(user.id),
                "campaigns": ids
            }

            message = json.dumps(json_msg)
            topic_message = "{topic}{message}".format(topic=TOPIC, message=message)

            # Log message
            swarm_logger = logging.getLogger('image_publisher_logger')
            swarm_logger.info("[{user}] {camp_ids}".format(user=user.email, camp_ids=camp_ids))

            self.sock.send_string(topic_message)

        except Exception as e:
            print("error {}".format(e))

    def send_to_platform(self, user, campaign, platform):
        try:
            time.sleep(1)
            ids = [str(campaign.id)]
            camp_ids = [('{}.{}'.format(campaign.project_id, campaign.apartment.reference_id))]
            json_msg = {
                "user": str(user.id),
                "campaigns": ids,
                "platform": platform
            }
            message = json.dumps(json_msg)
            topic_message = "{topic}{message}".format(topic=TOPIC, message=message)

            # Log message
            swarm_logger = logging.getLogger('image_publisher_logger')
            swarm_logger.info("[{user}] {camp_ids}".format(user=user.email, camp_ids=camp_ids))

            self.sock.send_string(topic_message)
        except Exception as e:
            print("error {}".format(e))
