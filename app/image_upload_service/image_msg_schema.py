from marshmallow import Schema, fields


class ImageMsgSechema(Schema):
    user = fields.Str(required=True)
    campaigns = fields.List(fields.Str(), required=True)
    platform = fields.Str(required=False, missing=None)
