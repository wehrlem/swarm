import logging

import falcon

from app.config import parser, settings
from app.middleware import CrossDomain, JSONTranslator, AuthMiddleware, SerializerMiddleware
from app.resources import setup_routes
# from app.util.config import setup_vyper
from app.util.error import error_handler
from app.util.logging import setup_logging
import app.models.data.common.mongo_setup as mongo_setup

logger = logging.getLogger(__name__)


def configure():
    # logging.getLogger("vyper").setLevel(logging.WARNING)
    # setup_vyper(parser)
    setup_logging()


# def create_app(adcreator):
def create_app():
    configure()
    app = falcon.API(
        middleware=[
            CrossDomain(),
            JSONTranslator(),
            AuthMiddleware()
        ]
    )

    app.add_error_handler(Exception, error_handler)

    # Connect to DB
    mongo_setup.global_init()

    setup_routes(app)

    return app


def start():
    logger.info("Starting {}".format(settings.get("APP_NAME")))
    logger.info("Environment: {}".format(settings.get("ENV_NAME")))
