
from functools import wraps
import falcon


def has_superuser_decorator():
    def request_decorator(check_roles):
        @wraps(check_roles)
        def wrapper(self, req, resp, *args, **kwargs):
            user = req.context['user']
            if user.is_superuser:
                return check_roles(self, req, resp, *args, **kwargs)
            raise falcon.HTTPUnauthorized(
                title='401 Unauthorized',
                description='You do not have the appropriate role. Please contact the support',
                challenges=None)
        return wrapper
    return request_decorator
