from marshmallow import Schema, fields, pre_dump, post_dump, pre_load, post_load, validates_schema
from marshmallow import ValidationError
from app.models.data.platforms import Platform
from app.common.schema import ComposableDict


class PlatformBaseSchemas(Schema):
    agency_id = fields.Str()
    host = fields.Str()
    username = fields.Str()
    password = fields.Str()
    is_active = fields.Boolean()

    @pre_dump()
    def format_platforms(self, data, **kwargs):
        if isinstance(data.standard, str):
            return getattr(data, data.standard)


class UserPlatformInterfacesSchema(Schema):
    platforms = ComposableDict(fields.Nested(PlatformBaseSchemas))

    @validates_schema
    def validate_platforms(self, data):
        active_platforms = list(Platform.objects.get_platforms())
        load_platforms = list(data['platforms'].keys())
        res = list(set(load_platforms) - set(active_platforms))
        if len(res) > 0:
            raise ValidationError('Unknown platform {}. Must be one of {}'.format(res, active_platforms))

    @pre_load
    def preprocess(self, data, **kwargs):
        return {'platforms': data}

    @post_load
    def save_platforms(self, data, **kwargs):
        return data.get('platforms', {})

    @post_dump()
    def add_empty_data(self, data, **kwargs):
        active_platforms = list(Platform.objects.get_platforms())
        platforms_data = data.get('platforms', {})
        to_remove = list(set(platforms_data) - set(active_platforms))
        to_add = list(set(active_platforms) - set(platforms_data))
        for x in to_remove:
            platforms_data.pop(x)
        for x in to_add:
            platforms_data[x] = {
                'agency_id': '',
                'host': '',
                'username': '',
                'password': '',
                'is_active': False,
            }
        return platforms_data
