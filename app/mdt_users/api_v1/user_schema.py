import app.models.helper.security as tokenHelper

from marshmallow import Schema, fields, post_load, validates_schema, validates
from marshmallow import ValidationError
from app.models.data.accounts import Account
from app.models.data.platforms import Platform
from app.common.schema import ComposableDict


class FtpConfigSchema(Schema):
    agency_id = fields.Str()
    host = fields.Str()
    username = fields.Str()
    password = fields.Str()
    export_name = fields.Str()
    publisher_id = fields.Str()
    is_active = fields.Boolean()


class BasePlatformSchema(Schema):
    idx = fields.Nested(FtpConfigSchema)
    swissrets = fields.Nested(FtpConfigSchema)
    standard = fields.Str()
    active = fields.Boolean()


class BaseUserSchema(Schema):
    name = fields.String(required=True)
    username = fields.String(required=True)
    email = fields.Email(required=True)

    @validates('username')
    def validate_username(self, username):
        if Account.objects.filter(username=username).first():
            raise ValidationError(f'Username \'{username}\' exist.')


class SubUserSchema(BaseUserSchema):
    team_id = fields.Int(required=True)
    project_id = fields.String(required=True)
    name = fields.String(required=True)
    username = fields.String(required=True)
    email = fields.String(required=True)


class UserSchema(BaseUserSchema):
    webpage = fields.Str()
    password = fields.Str(required=True)
    platforms = ComposableDict(fields.Nested(BasePlatformSchema))

    @validates
    def validate_platforms(self, data):
        platforms = data.get('platforms', None)
        if platforms:
            try:
                load_platforms = list(platforms.keys())
                active_platforms = list(Platform.objects.get_platforms())
                res = list(set(load_platforms) - set(active_platforms))
                if len(res) > 0:
                    raise ValidationError('Unknown platform {}. Must be one of {}'.format(res, active_platforms))
            except Exception:
                pass

    @post_load
    def create_user(self, data, **kwargs):
        user = Account.objects(username=data["username"]).first()
        if not user:
            user = Account(**data)
            user.is_active = True
            token = tokenHelper.generate_token()
            hash_1 = tokenHelper.hash_token(token)
            user.token = hash_1
            user.save()
        else:
            user.update(**data)
            user.save()
        return user


class UserTokenSchema(Schema):
    username = fields.Str(required=True)
