
import falcon
import app.util.json as json
import app.models.helper.security as tokenHelper
from marshmallow import ValidationError

from webargs.falconparser import use_args
from app.models.data.accounts import Account, FtpConfig, BasePlatformConfig
from app.util.error import HTTPError
from app.common.decorators import has_superuser_decorator
from .user_schema import UserSchema, UserTokenSchema, SubUserSchema
from .user_platforms_schema import UserPlatformInterfacesSchema


class SubUserDoesNotExist(Exception):
    pass


class UserApi(object):

    @use_args(UserSchema(strict=True, many=False))
    def on_post(self, req, resp, args):
        """
            Create User
        """

        resp.status = falcon.HTTP_200
        resp.context["result"] = {
            "message": "ACK"
        }

    @use_args(SubUserSchema(strict=True, many=False))
    def on_post_subuser(self, req, resp, args):
        """
            Create subuser
        """
        master = req.context['user']
        subuser = Account.objects.filter(parrent=master, team=args['team_id'], project_id=args['project_id']).first()
        if subuser is None:
            user = Account(parrent=master, username=args['username'], project_id=args['project_id'], team=args['team_id'], email=args['email'], name=args["name"])
            user.is_active = True
            token = tokenHelper.generate_token()
            hash_1 = tokenHelper.hash_token(token)
            user.token = hash_1
            user.save()

        resp.status = falcon.HTTP_200
        resp.context["result"] = {
            "message": "ACK"
        }

    @use_args(UserTokenSchema(strict=True))
    def on_post_token(self, req, resp, args):
        """
            Get token
        """
        # Users
        resp.status = falcon.HTTP_200
        token = ""
        if 'username' in args:
            user = Account.objects(username=args["username"]).first()
            if user is not None:
                token = user.getToken()
        resp.context["result"] = {"token": token}


class UserPlatformsApi(object):

    def on_get(self, req, resp):
        """
            Get user platforms data
        """
        user = req.context['user']
        schema = UserPlatformInterfacesSchema()
        resp.status = falcon.HTTP_200
        resp.context["result"] = schema.dump(user).data

    def get_json(self, req):
        if req.content_length in (None, 0):
            return
        body = req.stream.read()
        if not body:
            raise HTTPError(400, "A valid JSON document is required.")
        try:
            return json.loads(str(body.decode("utf-8")))
        except (ValueError, UnicodeDecodeError):
            raise HTTPError(400, "Could not decode the request body. The JSON was incorrect or not encoded as UTF-8.")

    def save_platforms(self, user, platforms):
        for key, value in platforms.items():
            platform = user.platforms.get(key, None)
            if platform and isinstance(platform.standard, str):
                pdata = getattr(platform, platform.standard, None)
                pdata.agency_id = value.get('agency_id', None)
                pdata.host = value.get('host', None)
                pdata.username = value.get('username', None)
                pdata.password = value.get('password', None)
                pdata.is_active = value.get('is_active', False)
                platform.active = pdata.is_active
            else:
                cfg = FtpConfig()
                cfg.agency_id = value.get('agency_id', None)
                cfg.host = value.get('host', None)
                cfg.username = value.get('username', None)
                cfg.password = value.get('password', None)
                cfg.is_active = value.get('is_active', False)
                user.platforms[key] = BasePlatformConfig(idx=cfg, standard='idx', active=cfg.is_active)
        user.save()

    def on_post(self, req, resp):
        data = self.get_json(req)
        schema = UserPlatformInterfacesSchema(strict=False)
        try:
            result = schema.load(data).data
            user = req.context['user']
            self.save_platforms(user, result)
            resp.status = falcon.HTTP_200
            resp.context["result"] = {'message': 'ACK'}
        except Exception as ex:
            resp.status = falcon.HTTP_401
            if '_schema' in ex.messages:
                resp.context["result"] = {'title': ex.messages['_schema']}
            else:
                resp.context["result"] = {'title': ex.messages}


class SubUserPlatformsApi(UserPlatformsApi):

    def get_user(self, req, project_id, team_id):
        master = req.context['user']
        subuser = Account.objects.filter(parrent=master, project_id=project_id, team=team_id).first()
        if subuser is None:
            raise SubUserDoesNotExist(f'Team \'{team_id}\' dont exist.')
        return subuser

    def on_get(self, req, resp, project_id, team_id):
        """
            Get Subuser platforms data
        """
        try:
            user = self.get_user(req, project_id, team_id)
            schema = UserPlatformInterfacesSchema()
            resp.status = falcon.HTTP_200
            resp.context["result"] = schema.dump(user).data
        except SubUserDoesNotExist:
            resp.status = falcon.HTTP_200
            resp.context["result"] = {'message': -1}
        except Exception as ex:
            resp.status = falcon.HTTP_401
            resp.context["result"] = {'title': ex.messages}

    def on_post(self, req, resp, project_id, team_id):
        data = self.get_json(req)
        user_schema = UserPlatformInterfacesSchema(strict=False)
        try:
            result = user_schema.load(data).data
            user = self.get_user(req, project_id, team_id)
            self.save_platforms(user, result)
            resp.status = falcon.HTTP_200
            resp.context["result"] = {'message': 'ACK'}
        except SubUserDoesNotExist:
            resp.status = falcon.HTTP_200
            resp.context["result"] = {'message': -1}
        except Exception as ex:
            resp.status = falcon.HTTP_401
            if '_schema' in ex.messages:
                resp.context["result"] = {'title': ex.messages['_schema']}
            else:
                resp.context["result"] = {'title': ex.messages}
