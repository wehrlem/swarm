
# from app.util.error import HTTPError
# from falcon_auth import FalconAuthMiddleware
# import falcon
from falcon_auth import TokenAuthBackend
import app.database as svc
from app.image_upload_service.pub import ImagePublisher

user_loader = svc.users.find_user_by_token
auth_backend = TokenAuthBackend(user_loader)


class AuthMiddleware(object):

    def __init__(self):
        self.image_pub = ImagePublisher()

    def process_request(self, req, resp):
        user = auth_backend.authenticate(req, resp, "")
        req.context['user'] = user
        self.image_pub.run()
        req.context['image_pub'] = self.image_pub
