import falcon.status_codes as status
from marshmallow import ValidationError
from app.util.error import HTTPError


class SerializerMiddleware:
    def process_resource(self, req, resp, resource, params):
        req_data = req.context.get('request') or req.params
        try:
            serializer = resource.serializers[req.method.lower()]
        except (AttributeError, IndexError, KeyError):
            return
        else:
            try:
                req.context['serializer'] = serializer().load(data=req_data)
            except ValidationError as err:
                raise HTTPError(status=422, errors=err.messages)
