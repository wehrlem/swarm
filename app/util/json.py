try:
    import simplejson as json
except ImportError:
    import json

import datetime


def default(o):
    if isinstance(o, (datetime.date, datetime.datetime)):
        return o.isoformat()


def loads(data):
    return json.loads(data)


def dumps(data):
    return json.dumps(data, default=default)
