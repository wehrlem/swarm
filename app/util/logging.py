import logging
import logging.config
import os

from app.config import settings

BASE_LOG_DIR = os.path.join(os.path.dirname(os.path.dirname(os.path.abspath(__file__))), "log")

if not os.path.exists(BASE_LOG_DIR):
    os.makedirs(BASE_LOG_DIR)


class GunicornFilter(logging.Filter):
    def filter(self, record):
        # workaround to remove the duplicate access log
        if '"- - HTTP/1.0" 0 0' in record.msg:
            return False
        else:
            return True


def setup_logging():
    config = {
        "version": 1,
        "disable_existing_loggers": False,
        "filters": {
            "gunicorn_filter": {
                "()": GunicornFilter
            }
        },
        "formatters": {
            "standard": {
                "format": "[%(asctime)s] [%(process)d] [%(levelname)s] [%(name)s] %(message)s",
                "datefmt": "%Y-%m-%d %H:%M:%S %z"
            }
        },
        "handlers": {
            "console": {
                "formatter": "standard",
                "class": "logging.StreamHandler",
                "filters": ["gunicorn_filter"],
            },
            'image_publisher_info_logfile': {
                'level': 'INFO',
                'class': 'logging.FileHandler',
                'filename': os.path.join(BASE_LOG_DIR, 'image_publisher_info.log'),
                'formatter': 'standard'
            },
            'image_service_info_logfile': {
                'level': 'INFO',
                'class': 'logging.FileHandler',
                'filename': os.path.join(BASE_LOG_DIR, 'image_service_info.log'),
                'formatter': 'standard'
            },
            'image_service_error_logfile': {
                'level': 'ERROR',
                'class': 'logging.FileHandler',
                'filename': os.path.join(BASE_LOG_DIR, 'image_service_error.log'),
                'formatter': 'standard'
            },
            'admanager_info_logfile': {
                'level': 'INFO',
                'class': 'logging.FileHandler',
                'filename': os.path.join(BASE_LOG_DIR, 'admanager_info.log'),
                'formatter': 'standard'
            },
            'admanager_error_logfile': {
                'level': 'ERROR',
                'class': 'logging.FileHandler',
                'filename': os.path.join(BASE_LOG_DIR, 'admanager_error.log'),
                'formatter': 'standard'
            },
            'admanager_warning_logfile': {
                'level': 'WARNING',
                'class': 'logging.FileHandler',
                'filename': os.path.join(BASE_LOG_DIR, 'admanager_warning.log'),
                'formatter': 'standard'
            },
            'ads_info_logfile': {
                'level': 'INFO',
                'class': 'logging.FileHandler',
                'filename': os.path.join(BASE_LOG_DIR, 'ads_info.log'),
                'formatter': 'standard'
            },
        },
        "loggers": {
            "": {
                "handlers": ['console'],
                "level": "INFO",
            },
            "image_publisher_logger": {
                'handlers': ['image_publisher_info_logfile'],
                'level': 'INFO',
                'propagate': True,
            },
            "image_service_logger": {
                'handlers': ['image_service_info_logfile', 'image_service_error_logfile'],
                'level': 'INFO',
                'propagate': True,
            },
            "admanager_info_logger": {
                'handlers': ['admanager_info_logfile'],
                'level': 'INFO',
                'propagate': True,
            },
            "admanager_error_logger": {
                'handlers': ['admanager_error_logfile'],
                'level': 'ERROR',
                'propagate': True,
            },
            "admanager_warning_logger": {
                'handlers': ['admanager_warning_logfile'],
                'level': 'WARNING',
                'propagate': True,
            },
            "ads_logger": {
                'handlers': ['ads_info_logfile'],
                'level': 'INFO',
                'propagate': True,
            },
        }
    }

    logging.config.dictConfig(config)
