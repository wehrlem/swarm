import logging

import falcon
import falcon.status_codes as status_codes

logger = logging.getLogger(__name__)


class HTTPError(falcon.HTTPError):
    def __init__(self, status, errors=None, **kwargs):
        self.errors = errors
        http_status = "HTTP_{}".format(status)
        if hasattr(status_codes, http_status):
            title = getattr(status_codes, http_status)
        else:
            raise ValueError("Status code '{}' does not exist!".format(status))
        super(HTTPError, self).__init__(title, **kwargs)

    def to_dict(self, *args, **kwargs):
        """
        Override `falcon.HTTPError` to include error messages in responses.
        """

        ret = super().to_dict(*args, **kwargs)

        if self.errors is not None:
            ret['errors'] = self.errors

        return ret


def error_handler(ex, req, resp, params):
    if not isinstance(ex, falcon.HTTPError):
        logger.exception("Unhandled error while processing request: {}".format(ex))
        raise HTTPError(500, str(ex))
    else:
        raise ex
