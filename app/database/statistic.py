from app.models.data.statistic import Statistic
from app.models.data.campaigns import Campaign


def get_statistic(camp: Campaign) -> Statistic:

    stat = Statistic.objects(campaign=camp).first()
    if stat is None:
        stat = Statistic(campaign=camp)
    return stat
