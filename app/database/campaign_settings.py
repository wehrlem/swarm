from app.models.data.campaign_settings import CampaignSettings
from app.models.data.campaign_settings import FacebookCampaignSetting


# Get Default Settings
def get_default_settings():
    settings = CampaignSettings.objects(name="DEFAULT").first()
    return settings


# Get Facebook Settings
def get_facebook_settings_default() -> FacebookCampaignSetting:
    fsett = FacebookCampaignSetting.objects(default=True).first()
    return fsett