from datetime import datetime
from dateutil import parser
from typing import List
import hashlib

# Campaign data
from app.models.data.campaigns import Campaign
from app.models.data.budgets import Budget
from app.models.data.budgetdistributions import BudgetDistribution
from app.models.data.targetings import Targeting
from app.models.data.locations import Location
# from ..geometries import Geometry
from app.models.data.address_components import AddressComponent
from app.models.data.googleads import GoogleAd
from app.models.data.instagramads import InstagramAd
from app.models.data.facebookads import FacebookAd


def set_budget(json) -> Budget:

    bdg = Budget()
    data = json
    bdg.currency = data['currency']
    bdg.brutto_budget = float(data['brutto_budget'])
    bdg.app_percentage = float(data['app_percentage'])
    bdg.budget_optimization = bool(data['budget_optimization'])

    platforms = ['google', 'facebook', 'instagram']
    if 'budget_distribution' in data:
        bdg.budget_distribution = []
        for platform in platforms:
            if platform in data['budget_distribution']:
                bdg_dist_data = data['budget_distribution'][platform]
                for tmp in bdg_dist_data:
                    bdgDistr = BudgetDistribution()
                    bdgDistr.netto_price = float(tmp['netto_price'])
                    bdgDistr.ad_type = tmp['ad_type']
                    bdgDistr.percentage = float(tmp['percentage'])
                    bdgDistr.platform = platform
                    bdg.budget_distribution.append(bdgDistr)
    return bdg


def get_locations(locations):
    locs = {}
    location_types = ['locality', 'country', 'postal_code']
    for location in locations:
        loc = Location()
        loc.address_components = []
        address = []
        for loc_type in location_types:
            if loc_type in location:
                adr_comp = AddressComponent()
                adr_comp.long_name = location[loc_type]
                adr_comp.types.append(loc_type)
                loc.address_components.append(adr_comp)
                address.append(location[loc_type])
        loc.formatted_address = ", ".join(address)
        hash_object = hashlib.md5(loc.formatted_address.encode())
        loc.place_id = hash_object.hexdigest()
        # Add new location
        locs[loc.place_id] = loc
    return locs


def update_location_targeting(camp: Campaign, locations):

    # Get Location dict
    locs = get_locations(locations)

    # Old IDs
    old_ids = [x.place_id for x in camp.targeting.locations]
    new_ids = [x for x in locs.keys()]

    # Changes
    loc_to_remove = list(set(old_ids) - set(new_ids))
    loc_to_add = list(set(new_ids) - set(old_ids))

    # Add location to database
    for id in loc_to_add:
        camp.targeting.locations.append(locs[id])

    # Remove location form database
    for id in loc_to_remove:
        camp.targeting.locations.filter(place_id=id).delete()


def update_language_targeting(camp: Campaign, languages):

    old = camp.targeting.languages
    to_remove = list(set(old) - set(languages))
    to_add = list(set(languages) - set(old))

    # Add
    for lang in to_add:
        camp.targeting.languages.append(lang)
    # Remove
    for lang in to_remove:
        camp.targeting.languages.remove(lang)


def update_keywords_targeting(camp: Campaign, keywords):

    old = camp.targeting.keywords
    to_remove = list(set(old) - set(keywords))
    to_add = list(set(keywords) - set(old))

    # Add
    for key in to_add:
        camp.targeting.keywords.append(key)
    # Remove
    for key in to_remove:
        camp.targeting.keywords.remove(key)


def set_targeting(camp: Campaign, targeting_msg) -> Targeting:

    # Location Targeting
    if 'locations' in targeting_msg:
        update_location_targeting(camp, targeting_msg['locations'])
    else:
        update_location_targeting(camp, [])

    # Languages
    if 'languages' in targeting_msg:
        update_language_targeting(camp, targeting_msg['languages'])
    else:
        update_language_targeting(camp, [])

    # Keywords
    if 'keywords' in targeting_msg:
        update_keywords_targeting(camp, targeting_msg['keywords'])
    else:
        update_keywords_targeting(camp, [])


def set_googleads(ads: List[GoogleAd], data):

    for ad_data in data:
        # Find ad by type
        if 'ad_type' in ad_data:
            _ad_type = ad_data['ad_type'].lower()
            ad = ads.filter(type=_ad_type).first()
            # If ad type not exists create new
            if ad is None:
                ad = GoogleAd()
                ads.append(ad)

            ad.type = ad_data['ad_type'].lower()
            ad.url = ad_data['url']

            ad.descriptions = []
            for x in ad_data['descriptions']:
                ad.descriptions.append(x)

            if ad.type == "search":
                ad.headline1 = ad_data['headline1']
                ad.headline2 = ad_data['headline2']
                ad.headline3 = ad_data['headline3']

            elif ad.type == "display":

                ad.long_headline = ad_data['long_headline']

                ad.marketing_images = []
                for x in ad_data['marketing_images']:
                    ad.marketing_images.append(x)

                ad.short_headlines = []
                for x in ad_data['short_headlines']:
                    ad.short_headlines.append(x)

                ad.square_marketing_images = []
                for x in ad_data['square_marketing_images']:
                    ad.square_marketing_images.append(x)

                ad.logo_images = []
                for x in ad_data['logo_images']:
                    ad.logo_images.append(x)


def set_facebookads(ads: List[FacebookAd], data):

    for ad_data in data:
        ad = None
        # Find ad by type
        if 'ad_type' in ad_data:
            ad = ads.filter(type=ad_data["ad_type"]).first()

        # If ad type not exists create new
        if ad is None:
            ad = FacebookAd()
            ads.append(ad)

        ad.type = ad_data['ad_type'].lower()
        ad.url = ad_data['url']

        ad.descriptions = []
        for x in ad_data['descriptions']:
            ad.descriptions.append(x)

        ad.marketing_images = []
        for x in ad_data['marketing_images']:
            ad.marketing_images.append(x)

        ad.title = ad_data['title']
        ad.message = ad_data['message']


def set_instagramads(ads: List[InstagramAd], data):

    for ad_data in data:
        ad = None
        # Find ad by type
        if 'ad_type' in ad_data:
            ad = ads.filter(type=ad_data["ad_type"]).first()

        # If ad type not exists create new
        if ad is None:
            ad = InstagramAd()
            ads.append(ad)

        ad.type = ad_data['ad_type'].lower()
        ad.url = ad_data['url']

        ad.descriptions = []
        for x in ad_data['descriptions']:
            ad.descriptions.append(x)

        ad.marketing_images = []
        for x in ad_data['marketing_images']:
            ad.marketing_images.append(x)

        ad.title = ad_data['title']
        ad.message = ad_data['message']


def load_json(camp: Campaign, message):

    if 'name' in message:
        camp.name = message['name']
    if 'description' in message:
        camp.description = message['description']
    if 'start_date' in message:
        camp.start_date = parser.parse(message['start_date'])
    if 'end_date' in message:
        camp.end_date = parser.parse(message['end_date'])
    camp.update_date = datetime.utcnow()
    if 'platforms' in message:
        camp.platforms = []
        for platform in message['platforms']:
            camp.platforms.append(platform)
    else:
        camp.platforms = []
    if 'budget' in message:
        camp.budget = set_budget(message['budget'])
    if 'targeting' in message:
        set_targeting(camp, message['targeting'])
    if 'ads' in message:
        ads_json = message['ads']
        # Facebook Ads
        if 'facebook' in ads_json and 'facebook' in camp.platforms:
            set_facebookads(camp.facebookads, ads_json['facebook'])
        # Google Ads
        if 'google' in ads_json and 'google' in camp.platforms:
            set_googleads(camp.googleads, ads_json['google'])
        # Instagram Ads
        if 'instagram' in ads_json and 'instagram' in camp.platforms:
            set_instagramads(camp.instagramads, ads_json['instagram'])
