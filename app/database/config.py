from app.models.data.config import Config


# Get default config
def get_default_config() -> Config:
    return Config.objects(name="DEFAULT").first()
