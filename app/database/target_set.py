from app.models.data.target_sets import TargetSet


def get_targetset_by_name(name: str) -> TargetSet:
    return TargetSet.objects(name=name).first()


def get_targetset_by_id(id: str) -> TargetSet:
    return TargetSet.objects(id=id).first()


def get_targetset_default() -> TargetSet:
    return TargetSet.objects(default=True).first()
