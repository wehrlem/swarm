# Database Models
from app.models.data.cp_config import CPConfig
from app.models.data.common.configs import Status


# Get config by campaign Id
def get_config(campaign_id: str) -> CPConfig:
    return  CampaignPlatformConfig.objects(campaign=campaign_id).first()

