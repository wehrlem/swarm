from app.models.data.campaigns import Campaign
from app.models.data.accounts import Account
from app.models.data.target_sets import TargetSet
from app.models.data.campaign_settings import FacebookCampaignSetting
from app.models.data.cp_config import CPConfig
from .campaign_load_json import load_json
from . import campaign_settings as cp_settings
from mongoengine.queryset.visitor import Q
from datetime import datetime


def get_campaign(id: str) -> Campaign:
    """
        Get all campaigns
    """
    return Campaign.objects(id=id).first()

def get_campaign_by_id(user_id, id) -> Campaign:
    """
        Get campaign by AccountID and CampaignID
    """
    return Campaign.objects(Q(id=id) & Q(account_id=user_id)).first()


def get_campaigns_by_user(user_id) -> Campaign:
    """
        Get campaigns by userID
    """
    return Campaign.objects.filter(account_id=user_id)


def get_active_campaigns_by_user(user_id) -> Campaign:
    """
        Get campaigns by userID
    """
    return Campaign.objects(account_id=user_id).filter(platforms__end_date<datetime.now())


def get_campaign_by_project(user_id, project_id, search=None):
    """
        Get campaigns by userID and project
    """
    queryset = Campaign.objects(Q(account_id=user_id) & Q(project_id=project_id))
    if search is not None:
        import re
        regex = re.compile('[^a-zA-Z0-9-]')
        search = regex.sub('', search)
        return queryset.filter(name__contains="{search}".format(search=search))
    return queryset


def remove_campaign(user_id, campaign_id):
    """
        Remove campaign by ID
    """
    camp = get_campaign_by_id(user_id, campaign_id)
    if camp is None:
        return False
    camp.delete()
    return True

# Upsert Campaign
def upsert_campaign(message, user: Account):

    new_campaign = False
    camp = None

    # Get Campaign
    if 'id' in message and message['id'] not in ("", None):
        camp = get_campaign(message['id'])

    # If campaign not exists
    if camp is None:
        camp = Campaign()
        new_campaign = True

    # Get default settings
    if camp.campaign_settings is None:
        camp.campaign_settings = cp_settings.get_default_settings()

    # Load JSON to Campaign
    load_json(camp, message)

    # Save campaign
    camp.save()

    # Add campaign to user
    if new_campaign is True:
        user.campaigns.append(camp)
        user.save()

    return camp, new_campaign


def get_campaigns(user: Account):

    try:
        res = []
        for campaign in user.campaigns:
            tmp = {
                "campaign_id": str(campaign.id),
                "name": campaign.name,
                "start_date": campaign.start_date,
                "end_date": campaign.end_date,
                "create_date": campaign.create_date,
                "update_date": campaign.update_date,
                "platforms": campaign.platforms,
            }
            if campaign.budget is not None:
                tmp["budget"] = campaign.budget.netto_budget
            res.append(tmp)
        return res

    except Exception:
        raise Exception("Get Campaigns error")
