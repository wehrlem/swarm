from app.models.data.google import GoogleLang
from difflib import SequenceMatcher
import operator
MIN_SIMILARITY_INDEX = 0.8


def GetGoogleLangId(lang_name, languages: GoogleLang):
    lang = __GetBestRatio(lang_name, languages)
    return lang


def GetGoogleLangIds(lang_list):

    try:
        source = GoogleLang.objects.all()
        response = {}
        for lang in lang_list:
            tmp = __GetBestRatio(lang, source)
            if tmp is not None:
                response[lang] = tmp.lang_id
        return response
    except Exception as e:
        print("No database {}".format(e))
        return None


def __Ratio(lang, language: GoogleLang):

    if(language.name is not None and lang is not None):
        return SequenceMatcher(None, lang, language.name).ratio()
    else:
        return float(0)


def __GetBestRatio(lang, languages)->GoogleLang:

    results = []
    for language in languages:
        ratio = __Ratio(lang, language)
        results.append(ratio)
    max_index, max_value = max(enumerate(results), key=operator.itemgetter(1))
    if max_value > MIN_SIMILARITY_INDEX:
        return languages[max_index]
    else:
        return None
