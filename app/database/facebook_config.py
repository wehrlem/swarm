from app.models.data.facebook_config import FacebookConfig


# Get default config
def get_default_config():
    return FacebookConfig.objects(default=True, active=True).first()
