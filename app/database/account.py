from app.models.data.accounts import Account
import app.models.helper.security as tokenHelper
import falcon


def create_account(name: str, email: str, webpage: str) -> Account:

    account = Account()
    account.name = name
    account.email = email
    account.webpage = webpage
    account.save()
    return account


def get_platform_connection_string(account: Account, platform_name):

    response = {}
    pdata = None
    if platform_name == "newhome":
        pdata = account.platforms.newhome.swissrets

    assert pdata is not None
    assert pdata.host is not None
    assert pdata.username is not None
    assert pdata.password is not None
    assert pdata.export_name is not None
    assert pdata.agency_id is not None

    response = {
        "host": pdata.host,
        "user": pdata.username,
        "password": pdata.password,
        "export_name": pdata.export_name,
        "organisation_id": pdata.agency_id,
        "publisher_id": pdata.publisher_id,
        "platform_name": platform_name
    }

    if response == {}:
        raise falcon.HTTPBadRequest("Platform was not cofigured for USER {}".formt(account.id))

    return response


def find_user_by_email(email: str) -> Account:
    ac = Account.objects(email=email).first()
    return ac


def find_user_by_username_password(username: str, password: str) -> Account:
    ac = Account.objects(username=username, password=password, is_active=True).first()
    return ac


def activate_user(user: Account):
    user.is_active = True
    user.save()


def find_user_by_token(token: str) -> Account:

    hash_1 = tokenHelper.hash_token(token)
    # print(hash_1)
    ac = Account.objects(token=hash_1, is_active=True).first()
    return ac
