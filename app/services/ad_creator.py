import json
import os

# Ads creator
from app.adplatform.google import GoogleAdCreator
from app.adplatform.facebook.facebook_ad_manager import FacebookAdManager

# Database
import app.database as db
from app.models.data.accounts import Account
from app.models.data.common.configs import Platforms
from app.models.data.config import Config


class AdCreator(object):

    def __init__(self):
        self.config = None
        self.googleCreator = None
        self.facebook: FacebookAdManager = None

    def initialize(self):

        self._set_config()

        if not self.googleCreator:
            self.googleCreator = GoogleAdCreator(self.config)
        if not self.facebook:
            self.facebook = FacebookAdManager(self.config)

    def CreateAds(self, request, user: Account):

        self.initialize()

        # Upsert campaign
        camp, new_camp = db.campaign.upsert_campaign(request, user)

        # Google Ads
        if Platforms.GOOGLE in camp.platforms:
            self.googleCreator.CreateTextAds(camp, new_camp)

        if Platforms.FACEBOOK in camp.platforms:
            self.facebook.handle_ad_request(camp, user)

        # Update changes in campaign platform mapper
        camp.save()

        # Return response
        print("Campaign ID: {}".format(camp.id))

        return camp

    def StartCampaign(self, campaign_id):

        self.initialize()

        camp = db.campaign.get_campaign(campaign_id)
        # Start Google Campaign
        if Platforms.GOOGLE in camp.platforms:
            self.googleCreator.StartCampaign(camp.config.google.campaign_id)
        pass

    def StopCampaign(self, campaing_id):
        # Get campaign by id
        camp = db.campaign.get_campaign(campaing_id)
        # Stop Google campaign
        self.googleCreator.StopCampaign(camp.config.google.campaign_id)

    def GetCampaigns(self, user: Account):
        # Get campaigns
        camp_list = db.campaign.get_campaigns(user)
        return camp_list

    def GetStatistic(self, campaign_id):

        self.initialize()

        # Get Campaign
        camp = db.campaign.get_campaign(campaign_id)

        # Campaign not exist
        if camp is None:
            raise Exception('Campaign with id {} does not exists'.format(campaign_id))

        # Get Statistic from DB
        stat = db.statistics.get_statistic(camp)

        # Get Google Statistics
        if Platforms.GOOGLE in camp.platforms:
            try:
                self.googleCreator.GetCampaignStatistic(camp, stat)
            except Exception:
                raise Exception('Google Statistic error')

        # Get Facebook Statistics
        if Platforms.FACEBOOK in camp.platforms:
            try:
                self.facebook.get_campaign_stats(camp, stat)
            except Exception:
                raise Exception('Facebook Statistic error')

        # Save Statistics
        stat.save()

        return stat

    def loadConfigFromFile(self):

        with open("{0}/config/config_service.local.json".format(os.getcwd())) as config_file:
            data = json.load(config_file)

        return data

    def _set_config(self):
        if not self.config:
            config = db.config.get_default_config()
            self.config = config
