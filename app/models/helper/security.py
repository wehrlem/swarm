import secrets
import hashlib
import binascii


def hash_token(token):
    """Hash a token for storing."""
    # salt = hashlib.sha256(os.urandom(60)).hexdigest().encode('ascii')
    salt = hashlib.sha256('1WkkeC8Jl6gTnn6FiqCBiEzT1U4KmQM3ZcFYu-AJVNw'.encode()).hexdigest().encode('ascii')
    pwdhash = hashlib.pbkdf2_hmac('sha256', token.encode('utf-8'), salt, 100000)
    pwdhash = binascii.hexlify(pwdhash)

    return (salt + pwdhash).decode('ascii')


def verify_token(stored_token, provided_token):
    """Verify a stored token against one provided by user"""
    salt = stored_token[:64]
    stored_token = stored_token[64:]
    pwdhash = hashlib.pbkdf2_hmac('sha256', provided_token.encode('utf-8'), salt.encode('ascii'), 100000)
    pwdhash = binascii.hexlify(pwdhash).decode('ascii')

    return pwdhash == stored_token


def generate_token():
    """Generating url safe tokens"""

    return secrets.token_urlsafe()
