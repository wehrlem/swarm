class AdsException(Exception):
    def __init__(self, module, message):
        self.module = module
        self.message = message
