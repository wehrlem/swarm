import mongoengine


class FacebookAccessConfig(mongoengine.EmbeddedDocument):

    access_token = mongoengine.StringField()
    ad_account_id = mongoengine.StringField()
    app_secret = mongoengine.StringField()
    app_id = mongoengine.StringField()


class GoogleConfig(mongoengine.EmbeddedDocument):

    developer_token = mongoengine.StringField()
    client_customer_id = mongoengine.StringField()
    client_id = mongoengine.StringField()
    client_secret = mongoengine.StringField()
    refresh_token = mongoengine.StringField()


class Config(mongoengine.Document):

    name = mongoengine.StringField()
    google: GoogleConfig = mongoengine.EmbeddedDocumentField(GoogleConfig, default=GoogleConfig())
    facebook: FacebookAccessConfig = mongoengine.EmbeddedDocumentField(FacebookAccessConfig, default=FacebookAccessConfig())

    meta = {
        'db_alias': 'core',
        'collection': 'config'
    }

