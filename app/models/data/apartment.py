import mongoengine

categories_choices = ('apartment', 'atelier', 'attic-flat', 'bar', 'cafe', 'car-repair-shop', 'castle', 'chalet', 'earth-sheltered-dwelling', 'detached-house', 'doctors-office', 'duplex-house', 'farm-house', 'furnished-flat', 'granny-flat', 'hobby-room', 'hotel', 'loft', 'maisonette', 'multiplex-house', 'office', 'one-room-flat', 'restaurant', 'retail-location', 'roof-flat', 'row-house', 'rustico', 'single-room', 'stoeckli', 'stepped-flat', 'stepped-house', 'villa', 'workshop', 'parking-space', 'plot', 'house', 'other', 'craft')
# type_categories = ('apartment', 'atelier', 'attic-flat', 'bar', 'cafe', 'car-repair-shop', 'castle', 'chalet', 'earth-sheltered-dwelling', 'detached-house', 'doctors-office', 'duplex-house', 'farm-house', 'furnished-flat', 'granny-flat', 'hobby-room', 'hotel', 'loft', 'maisonette', 'multiplex-house', 'office', 'one-room-flat', 'restaurant', 'retail-location', 'roof-flat', 'row-house', 'rustico', 'single-room', 'stoeckli', 'stepped-flat', 'stepped-house', 'villa', 'workshop')
type_applicable = ('applies', 'does-not-apply', 'unknown')
type_development = ('full', 'partial', 'undeveloped')
type_heating_generation = ('district', 'electricity', 'gas', 'geothermal-probe', 'heatpump-air-and-water', 'heatpump', 'oil', 'photovoltaics', 'solar-thermal', 'wood-pellet', 'wood')
type_heating_distribution = ('radiator', 'floor')
# rent_interval = ('onetime', 'day', 'week', 'month', 'year')
price_interval = ('onetime', 'day', 'week', 'month', 'year')
price_referring = ('all', 'm2', 'km2')
deposit_type = ('bank-guarantee', 'deposit-guarantee')
additional_offer_type = ('parking-exterior-space', 'arking-carport', 'parking-garage', 'parking-garage-connected', 'parking-garage-box', 'parking-garage-underground', 'parking-house', 'parking-duplex', 'parking-double-garage', 'room-workroom', 'room-storage-basement')
type_utilization = ('agricultural', 'commercial', 'construction', 'gastronomy', 'industrial', 'investment', 'parking', 'residential', 'storage', 'vacation')


class Availability(mongoengine.EmbeddedDocument):
    CHOICES = ('active', 'private', 'reference', 'reserved', 'taken')
    expiration = mongoengine.DateTimeField()
    start = mongoengine.DateTimeField()
    value = mongoengine.StringField()


class Bfs(mongoengine.EmbeddedDocument):
    egid = mongoengine.StringField()
    ewid = mongoengine.StringField()


class CustomCaracteristic(mongoengine.EmbeddedDocument):
    key = mongoengine.StringField(required=True)
    value = mongoengine.StringField(required=True)


class Characteristics(mongoengine.EmbeddedDocument):

    area_bwf = mongoengine.DecimalField(min_value=0)
    area_nwf = mongoengine.DecimalField(min_value=0)
    area_property_land = mongoengine.DecimalField(min_value=0)
    area_sia_aff = mongoengine.DecimalField(min_value=0)
    area_sia_agf = mongoengine.DecimalField(min_value=0)
    area_sia_akf = mongoengine.DecimalField(min_value=0)
    area_sia_akfn = mongoengine.DecimalField(min_value=0)
    area_sia_akft = mongoengine.DecimalField(min_value=0)
    area_sia_anf = mongoengine.DecimalField(min_value=0)
    area_sia_angf = mongoengine.DecimalField(min_value=0)
    area_sia_avf = mongoengine.DecimalField(min_value=0)
    area_sia_buf = mongoengine.DecimalField(min_value=0)
    area_sia_ff = mongoengine.DecimalField(min_value=0)
    area_sia_gf = mongoengine.DecimalField(min_value=0)
    area_sia_ggf = mongoengine.DecimalField(min_value=0)
    area_sia_gsf = mongoengine.DecimalField(min_value=0)
    area_sia_hnf = mongoengine.DecimalField(min_value=0)
    area_sia_kf = mongoengine.DecimalField(min_value=0)
    area_sia_kfn = mongoengine.DecimalField(min_value=0)
    area_sia_kft = mongoengine.DecimalField(min_value=0)
    area_sia_nf = mongoengine.DecimalField(min_value=0)
    area_sia_ngf = mongoengine.DecimalField(min_value=0)
    area_sia_nnf = mongoengine.DecimalField(min_value=0)
    area_sia_uf = mongoengine.DecimalField(min_value=0)
    area_sia_uuf = mongoengine.DecimalField(min_value=0)
    area_sia_vf = mongoengine.DecimalField(min_value=0)
    are_pets_allowed = mongoengine.StringField(choices=type_applicable)
    ceiling_height = mongoengine.DecimalField(min_value=0)
    crane_capacity = mongoengine.DecimalField(min_value=0)
    elevator_load = mongoengine.DecimalField(min_value=0)
    floor = mongoengine.IntField(null=True)
    floor_load = mongoengine.DecimalField(min_value=0)
    gross_premium = mongoengine.DecimalField(min_value=0, max_value=100)
    hall_height = mongoengine.DecimalField(min_value=0)

    has_attic = mongoengine.StringField(choices=type_applicable)
    has_balcony = mongoengine.StringField(choices=type_applicable)
    has_building_law_restrictions = mongoengine.StringField(choices=type_applicable)
    has_cable_tv = mongoengine.StringField(choices=type_applicable)
    has_car_port = mongoengine.StringField(choices=type_applicable)
    has_car_port_double = mongoengine.StringField(choices=type_applicable)
    has_cellar = mongoengine.StringField(choices=type_applicable)
    has_charging_station = mongoengine.StringField(choices=type_applicable)
    has_connected_building_land = mongoengine.StringField(choices=type_applicable)
    has_demolition_property = mongoengine.StringField(choices=type_applicable)
    has_dishwasher = mongoengine.StringField(choices=type_applicable)
    has_elevator = mongoengine.StringField(choices=type_applicable)
    has_fireplace = mongoengine.StringField(choices=type_applicable)
    has_flat_sharing_community = mongoengine.StringField(choices=type_applicable)
    has_foreign_quota = mongoengine.StringField(choices=type_applicable)
    has_garage = mongoengine.StringField(choices=type_applicable)
    has_garage_double = mongoengine.StringField(choices=type_applicable)
    has_garage_underground = mongoengine.StringField(choices=type_applicable)
    has_garden_shed = mongoengine.StringField(choices=type_applicable)
    has_lake_view = mongoengine.StringField(choices=type_applicable)
    has_lifting_platform = mongoengine.StringField(choices=type_applicable)
    has_mountain_view = mongoengine.StringField(choices=type_applicable)
    has_nice_view = mongoengine.StringField(choices=type_applicable)
    has_parking = mongoengine.StringField(choices=type_applicable)
    has_photovoltaic = mongoengine.StringField(choices=type_applicable)
    has_playground = mongoengine.StringField(choices=type_applicable)
    has_ramp = mongoengine.StringField(choices=type_applicable)
    has_steamer = mongoengine.StringField(choices=type_applicable)
    has_store_room = mongoengine.StringField(choices=type_applicable)
    has_supply_gas = mongoengine.StringField(choices=type_applicable)
    has_supply_power = mongoengine.StringField(choices=type_applicable)
    has_supply_sewage = mongoengine.StringField(choices=type_applicable)
    has_supply_water = mongoengine.StringField(choices=type_applicable)
    has_swimming_pool = mongoengine.StringField(choices=type_applicable)
    has_thermal_solar_collector = mongoengine.StringField(choices=type_applicable)
    has_tiled_stove = mongoengine.StringField(choices=type_applicable)
    has_tumble_dryer = mongoengine.StringField(choices=type_applicable)
    has_washing_machine = mongoengine.StringField(choices=type_applicable)
    is_child_friendly = mongoengine.StringField(choices=type_applicable)
    is_corner_house = mongoengine.StringField(choices=type_applicable)
    is_demolition_property = mongoengine.StringField(choices=type_applicable)
    is_dilapidated = mongoengine.StringField(choices=type_applicable)
    is_first_occupancy = mongoengine.StringField(choices=type_applicable)
    is_ground_floor = mongoengine.StringField(choices=type_applicable)
    is_ground_floor_raised = mongoengine.StringField(choices=type_applicable)
    is_gutted = mongoengine.StringField(choices=type_applicable)
    is_in_need_of_renovation = mongoengine.StringField(choices=type_applicable)
    is_in_need_of_renovation_partially = mongoengine.StringField(choices=type_applicable)
    is_like_new = mongoengine.StringField(choices=type_applicable)
    is_middle_house = mongoengine.StringField(choices=type_applicable)
    is_modernized = mongoengine.StringField(choices=type_applicable)
    is_new_construction = mongoengine.StringField(choices=type_applicable)
    is_old_building = mongoengine.StringField(choices=type_applicable)
    is_projection = mongoengine.StringField(choices=type_applicable)
    is_quiet = mongoengine.StringField(choices=type_applicable)
    is_refurbished = mongoengine.StringField(choices=type_applicable)
    is_refurbished_partially = mongoengine.StringField(choices=type_applicable)
    is_secondary_residence_allowed = mongoengine.StringField(choices=type_applicable)
    is_shell_construction = mongoengine.StringField(choices=type_applicable)
    is_smoking_allowed = mongoengine.StringField(choices=type_applicable)
    is_sunny = mongoengine.StringField(choices=type_applicable)
    is_under_roof = mongoengine.StringField(choices=type_applicable)
    is_well_tended = mongoengine.StringField(choices=type_applicable)
    is_wheelchair_accessible = mongoengine.StringField(choices=type_applicable)
    is_minergie_certified = mongoengine.StringField(choices=type_applicable)
    is_minergie_general = mongoengine.StringField(choices=type_applicable)

    number_of_apartements = mongoengine.IntField(min_value=0)
    number_of_bathrooms = mongoengine.IntField(min_value=0)
    number_of_floors = mongoengine.IntField(min_value=0)
    number_of_parcels = mongoengine.IntField(min_value=0)
    number_of_rooms = mongoengine.DecimalField(min_value=0)
    number_of_showers = mongoengine.IntField(min_value=0)
    number_of_toilets = mongoengine.IntField(min_value=0)
    number_of_toilets_guest = mongoengine.IntField(min_value=0)

    on_even_ground = mongoengine.StringField(choices=type_applicable)
    on_hillside = mongoengine.StringField(choices=type_applicable)
    on_hillside_south = mongoengine.StringField(choices=type_applicable)

    utilization_ratio = mongoengine.DecimalField(min_value=0, max_value=1)
    utilization_ratio_construction = mongoengine.DecimalField(min_value=0, max_value=1)
    volume_gva = mongoengine.DecimalField(min_value=0)
    volumesia = mongoengine.DecimalField(min_value=0)
    volumesia_afv = mongoengine.DecimalField(min_value=0)
    volumesia_akv = mongoengine.DecimalField(min_value=0)
    volumesia_angv = mongoengine.DecimalField(min_value=0)
    volumesia_anv = mongoengine.DecimalField(min_value=0)
    volumesia_avv = mongoengine.DecimalField(min_value=0)
    volumesia_gv = mongoengine.DecimalField(min_value=0)
    year_built = mongoengine.IntField(null=True)
    year_last_renovated = mongoengine.IntField(null=True)

    # IDX fields
    has_isdn = mongoengine.StringField(choices=type_applicable)
    has_railway_terminal = mongoengine.StringField(choices=type_applicable)
    has_restrooms = mongoengine.StringField(choices=type_applicable)
    is_under_building_laws = mongoengine.StringField(choices=type_applicable)

    # Custom caracteristics
    custom_fields = mongoengine.EmbeddedDocumentListField(CustomCaracteristic)


# Valid
class Heating(mongoengine.EmbeddedDocument):
    generation = mongoengine.StringField(choices=type_heating_generation)
    distribution = mongoengine.StringField(choices=type_heating_distribution)


class Document(mongoengine.EmbeddedDocument):
    url = mongoengine.StringField(required=True)
    title = mongoengine.StringField()
    mime_type = mongoengine.StringField(min_length=7)

    @property
    def name(self):
        if self.url is not None:
            return self.url.split("/")[-1]


class Image(mongoengine.EmbeddedDocument):
    url = mongoengine.StringField(required=True)
    title = mongoengine.StringField()
    description = mongoengine.StringField()
    mime_type = mongoengine.StringField(min_length=7)

    @property
    def name(self):
        if self.url is not None:
            imname = self.url.split("/")[-1]
            extension = imname.split('.')[-1]
            return imname.replace(extension, 'jpg')


class Link(mongoengine.EmbeddedDocument):
    url = mongoengine.StringField(required=True)
    title = mongoengine.StringField()


class LinkDirect(mongoengine.EmbeddedDocument):
    url = mongoengine.StringField(required=True)
    title = mongoengine.StringField()


class LinkVirtualTour(mongoengine.EmbeddedDocument):
    url = mongoengine.StringField(required=True)
    title = mongoengine.StringField()


class LinkYoutube(mongoengine.EmbeddedDocument):
    url = mongoengine.StringField(required=True)
    title = mongoengine.StringField()


class Logo(mongoengine.EmbeddedDocument):
    url = mongoengine.StringField(required=True)
    mime_type = mongoengine.StringField(min_length=7)


class Plan(mongoengine.EmbeddedDocument):
    url = mongoengine.StringField(required=True)
    title = mongoengine.StringField()
    mime_type = mongoengine.StringField(min_length=7)


class Attachments(mongoengine.EmbeddedDocument):
    images = mongoengine.EmbeddedDocumentListField(Image, default=None, blank=True, required=False)
    documents = mongoengine.EmbeddedDocumentListField(Document, default=None, blank=True, required=False)
    links = mongoengine.EmbeddedDocumentListField(Link, default=None, blank=True, required=False)
    link_directs = mongoengine.EmbeddedDocumentListField(Link, default=None, blank=True, required=False)
    link_virtual_tours = mongoengine.EmbeddedDocumentListField(Link, default=None, blank=True, required=False)
    link_youtubes = mongoengine.EmbeddedDocumentListField(Link, default=None, blank=True, required=False)
    logos = mongoengine.EmbeddedDocumentListField(Logo, default=None, blank=True, required=False)
    plans = mongoengine.EmbeddedDocumentListField(Plan, default=None, blank=True, required=False)


class Event(mongoengine.EmbeddedDocument):
    name = mongoengine.StringField()
    start = mongoengine.DateTimeField()
    end = mongoengine.DateTimeField()
    location = mongoengine.StringField()
    summary = mongoengine.StringField()


class Localisation(mongoengine.EmbeddedDocument):

    # Simple marketing title.
    name = mongoengine.StringField(required=True)

    # Lang code
    lang = mongoengine.StringField(required=True, min_length=2, max_length=2, regex='[a-z]{2}')

    # A short extract from description, location and equipment.
    excerpt = mongoengine.StringField()

    # Main description.
    description = mongoengine.StringField()

    # Description of the surrounding and location.
    location = mongoengine.StringField()

    # Description of available and included equipment.
    equipment = mongoengine.StringField()

    # Sequence links, files, embeds and media.
    attachments = mongoengine.EmbeddedDocumentField(Attachments)

    # List of events
    events = mongoengine.EmbeddedDocumentListField(Event)

    # Specific visiting instructions or information, where to get the key, when to contact and similar.
    visit_information = mongoengine.StringField()


# Valid
class Buy(mongoengine.EmbeddedDocument):
    price = mongoengine.IntField(min_value=0, null=True)
    extra = mongoengine.IntField(min_value=0)
    wir_percentage = mongoengine.DecimalField(min_value=0, max_value=100)
    # referring = mongoengine.StringField(choices=price_referring)


# Valid
class Rent(mongoengine.EmbeddedDocument):

    class PriceUnit(object):
        PER_M2_YEAR = 'per_m2_year'
        PER_MONTH = 'per_month'
        PER_YEAR = 'per_year'
        CHOICES = (PER_M2_YEAR, PER_MONTH, PER_YEAR)

    gross = mongoengine.IntField(min_value=0, null=True)
    net = mongoengine.IntField(min_value=0, null=True)
    extra = mongoengine.IntField(min_value=0, null=True)
    price_unit = mongoengine.StringField(null=True, choices=PriceUnit.CHOICES, default=PriceUnit.PER_MONTH)
    interval = mongoengine.StringField(choices=price_interval)
    # referring = mongoengine.StringField(choices=price_referring)


# Valid
class Deposit(mongoengine.EmbeddedDocument):
    type = mongoengine.StringField(choices=deposit_type)
    value = mongoengine.IntField(min_value=0, null=True)


# Valid
class Auction(mongoengine.EmbeddedDocument):
    expiration = mongoengine.DateTimeField()
    start = mongoengine.DateTimeField()


# Valid
class AdditionalOffer(mongoengine.EmbeddedDocument):
    interval = mongoengine.IntField(min_value=0)
    type = mongoengine.StringField()
    value = mongoengine.IntField(min_value=0, choices=additional_offer_type)


# Valid
class Prices(mongoengine.EmbeddedDocument):
    currency = mongoengine.StringField(min_length=3, max_length=3, regex='[A-Z]', required=True)
    rent = mongoengine.EmbeddedDocumentField(Rent)
    buy = mongoengine.EmbeddedDocumentField(Buy)
    auction = mongoengine.EmbeddedDocumentField(Auction)
    deposit = mongoengine.EmbeddedDocumentField(Deposit)
    additional_offers = mongoengine.EmbeddedDocumentListField(AdditionalOffer)


class GeoCoordinates(mongoengine.EmbeddedDocument):
    latitude = mongoengine.FloatField(required=True)
    longitude = mongoengine.FloatField(required=True)
    elevation = mongoengine.FloatField()


class Address(mongoengine.EmbeddedDocument):
    country_code = mongoengine.StringField()
    locality = mongoengine.StringField()
    region = mongoengine.StringField(null=True)
    postal_code = mongoengine.StringField()
    post_office_box_number = mongoengine.StringField(null=True)
    street = mongoengine.StringField()
    street_number = mongoengine.StringField(null=True)
    street_addition = mongoengine.StringField(null=True)
    geo = mongoengine.EmbeddedDocumentField(GeoCoordinates)


class Organization(mongoengine.EmbeddedDocument):
    # id = mongoengine.StringField(required=True)
    address = mongoengine.EmbeddedDocumentField(Address)
    brand = mongoengine.StringField()
    email = mongoengine.StringField()
    email_rem = mongoengine.StringField()
    legal_name = mongoengine.StringField()
    mobile = mongoengine.StringField()
    phone = mongoengine.StringField()
    website = mongoengine.StringField()


class Person(mongoengine.EmbeddedDocument):
    function = mongoengine.StringField()
    given_name = mongoengine.StringField()
    family_name = mongoengine.StringField()
    email = mongoengine.StringField()
    mobile = mongoengine.StringField()
    phone = mongoengine.StringField()
    gender = mongoengine.StringField()
    note = mongoengine.StringField()


class Seller(mongoengine.EmbeddedDocument):
    organization = mongoengine.EmbeddedDocumentField(Organization)
    contact_person = mongoengine.EmbeddedDocumentField(Person)
    inquiry_person = mongoengine.EmbeddedDocumentField(Person)
    visit_person = mongoengine.EmbeddedDocumentField(Person)


class Apartment(mongoengine.EmbeddedDocument):
    class MinergieCertification(object):
        CHOICES = [
            'Minergie',
            'Minergie-P',
            'Minergie-Eco',
            'Minergie-P-Eco'
        ]

    class AdType(object):
        CHOICES = [
            'buy',
            'rent'
        ]

    address: Address = mongoengine.EmbeddedDocumentField(Address)
    author = mongoengine.StringField()
    availability = mongoengine.EmbeddedDocumentField(Availability, required=True)
    bfs = mongoengine.EmbeddedDocumentField(Bfs)
    building_zones = mongoengine.StringField()
    category = mongoengine.StringField()
    object_type = mongoengine.StringField()
    # categories = mongoengine.ListField(mongoengine.StringField(choices=categories_choices))
    characteristics = mongoengine.EmbeddedDocumentField(Characteristics)
    created = mongoengine.DateTimeField()
    development = mongoengine.StringField(choices=type_development, default="full")
    heating = mongoengine.EmbeddedDocumentField(Heating)
    localizations = mongoengine.EmbeddedDocumentListField(Localisation, min_length=1, required=True)
    minergie_certification = mongoengine.StringField(choices=MinergieCertification.CHOICES, required=False)
    modified = mongoengine.DateTimeField()
    parcel_numbers = mongoengine.StringField()
    prices = mongoengine.EmbeddedDocumentField(Prices, required=True)
    reference_id = mongoengine.StringField(required=True)
    seller = mongoengine.EmbeddedDocumentField(Seller)
    type = mongoengine.StringField(choices=AdType.CHOICES, required=True)
    utilizations = mongoengine.ListField(mongoengine.StringField(choices=type_utilization))
    visual_reference_id = mongoengine.StringField()

    meta = {
        'db_alias': 'core',
    }

    def get_localisation(self, lang):
        for x in self.localizations:
            if x.lang == lang:
                return x
        if len(self.localizations) > 0:
            return self.localizations[0]

    def get_images(self, lang):
        loc = self.get_localisation(lang)
        if loc is not None:
            if loc.attachments.images is not None:
                return loc.attachments.images
        return []

    def get_documents(self, lang):
        loc = self.get_localisation(lang)
        if loc is not None:
            if loc.attachments.documents is not None:
                return loc.attachments.documents
        return []
