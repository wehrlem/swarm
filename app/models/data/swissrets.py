import mongoengine


class Address(mongoengine.EmbeddedDocument):

    countryCode = mongoengine.StringField()
    locality = mongoengine.StringField()
    region = mongoengine.StringField()
    postalCode = mongoengine.StringField()


class Property(mongoengine.Document):

    address = mongoengine.EmbeddedDocument()
