import mongoengine
import datetime
from .google import GoogleInterest


class FacebookAdSetting(mongoengine.EmbeddedDocument):

    bid_strategy = mongoengine.StringField()
    billing_event = mongoengine.StringField()
    optimization_goal = mongoengine.StringField()
    budget_type = mongoengine.StringField()
    call_to_action_type = mongoengine.StringField()
    destination_type = mongoengine.StringField()
    ad_formats = mongoengine.ListField(mongoengine.StringField())


class FacebookTargetSet(mongoengine.EmbeddedDocument):

    gender_choices = (
        "Male",
        "Female"
    )

    name = mongoengine.StringField(required=True)
    age_min = mongoengine.StringField(required=True, default='18')
    age_max = mongoengine.StringField(required=True, default='60')
    gender = mongoengine.ListField(mongoengine.StringField(choices=gender_choices))
    keywords = mongoengine.ListField(mongoengine.StringField())
    placements = mongoengine.ListField(mongoengine.StringField())
    interest = mongoengine.ListField(mongoengine.StringField())
    languages = mongoengine.ListField(mongoengine.StringField())
    life_events = mongoengine.ListField(mongoengine.StringField())
    behaviors = mongoengine.ListField(mongoengine.StringField())
    publisher_platforms = mongoengine.ListField(mongoengine.StringField())
    device_platforms = mongoengine.ListField(mongoengine.StringField())
    facebook_positions = mongoengine.ListField(mongoengine.StringField())


class GoogleTargetSet(mongoengine.EmbeddedDocument):

    gender_choices = (
        "Male",
        "Female"
    )

    name = mongoengine.StringField(required=True)
    creation_date = mongoengine.DateTimeField(default=datetime.datetime.now)
    age_range = mongoengine.StringField(required=True, default='18-60')
    gender = mongoengine.ListField(mongoengine.StringField(choices=gender_choices))
    keywords = mongoengine.ListField(mongoengine.StringField())
    placements = mongoengine.ListField(mongoengine.StringField())
    interest = mongoengine.ListField(mongoengine.ReferenceField(GoogleInterest))


class FacebookCampaignSetting(mongoengine.Document):

    name = mongoengine.StringField(unique=True)
    objective = mongoengine.StringField()
    status = mongoengine.StringField()
    creation_date = mongoengine.DateTimeField(default=datetime.datetime.now)

    active = mongoengine.BooleanField(default=False)
    default = mongoengine.BooleanField(default=False)

    target_set: FacebookTargetSet = mongoengine.EmbeddedDocumentField(FacebookTargetSet, default=FacebookTargetSet())
    ad_setting: FacebookAdSetting = mongoengine.EmbeddedDocumentField(FacebookAdSetting, default=FacebookAdSetting())

    meta = {
        'db_alias': 'core',
        'collection': 'facebook_campaign_setting'
    }


class TargetingSettings(mongoengine.EmbeddedDocument):

    name = mongoengine.StringField()
    target_all = mongoengine.BooleanField()


class GoogleCampaignSetting(mongoengine.Document):

    name = mongoengine.StringField(unique=True)
    target_set: GoogleTargetSet = mongoengine.EmbeddedDocumentField(GoogleTargetSet, default=GoogleTargetSet())
    max_cpc_bid = mongoengine.FloatField()
    daily_budget = mongoengine.FloatField()
    targeting_settings = mongoengine.EmbeddedDocumentListField(TargetingSettings)

    meta = {
        'db_alias': 'core',
        'collection': 'google_campaign_setting'
    }


class CampaignSettings(mongoengine.Document):

    name = mongoengine.StringField()
    creation_date = mongoengine.DateTimeField(default=datetime.datetime.now)
    update_date = mongoengine.DateTimeField()

    facebook: FacebookCampaignSetting = mongoengine.ReferenceField(FacebookCampaignSetting)
    google: GoogleCampaignSetting = mongoengine.ReferenceField(GoogleCampaignSetting)

    meta = {
        'db_alias': 'core',
        'collection': 'campaign_setting'
    }
