import mongoengine
import datetime


class GoogleTargetSet(mongoengine.EmbeddedDocument):
    max_cpc_bid = mongoengine.FloatField()
    daily_budget = mongoengine.FloatField()
    interest = mongoengine.ListField(mongoengine.IntField())


class TargetSet(mongoengine.Document):

    gender_choices = (
        "Male",
        "Female"
    )

    name = mongoengine.StringField(required=True)
    targetset_update = mongoengine.BooleanField(default=False)
    creation_date = mongoengine.DateTimeField(default=datetime.datetime.now)
    age_range = mongoengine.StringField(required=True, default='18-60')
    gender = mongoengine.ListField(mongoengine.StringField(choices=gender_choices))
    keywords = mongoengine.ListField(mongoengine.StringField())
    placements = mongoengine.ListField(mongoengine.StringField())
    interest = mongoengine.ListField(mongoengine.StringField())
    devices = mongoengine.ListField(mongoengine.StringField())
    languages = mongoengine.ListField(mongoengine.StringField())
    life_events = mongoengine.ListField(mongoengine.StringField())
    behaviours = mongoengine.ListField(mongoengine.StringField())

    # Google Target Set
    google: GoogleTargetSet = mongoengine.EmbeddedDocumentField(GoogleTargetSet, default=GoogleTargetSet())

    default = mongoengine.BooleanField(default=False)
    active = mongoengine.BooleanField(default=False)

    meta = {
        'db_alias': 'core',
        'collection': 'target_sets'
    }
