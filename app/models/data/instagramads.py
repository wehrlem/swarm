import mongoengine


class InstagramAd(mongoengine.EmbeddedDocument):

    type_choices = (
        'feed',
        'right_column'
    )

    type = mongoengine.StringField(required=True, choices=type_choices)
    url = mongoengine.StringField(required=True)
    title = mongoengine.StringField(required=True)
    message = mongoengine.StringField(required=True)

    descriptions = mongoengine.ListField(mongoengine.StringField())
    marketing_images = mongoengine.ListField(mongoengine.StringField())
