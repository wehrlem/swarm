import mongoengine
import datetime


class Token(mongoengine.EmbeddedDocument):

    app_id = mongoengine.StringField(required=True)
    app_secret = mongoengine.StringField(required=True)
    account_id = mongoengine.StringField(required=True)
    page_id = mongoengine.StringField()
    access_token = mongoengine.StringField(required=True)

    updated_date = mongoengine.DateTimeField(default=datetime.datetime.now)

    meta = {
        'db_alias': 'core',
        'collection': 'platforms'
    }