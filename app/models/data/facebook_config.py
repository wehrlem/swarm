import mongoengine
import datetime
from typing import List, Dict

class AccessConfig(mongoengine.EmbeddedDocument):

    access_token = mongoengine.StringField()
    ad_account_id = mongoengine.StringField()
    app_secret = mongoengine.StringField()
    app_id = mongoengine.StringField()

class CampaignSetting(mongoengine.EmbeddedDocument):

    objective = mongoengine.StringField()
    status = mongoengine.StringField()


class AdSetting(mongoengine.EmbeddedDocument):

    bid_strategy = mongoengine.StringField()
    adset_billing_event = mongoengine.StringField()
    adset_optimization_goal = mongoengine.StringField()
    adset_budget_type = mongoengine.StringField()
    ad_call_to_action_type = mongoengine.StringField()
    ad_adformats = mongoengine.ListField(mongoengine.StringField())

class TargetSetSetting(mongoengine.EmbeddedDocument):
    gender_choices = (
        "Male",
        "Female"
    )

    name = mongoengine.StringField(required=True)
    creation_date = mongoengine.DateTimeField(default=datetime.datetime.now)
    age_range = mongoengine.StringField(required=True, default='18-60')
    gender = mongoengine.ListField(mongoengine.StringField(choices=gender_choices))
    keywords = mongoengine.ListField(mongoengine.StringField())
    placements = mongoengine.ListField(mongoengine.StringField())
    interest = mongoengine.ListField(mongoengine.StringField())
    devices = mongoengine.ListField(mongoengine.StringField())
    languages = mongoengine.ListField(mongoengine.StringField())
    life_events = mongoengine.ListField(mongoengine.StringField())
    behaviours = mongoengine.ListField(mongoengine.StringField())




class FacebookConfig(mongoengine.Document):

    name = mongoengine.StringField()
    active = mongoengine.BooleanField(default=False)
    default = mongoengine.BooleanField(default=False)
    activation_time = mongoengine.DateTimeField(default=datetime.datetime.now)
    deactivation_time = mongoengine.DateTimeField()

    access: AccessConfig = mongoengine.EmbeddedDocumentField(AccessConfig, default=AccessConfig())
    campaign_setting: CampaignSetting = mongoengine.EmbeddedDocumentField(CampaignSetting, default=CampaignSetting())
    ad_setting: AdSetting = mongoengine.EmbeddedDocumentField(AdSetting, default=AdSetting())
    target_set: TargetSetSetting = mongoengine.EmbeddedDocumentField(TargetSetSetting, default=TargetSetSetting())

    meta = {
        'db_alias': 'core',
        'collection': 'facebook_config'
    }







