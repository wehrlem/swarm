import mongoengine


class BudgetDistribution(mongoengine.EmbeddedDocument):

    platform = mongoengine.StringField(required=True)
    ad_type = mongoengine.StringField(required=True)
    percentage = mongoengine.FloatField(default=0.0)
    netto_price = mongoengine.FloatField(default=0.0)
