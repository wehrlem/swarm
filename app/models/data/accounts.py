import mongoengine
import datetime
import app.models.helper.security as tokenHelper

from mongoengine.queryset.visitor import Q
from app.models.data.platforms import Platform


class FtpConfig(mongoengine.EmbeddedDocument):

    agency_id = mongoengine.StringField(default="")
    host = mongoengine.StringField(default="")
    username = mongoengine.StringField(default="")
    password = mongoengine.StringField(default="")
    export_name = mongoengine.StringField(default="")
    publisher_id = mongoengine.StringField(default="")
    is_active = mongoengine.BooleanField(default=False)
    save_in_root = mongoengine.BooleanField(default=False)


class SwissRETSConfig(mongoengine.EmbeddedDocument):

    name = mongoengine.StringField(default="")
    host = mongoengine.StringField(default="")
    username = mongoengine.StringField(default="")
    password = mongoengine.StringField(default="")
    organisation_id = mongoengine.StringField(default="")
    export_name = mongoengine.StringField(default="")


class BasePlatformConfig(mongoengine.EmbeddedDocument):

    """App level access to newhome.ch platform"""
    class Standard:
        IDX = 'idx'
        SwissRETS = "swissrets"
        CHOICES = (
            IDX,
            SwissRETS
        )
    idx: FtpConfig = mongoengine.EmbeddedDocumentField(FtpConfig)
    swissrets: FtpConfig = mongoengine.EmbeddedDocumentField(FtpConfig)
    standard = mongoengine.StringField(choices=Standard.CHOICES, required=True)
    active = mongoengine.BooleanField(required=True)


class FacebookSettings(mongoengine.EmbeddedDocument):
    # facebook
    facebook_page_id = mongoengine.StringField(default="")
    facebook_page_name = mongoengine.StringField(default="")
    facebook_page_active = mongoengine.BooleanField(default=False)


class Account(mongoengine.Document):

    registered_date = mongoengine.DateTimeField(default=datetime.datetime.now)
    name = mongoengine.StringField()
    email = mongoengine.EmailField()
    webpage = mongoengine.StringField()
    username = mongoengine.StringField(required=True, unique=True)
    password = mongoengine.StringField()
    token = mongoengine.StringField(required=True)
    is_active = mongoengine.BooleanField(default=False)
    saved_data = mongoengine.DictField()
    is_superuser = mongoengine.BooleanField(default=False)
    team = mongoengine.IntField()
    project_id = mongoengine.StringField()
    parrent = mongoengine.ReferenceField('self')
    platforms = mongoengine.MapField(mongoengine.EmbeddedDocumentField(BasePlatformConfig))

    meta = {
        'db_alias': 'core',
        'collection': 'accounts',
        'strict': False
    }

    def addSubAccount(self, user):
        if Account.objects.filter(parrent=user).first():
            return False
        user.parrent = self
        user.save()
        return True

    def getSubUserByTeamId(self, project_id, team):
        return Account.objects.filter(Q(team=team, project_id=project_id) & Q(parrent=self)).first()

    def getTeamsWithActivePlatform(self, platform_name):
        return Account.objects.filter(Q(parrent=self), **{f'platforms__{platform_name}__active': True}).values_list('team', 'project_id')

    def getActiveSubAccounts(self):
        '''
            Get subaccounts with active platforms
        '''
        queryset = Account.objects.filter(Q(parrent=self))
        tmpq = None
        for p_name in Platform.objects.get_active_platforms():
            tq = Q(**{f'platforms__{p_name}__active': True})
            if tmpq is None:
                tmpq = tq
            else:
                tmpq |= tq
        if tmpq:
            queryset = queryset.filter(tmpq)
        return queryset

    def getStandard(self, platform_name):
        base_config = self.platforms.get(platform_name, None)
        if base_config is not None:
            return base_config.standard

    def getPlatformStatus(self, platform_name):
        platform_cfg = self.platforms.get(platform_name, None)
        if platform_cfg is not None:
            return platform_cfg.active
        else:
            return False

    def getPlatformConfig(self, platform_name):
        platform_cfg = self.platforms.get(platform_name, None)
        if platform_cfg is not None and hasattr(platform_cfg, "standard"):
            if platform_cfg.standard is not None:
                return platform_cfg.standard, getattr(platform_cfg, platform_cfg.standard)
        return None, None

    def getActivePlatforms(self):
        active_platforms = []
        for platform in Platform.objects.get_active_platforms():
            pdata = self.platforms.get(platform, None)
            if isinstance(pdata, BasePlatformConfig):
                if getattr(pdata, "active"):
                    active_platforms.append(platform)
        return active_platforms

    def getToken(self):
        token = tokenHelper.generate_token()
        self.token = tokenHelper.hash_token(token)
        self.save()
        return token
