import mongoengine


class GoogleAd(mongoengine.EmbeddedDocument):

    type_choices = (
        'display',
        'search'
    )

    type = mongoengine.StringField(required=True, choices=type_choices)
    url = mongoengine.StringField(required=True)
    headline1 = mongoengine.StringField()
    headline2 = mongoengine.StringField()
    headline3 = mongoengine.StringField()

    short_headlines = mongoengine.ListField(mongoengine.StringField())
    long_headline = mongoengine.StringField()
    descriptions = mongoengine.ListField(mongoengine.StringField())

    marketing_images = mongoengine.ListField(mongoengine.StringField())
    square_marketing_images = mongoengine.ListField(mongoengine.StringField())
    logo_images = mongoengine.ListField(mongoengine.StringField())
