import mongoengine
import datetime
from app.models.data.budgetdistributions import BudgetDistribution


class Budget(mongoengine.EmbeddedDocument):

    create_date = mongoengine.DateTimeField(default=datetime.datetime.now)
    update_date = mongoengine.DateTimeField(default=datetime.datetime.now)

    budget_optimization = mongoengine.BooleanField(required=True)
    currency = mongoengine.StringField(required=True)
    brutto_budget = mongoengine.FloatField(default=0.0)
    app_percentage = mongoengine.FloatField(default=0.0)

    budget_distribution = mongoengine.EmbeddedDocumentListField(BudgetDistribution)

    @property
    def netto_budget(self):
        nb = self.brutto_budget * self.app_percentage
        return nb
