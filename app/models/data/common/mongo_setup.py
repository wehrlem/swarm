import mongoengine
from dotenv import load_dotenv
import os

# Load settings
load_dotenv()


def global_init(server=None):

    DEFAULT_PORT = 27017
    ALIAS_CORE_KEY = "ALIAS_CORE"
    DB_NAME_KEY = "DB_NAME"
    HOST_KEY = "HOST"
    PORT_KEY = "PORT"
    DB_USER_KEY = "DB_USER"
    DB_PASS_KEY = "DB_PASS"

    if server:
        ALIAS_CORE_KEY = "{}_ALIAS_CORE".format(server)
        DB_NAME_KEY = "{}_DB_NAME".format(server)
        HOST_KEY = "{}_HOST".format(server)
        PORT_KEY = "{}_PORT".format(server)
        DB_USER_KEY = "{}_DB_USER".format(server)
        DB_PASS_KEY = "{}_DB_PASS".format(server)

    args = dict(
        alias=os.getenv(ALIAS_CORE_KEY),
        name=os.getenv(DB_NAME_KEY),
        host=os.getenv(HOST_KEY),
        port=int(os.getenv(PORT_KEY)) if os.getenv(PORT_KEY) else DEFAULT_PORT,
        username=os.getenv(DB_USER_KEY),
        password=os.getenv(DB_PASS_KEY),
    )
    mongoengine.register_connection(**args)
