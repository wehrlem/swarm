class CampaignPlatformChoices(object):
    FACEBOOK = 'facebook'
    GOOGLE = 'google'
    INSTAGRAM = 'instagram'
    NEWHOME = 'newhome'
    HOMEGATE = 'homegate'
    IMMOSCOUT = 'immoscout'
    FLATFOX = 'flatfox'

    CHOICES = (
        NEWHOME,
        HOMEGATE,
        IMMOSCOUT,
        FLATFOX
    )


class CampaignStatus(object):
    CHOICES = (
        'active',
        'inactive'
    )


class CampaignType(object):
    CHOICES = (
        'single',
        'collection',
        'campaign'
    )


class Status(object):
    ACTIVE = "ACTIVE"
    ADD = "ADD"
    REMOVE = "REMOVE"


class GoogleAdType(object):
    SEARCH = "SEARCH"
    SEARCH_OBSERVATION = "SEARCH_OBSERVATION"
    DISPLAY = "DISPLAY"
