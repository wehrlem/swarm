import json
import mongoengine
from datetime import datetime, timedelta

# from app.models.data.budgets import Budget
# from app.models.data.targetings import Targeting
# from app.models.data.googleads import GoogleAd
# from app.models.data.facebookads import FacebookAd
# from app.models.data.instagramads import InstagramAd
# from app.models.data.campaign_settings import CampaignSettings
# from app.models.data.common.configs import Platforms
# from app.models.data.campaign_platform_mapper import CampaignPlatformMapper
from app.models.data.accounts import Account
from app.models.data.apartment import Apartment
from app.models.data.common.configs import CampaignType
from app.models.data.common.configs import CampaignPlatformChoices

from mongoengine.queryset.visitor import Q
from mongoengine.queryset import QuerySet

END_HOUR = 23


class CampaignQuerySet(QuerySet):

    def __get_published_base(self, platform):
        all_camps = self.aggregate(
            {
                "$unwind": "$platforms"
            },
            {
                "$match": {
                    "platforms.name": platform,
                    "$or": [
                        {"platforms.status": "active"},
                        {"platforms.status": "publish"}
                    ],
                    "$and": [
                        {"platforms.start_date": {"$lte": datetime.now()}},
                        {
                            "$or": [
                                {"platforms.end_date": None},
                                {"platforms.end_date": {"$gte": datetime.now()}}
                            ]
                        }
                    ],
                }
            },
            {
                "$group": {
                    "_id": "$_id",
                }
            }
        )
        ids = [str(x["_id"])for x in all_camps]
        return self.filter(id__in=ids)

    # Get published by platforms
    def get_published(self, platforms):
        return self.__get_published_base({
            "$in": platforms
        })

    def add_team(self, user):
        return self.filter(Q(team=user.team) & Q(project_id=user.project_id))

    def exclude_active_teams(self, user, platform_name):
        team_filter = Q()
        for team_id, project_id in user.getTeamsWithActivePlatform(platform_name):
            tq = (Q(project_id__ne=project_id) | Q(team__ne=team_id))
            team_filter = team_filter & tq
        return self.filter(team_filter)

    # Get published by platform name
    def get_published_by_platform(self, platform_name):
        return self.__get_published_base(platform_name)

    def get_campaign(self, campaign_id):
        return self.filter(Q(id=campaign_id))

    def get_campaign_by_reference_id(self, user, project_id, reference_id):
        return self.filter(Q(project_id=project_id) & Q(apartment__reference_id=reference_id) & Q(account_id=user))

    def get_last_campaign(self, reference_id):
        return self.filter(Q(apartment__reference_id=reference_id)).order_by("-create_date").first()

    def get_campaigns(self, projects):
        filters = (Q(project_id=""))
        for project_name, apartments in projects.items():
            if project_name is not None or project_name != "" or type(apartments) is list:
                filters = filters | (Q(project_id=project_name) & Q(apartment__reference_id__in=apartments))
        return self.filter(filters)

    def get_active_campaigns(self, user_id, project_id, search=None):
        filters = (Q(account_id=user_id) & Q(project_id=project_id))
        if search is not None:
            import re
            regex = re.compile('[^a-zA-Z0-9-]')
            search = regex.sub('', search)
            return self.filter(filters).filter(name__contains="{search}".format(search=search))
        return self.filter(filters)


class CampaignPlatform(mongoengine.EmbeddedDocument):
    STATUS_CHOICES = [
        "publish",
        "review",
        "inactive",
        "active",
        "draft"
    ]
    name = mongoengine.StringField(required=True)
    create_date = mongoengine.DateField(default=datetime.utcnow)
    start_date = mongoengine.DateTimeField()
    end_date = mongoengine.DateTimeField()
    product = mongoengine.StringField()
    duration = mongoengine.IntField()
    status = mongoengine.StringField(choices=STATUS_CHOICES, default="draft")

    @property
    def product_key(self):
        if self.product:
            return self.product[:5]

    @property
    def product_duration(self):
        try:
            if self.duration:
                return self.duration
            if self.product:
                return int(self.product[5:])
        except Exception:
            pass

    @property
    def is_published(self):
        """
            If ad is published on platform with delay
        """
        status = True if self.status in ['draft', 'publish', 'active'] else False
        start_date = True if isinstance(self.start_date, datetime) else False
        end_date = True if self.end_date is None or (isinstance(self.end_date, datetime) and self.end_date >= datetime.now() and self.end_date >= self.start_date) else False
        return status and start_date and end_date

    @property
    def is_active(self):
        """
            If ad is curently published on platform
        """
        start_date = True if isinstance(self.start_date, datetime) and (self.start_date <= datetime.now()) else False
        return start_date and self.is_published

    @property
    def has_promotion(self):
        if not self.is_published or self.product == "basic":
            return False
        if self.product_duration:
            start_promotion = self.start_date.replace(hour=0, minute=0, second=0, microsecond=0)
            end_promotion = start_promotion + timedelta(days=self.product_duration - 1, hours=END_HOUR)
        else:
            end_promotion = self.end_date
        if end_promotion is None:
            return True
        if isinstance(end_promotion, datetime) and datetime.utcnow() <= end_promotion:
            return True
        return False


class Campaign(mongoengine.Document):

    name: str = mongoengine.StringField()

    campaign_type = mongoengine.StringField(choices=CampaignType.CHOICES, required=True, default='single')
    application_url = mongoengine.StringField(required=False)
    project_id = mongoengine.StringField(required=True)
    team = mongoengine.IntField()

    platforms = mongoengine.ListField(mongoengine.EmbeddedDocumentField(CampaignPlatform))
    apartment: Apartment = mongoengine.EmbeddedDocumentField(Apartment)

    create_date = mongoengine.DateTimeField(default=datetime.utcnow)
    update_date = mongoengine.DateTimeField()
    account_id = mongoengine.ReferenceField(Account, required=True)

    refresh = mongoengine.IntField(default=0)

    # facebookads: FacebookAd = mongoengine.EmbeddedDocumentListField(FacebookAd)
    # googleads: GoogleAd = mongoengine.EmbeddedDocumentListField(GoogleAd)
    # instagramads: InstagramAd = mongoengine.EmbeddedDocumentListField(InstagramAd)

    # budget: Budget = mongoengine.EmbeddedDocumentField(Budget)
    # targeting: Targeting = mongoengine.EmbeddedDocumentField(Targeting)

    # campaign_platform_mapper: CampaignPlatformMapper = mongoengine.EmbeddedDocumentField(CampaignPlatformMapper)
    # campaign_settings: CampaignSettings = mongoengine.ReferenceField(CampaignSettings)

    meta = {
        'db_alias': 'core',
        'collection': 'campaigns',
        'indexes': [
            {
                'fields': ['account_id', 'project_id', 'apartment.reference_id'],
                'unique': True
            },
        ],
        'queryset_class': CampaignQuerySet,
        'strict': False,
    }

    def update(self, **kwargs):
        if "platforms" in kwargs:
            platforms = kwargs.pop("platforms")
            platforms_keys = []
            for platform in platforms:
                if not platform.get("name"):
                    continue
                platforms_keys.append(platform.get("name"))
                pl = self.get_platform(platform.get("name"))
                if pl:
                    # Create new ads
                    pl.product = platform.get("product")
                    pl.create_date = datetime.utcnow()
                    pl.duration = platform.get("duration")
                    pl.start_date = platform.get("start_date")
                    pl.end_date = platform.get("end_date")
                    pl.status = "active"
                    if not pl.is_published:
                        pl.status = 'inactive'
                    continue
                else:
                    # Create new
                    p = CampaignPlatform(
                        name=platform.get("name"),
                        end_date=platform.get("end_date"),
                        product=platform.get("product"),
                        duration=platform.get("duration")
                    )
                    p.start_date = platform.get("start_date")
                    p.status = "active"
                    if not p.is_published:
                        p.status = 'inactive'
                    self.platforms.append(p)
            for x in self.platforms:
                if x.name not in platforms_keys:
                    x.product = None
                    x.duration = None
                    x.start_date = None
                    x.end_date = None
                    x.status = "inactive"
        return super(Campaign, self).update(**kwargs)

    def update_platforms(self):
        for x in self.platforms:
            if x.is_published:
                x.status = 'active'
            else:
                x.status = 'inactive'

    @property
    def apartment_id(self):
        if self.refresh is None or self.refresh == 0:
            return self.apartment.reference_id
        else:
            return "{}-{}".format(self.apartment.reference_id, self.refresh)

    @property
    def duration_in_days(self):
        dt = self.end_date - self.start_date
        return dt.days

    def is_platform_set(self, platform_name):
        for x in self.platforms:
            if x.name == platform_name:
                return True
        return False

    def update_platform_status(self, platform_name):
        for x in self.platforms:
            if x.name == platform_name:
                x.status = "publish"

    def get_platform(self, platform_name) -> CampaignPlatform:
        for x in self.platforms:
            if x.name == platform_name:
                return x

    def get_active_platforms(self, platform_name=None):
        subuser = None
        if self.team:
            subuser = Account.objects.filter(parrent=self.account_id, team=self.team).first()
        response = {}
        for p_name in self.platforms:
            if platform_name and p_name.name != platform_name:
                continue
            if p_name.is_active:
                if p_name.name == CampaignPlatformChoices.FLATFOX:
                    response[p_name.name] = self.account_id
                elif subuser and p_name.name in subuser.platforms and subuser.platforms[p_name.name].active:
                    response[p_name.name] = subuser
                else:
                    response[p_name.name] = self.account_id
        return response

    def get_published_platforms(self, platform_name=None):
        subuser = None
        if self.team:
            subuser = Account.objects.filter(parrent=self.account_id, team=self.team).first()
        response = {}
        for p_name in self.platforms:
            if platform_name and p_name.name != platform_name:
                continue
            if p_name.is_published:
                if p_name.name == CampaignPlatformChoices.FLATFOX:
                    response[p_name.name] = self.account_id
                elif subuser and p_name.name in subuser.platforms and subuser.platforms[p_name.name].active:
                    response[p_name.name] = subuser
                else:
                    response[p_name.name] = self.account_id
        return response

    @property
    def active_platforms(self):
        rez = []
        for x in self.platforms:
            if x.is_active:
                rez.append(x)
        return rez

    @property
    def status(self):
        for x in self.platforms:
            if x.is_active is True:
                return True
        return False

    @property
    def number_active_platforms(self):
        return len(self.active_platforms)

    @property
    def active_platforms_names(self):
        return [x.name for x in self.active_platforms]

    def deactivate_platform(self, platform_name):
        for x in self.platforms:
            if x.name == platform_name:
                x.status = "inactive"
                self.save()
                return

    def deactivate(self):
        for x in self.platforms:
            x.status = "inactive"
        self.save()

    def activatePlatform(self, platform: CampaignPlatform):
        old_platform = self.get_platform(platform.name)
        if old_platform is None:
            self.platforms.append(platform)
            self.save()
        else:
            old_platform.start_date = platform.start_date
            old_platform.end_date = platform.end_date
            old_platform.product = platform.product
            old_platform.status = "active"
            self.save()

    def get_platform_status(self, platform_name):
        for x in self.platforms:
            if x.name == platform_name:
                return x.is_active
        return False
