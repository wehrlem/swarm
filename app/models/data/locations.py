import mongoengine
from app.models.data.address_components import AddressComponent
from app.models.data.geometries import Geometry
from bson.objectid import ObjectId


class Location(mongoengine.EmbeddedDocument):

    id = mongoengine.ObjectIdField(required=True, default=ObjectId)
    place_id = mongoengine.StringField(required=True)
    formatted_address = mongoengine.StringField()
    name = mongoengine.StringField()

    address_components = mongoengine.EmbeddedDocumentListField(AddressComponent)
    geometry = mongoengine.EmbeddedDocumentField(Geometry)
