import mongoengine
from typing import List, Dict
from .common.configs import GoogleAdType


class LocationConfig(mongoengine.EmbeddedDocument):

    status_choice = (
        'ADD',
        'REMOVE',
        'ACTIVE',
        'REMOVED'
    )

    location_id = mongoengine.ObjectIdField(required=True)
    google_id = mongoengine.IntField()
    status = mongoengine.StringField(required=True, choices=status_choice)


class LanguageConfig(mongoengine.EmbeddedDocument):

    status_choice = (
        'ADD',
        'REMOVE',
        'ACTIVE'
    )

    name = mongoengine.StringField(required=True)
    google_id = mongoengine.IntField()
    status = mongoengine.StringField(required=True, choices=status_choice)


class KeywordConfig(mongoengine.EmbeddedDocument):

    status_choice = (
        'ADD',
        'REMOVE',
        'ACTIVE'
    )

    keyword = mongoengine.StringField(required=True)
    google_id = mongoengine.IntField()
    status = mongoengine.StringField(required=True, choices=status_choice)


class UserInterestConfig(mongoengine.EmbeddedDocument):

    status_choice = (
        'ADD',
        'REMOVE',
        'ACTIVE'
    )

    user_interest = mongoengine.IntField(required=True)
    google_id = mongoengine.IntField()
    status = mongoengine.StringField(required=True, choices=status_choice)


class GoogleAd(mongoengine.EmbeddedDocument):

    type_choice = (
        GoogleAdType.SEARCH,
        GoogleAdType.SEARCH_OBSERVATION,
        GoogleAdType.DISPLAY
    )

    type: str = mongoengine.StringField(required=True, default=GoogleAdType.SEARCH, choices=type_choice)
    campaign_id = mongoengine.LongField(default=None)
    ad_group_id = mongoengine.LongField(default=None)
    budget_id = mongoengine.LongField(default=None)
    ad_id = mongoengine.LongField(default=None)
    campaign_status: bool = mongoengine.BooleanField(required=True, default=False)


def get_google_default_ads():
    return [GoogleAd()]


class GoogleCPConfig(mongoengine.EmbeddedDocument):

    ads: List[GoogleAd] = mongoengine.EmbeddedDocumentListField(GoogleAd, default=get_google_default_ads())
    locations: List[LocationConfig] = mongoengine.EmbeddedDocumentListField(LocationConfig)
    languages: List[LanguageConfig] = mongoengine.EmbeddedDocumentListField(LanguageConfig)
    keywords: List[KeywordConfig] = mongoengine.EmbeddedDocumentListField(KeywordConfig)
    interest: List[UserInterestConfig] = mongoengine.EmbeddedDocumentListField(UserInterestConfig)


class CPConfig(mongoengine.Document):

    google: GoogleCPConfig = mongoengine.EmbeddedDocumentField(GoogleCPConfig, default=GoogleCPConfig())
    facebook = mongoengine.StringField()
    instagram = mongoengine.StringField()

    meta = {
        'db_alias': 'core',
        'collection': 'cp_config'
    }
