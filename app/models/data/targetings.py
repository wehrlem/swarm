import mongoengine

from app.models.data.locations import Location
from app.models.data.target_sets import TargetSet


class Targeting(mongoengine.EmbeddedDocument):

    # target_set: TargetSet = mongoengine.ReferenceField('TargetSet', required=True)

    locations: Location = mongoengine.EmbeddedDocumentListField(Location)
    languages: str = mongoengine.ListField(mongoengine.StringField())
    keywords: str = mongoengine.ListField(mongoengine.StringField())
