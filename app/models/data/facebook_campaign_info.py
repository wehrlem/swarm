import mongoengine


class FacebookCampaignInfo(mongoengine.Document):

    type_choices = (
        'feed',
        'right_column'
    )

    type: str = mongoengine.StringField(required=True, default="feed", choices=type_choices)
    campaign_id = mongoengine.LongField(default=None)
    ad_set_id = mongoengine.LongField(default=None)
    campaign_status: bool = mongoengine.BooleanField(required=True, default=False)

    meta = {
        'db_alias': 'core',
        'collection': 'facebook_campaigns_info'
    }
