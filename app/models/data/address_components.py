import mongoengine


class AddressComponent(mongoengine.EmbeddedDocument):

    type_choices = (
        'political',
        'country',
        'postal_code',
        'locality',
        'administrative_area_level_2',
        'administrative_area_level_1')

    long_name = mongoengine.StringField()
    short_name = mongoengine.StringField()
    types = mongoengine.ListField(mongoengine.StringField(choices=type_choices, required=True))
