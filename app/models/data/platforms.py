import mongoengine
from mongoengine.queryset.visitor import Q
from mongoengine.queryset import QuerySet


class PlatformQuerySet(QuerySet):

    def get_products(self, platform_name):
        return self.filter(Q(name=platform_name))

    def get_platforms(self):
        return self.scalar('name')

    def get_active_platforms(self):
        return self.filter(active=True).scalar('name')


class PackageItem(mongoengine.EmbeddedDocument):
    value = mongoengine.StringField(required=True)
    duration = mongoengine.IntField(required=True)
    price = mongoengine.StringField()
    type = mongoengine.StringField()
    min = mongoengine.IntField()
    step = mongoengine.IntField()

    meta = {
        'strict': False
    }


class Link(mongoengine.EmbeddedDocument):
    title = mongoengine.StringField()
    url = mongoengine.URLField()


class Package(mongoengine.EmbeddedDocument):
    name = mongoengine.StringField(required=True)
    displayName = mongoengine.StringField()
    options = mongoengine.EmbeddedDocumentListField(PackageItem)
    description = mongoengine.StringField()
    link = mongoengine.EmbeddedDocumentField(Link)

    meta = {
        'strict': False
    }


class Platform(mongoengine.Document):
    CHOICES = [
        "platform",
        "network"
    ]
    name = mongoengine.StringField(required=True)
    platformType = mongoengine.StringField(choices=CHOICES, required=True, default='platform')
    displayName = mongoengine.StringField()
    active = mongoengine.BooleanField(default=True)
    logo = mongoengine.StringField()
    packages = mongoengine.EmbeddedDocumentListField(Package)

    meta = {
        'db_alias': 'core',
        'collection': 'platform',
        'strict': False,
        'queryset_class': PlatformQuerySet,
    }

    def get_package(self, package_name):
        if package_name is None or package_name == '':
            return None
        for x in self.packages:
            if x.name == package_name:
                return {
                    "displayValue": x.displayName,
                    "price": "",
                    "duration": ""
                }
            for opt in x.options:
                if opt.value == package_name:
                    return {
                        "displayValue": x.displayName,
                        "price": opt.price,
                        "duration": opt.duration
                    }

    def get_products(self):
        items = []
        for x in self.packages:
            items.append(x.name)
            for x in x.options:
                items.append(x.value)
        return items
