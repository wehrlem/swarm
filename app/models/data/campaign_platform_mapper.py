import mongoengine
from typing import List, Dict
from .common.configs import GoogleAdType


class GoogleLocationMapper(mongoengine.EmbeddedDocument):

    status_choice = (
        'ADD',
        'REMOVE',
        'ACTIVE',
        'REMOVED'
    )

    location_id = mongoengine.ObjectIdField(required=True)
    google_id = mongoengine.IntField()
    status = mongoengine.StringField(required=True, choices=status_choice)


class GoogleEntityMapper(mongoengine.EmbeddedDocument):

    status_choice = (
        'ADD',
        'REMOVE',
        'ACTIVE'
    )

    name = mongoengine.StringField(required=True)
    entity = mongoengine.GenericReferenceField()
    google_id = mongoengine.LongField()
    status = mongoengine.StringField(required=True, choices=status_choice)


class GoogleAdMapper(mongoengine.EmbeddedDocument):

    type_choice = (
        GoogleAdType.SEARCH,
        GoogleAdType.SEARCH_OBSERVATION,
        GoogleAdType.DISPLAY
    )

    type: str = mongoengine.StringField(required=True, default=GoogleAdType.SEARCH, choices=type_choice)
    campaign_id = mongoengine.LongField(default=None)
    ad_group_id = mongoengine.LongField(default=None)
    budget_id = mongoengine.LongField(default=None)
    ad_id = mongoengine.LongField(default=None)
    campaign_status: bool = mongoengine.BooleanField(required=True, default=False)


def get_google_default_ads():
    return [GoogleAdMapper()]


class GoogleCampaignMapper(mongoengine.EmbeddedDocument):

    ads: List[GoogleAdMapper] = mongoengine.EmbeddedDocumentListField(GoogleAdMapper, default=get_google_default_ads())
    locations: List[GoogleEntityMapper] = mongoengine.EmbeddedDocumentListField(GoogleEntityMapper)
    languages: List[GoogleEntityMapper] = mongoengine.EmbeddedDocumentListField(GoogleEntityMapper)
    keywords: List[GoogleEntityMapper] = mongoengine.EmbeddedDocumentListField(GoogleEntityMapper)
    interest: List[GoogleEntityMapper] = mongoengine.EmbeddedDocumentListField(GoogleEntityMapper)


class FacebookCampaignMapper(mongoengine.EmbeddedDocument):

    status_choice = (
        "PAUSED",
        "ACTIVE",
        "FINISHED"
    )
    campaign_id = mongoengine.StringField(default="")
    ad_set_id = mongoengine.StringField(default="")
    ad_ids = mongoengine.ListField(mongoengine.StringField(required=True))
    campaign_status = mongoengine.StringField(choices=status_choice, required=True, default="PAUSED")


class CampaignPlatformMapper(mongoengine.EmbeddedDocument):

    google: GoogleCampaignMapper = mongoengine.EmbeddedDocumentField(GoogleCampaignMapper, default=GoogleCampaignMapper())
    facebook: FacebookCampaignMapper = mongoengine.EmbeddedDocumentField(FacebookCampaignMapper, default=FacebookCampaignMapper())

