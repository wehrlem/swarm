import mongoengine


class Geometry(mongoengine.EmbeddedDocument):

    lattitude = mongoengine.DecimalField()
    longitude = mongoengine.DecimalField()
    south = mongoengine.DecimalField()
    west = mongoengine.DecimalField()
    north = mongoengine.DecimalField()
    east = mongoengine.DecimalField()
        