# Data Models
# from ..campaigns import Campaign as CampaignData
# from ..address_components import AddressComponent as AddressComponentData
from ..locations import Location as LocationData
from ..budgets import Budget as BudgetData
# from ..budgets import BudgetDistribution as BudgetDistributionData
from ..facebookads import FacebookAd as FacebookAdData
from ..instagramads import InstagramAd as InstagramAdData
from ..googleads import GoogleAd as GoogleAdData
from ..target_sets import TargetSet as TargetSetData

# Request Models
from ...requests import CampaignRequest
from ...requests.targeting import Targeting as TargetingRequest
from ...requests.location import Location as LocationRequest
from ...requests.location import AddressComponent as AddressComponentRequest
# from ...requests.location import Geometry as GeometryRequest
from ...requests.budget import Budget as BudgetRequest
from ...requests.budget import BudgetDistribution as BudgetDistributionRequest
from ...requests.budget import BudgetValue as BudgetValueRequest
from ...requests.ads import Ads as AdsRequest
from ...requests.ads import GoogleAd as GoogleAdRequest
from ...requests.ads import FacebookAd as FacebookAdRequest
from ...requests.targeting import TargetSet as TargetSetRequest

from ....services import data_services as db


def to_campaign_request(campaign):

    if campaign is None:
            return None
    request = CampaignRequest()
    if campaign.id is not None:
        request.id = str(campaign.id)
    if campaign.name is not None:
        request.name = campaign.name
    if campaign.start_date is not None:
        request.start_date = campaign.start_date
    if campaign.end_date is not None:
        request.end_date = campaign.end_date
    request.duration = campaign.duration_in_days
    if campaign.platforms is not None:
        for platform in campaign.platforms:
            request.platforms.append(platform)
    if campaign.targeting is not None:
        request.targeting = TargetingRequest()
    if campaign.targeting is not None:
        # Locations
        if campaign.targeting.locations is not None:
            for location in campaign.targeting.locations:
                request.targeting.locations.append(mapLocation(location))
        # Languages
        if campaign.targeting.languages is not None:
            for language in campaign.targeting.languages:
                request.targeting.languages.append(language)
        # Target set
        request.targeting.target_set = mapTargetSet(campaign.targeting.target_set)
    # Budget
    if campaign.budget is not None:
        request.budget = mapBudget(campaign.budget)
    # Ads
    request.ads = AdsRequest()
    # Facebook
    if campaign.facebookads is not None:
        for fbad in campaign.facebookads:
            request.ads.facebook.append(mapFacebookAds(fbad))
    # Instagram
    if campaign.instagramads is not None:
        for instad in campaign.instagramads:
            request.ads.instagram.append(mapInstagramAds(instad))
    # Google
    if campaign.googleads is not None:
        for googlead in campaign.googleads:
            request.ads.google.append(mapGoogleAds(googlead))

    return request


def mapTargetSet(target_set_id: TargetSetData):

    # Target set request
    tar_reqest = TargetSetRequest()

    if target_set_id is None:
        target_set = db.get_default_targetset()
    else:
        target_set = db.get_targetset_by_id(target_set_id)

    if target_set.gender is not None:
        for tmp in target_set.gender:
            tar_reqest.gender.append(tmp)
    if target_set.keywords is not None:
        for tmp in target_set.keywords:
            tar_reqest.keywords.append(tmp)
    if target_set.placements is not None:
        for tmp in target_set.placements:
            tar_reqest.placements.append(tmp)
    if target_set.interest is not None:
        for tmp in target_set.interest:
            tar_reqest.interest.append(tmp)
    if target_set.devices is not None:
        for tmp in target_set.devices:
            tar_reqest.devices.append(tmp)
    return tar_reqest


def mapLocation(location: LocationData) -> LocationRequest:
    loc = LocationRequest()
    for daddress in location.address_components:
        adr = AddressComponentRequest()
        adr.long_name = daddress.long_name
        adr.short_name = daddress.short_name
        adr.types = [x for x in daddress.types]
        loc.address_components.append(adr)
    return loc


def mapBudget(budget_data: BudgetData) -> BudgetRequest:

    if budget_data is None:
        return None

    # Budget info
    bdg = BudgetRequest()
    bdg.budget_optimization = budget_data.budget_optimization
    bdg.currency = budget_data.currency
    bdg.netto_budget = budget_data.netto_budget
    bdg.brutto_budget = budget_data.brutto_budget

    # Map Budget distribution
    bdg.budget_distribution = BudgetDistributionRequest()
    for tmp in budget_data.budget_distribution:
        budget_value = BudgetValueRequest()
        budget_value.ad_type = tmp.ad_type
        budget_value.netto_price = tmp.netto_price
        budget_value.percentage = tmp.percentage
        if "google" == tmp.platform:
            bdg.budget_distribution.google.append(budget_value)
        elif "facebook" == tmp.platform:
            bdg.budget_distribution.facebook.append(budget_value)
        elif "instagram" == tmp.platform:
            bdg.budget_distribution.instagram.append(budget_value)

    return bdg


def mapFacebookAds(facebookad: FacebookAdData):
    fbad = FacebookAdRequest()
    fbad.url = facebookad.url
    fbad.ad_type = facebookad.type
    fbad.title = facebookad.title
    fbad.message = facebookad.message
    if facebookad.descriptions is not None:
        for desc in facebookad.descriptions:
            fbad.descriptions.append(desc)
    if facebookad.marketing_images is not None:
        for image in facebookad.marketing_images:
            fbad.marketing_images.append(image)
    return fbad


def mapInstagramAds(instad: InstagramAdData):
    ad = FacebookAdRequest()
    ad.url = instad.url
    ad.ad_type = instad.type
    ad.title = instad.title
    ad.message = instad.message
    if instad.descriptions is not None:
        for desc in instad.descriptions:
            ad.descriptions.append(desc)
    if instad.marketing_images is not None:
        for image in instad.marketing_images:
            ad.marketing_images.append(image)
    return ad


def mapGoogleAds(google: GoogleAdData):
    ad = GoogleAdRequest()
    ad.url = google.url
    ad.ad_type = google.type
    ad.headline1 = google.headline1
    ad.headline2 = google.headline2
    ad.headline3 = google.headline3
    if google.descriptions is not None:
        for desc in google.descriptions:
            ad.descriptions.append(desc)
    return ad
