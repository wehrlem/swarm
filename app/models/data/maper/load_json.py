from ..campaigns import Campaign
from ..budgets import Budget
from ..budgetdistributions import BudgetDistribution
from ..targetings import Targeting
from ..locations import Location
# from ..geometries import Geometry
from ..address_components import AddressComponent
from ..googleads import GoogleAd
from ..instagramads import InstagramAd
from ..facebookads import FacebookAd
from datetime import datetime
from dateutil import parser
from typing import List


def set_budget(json) -> Budget:

    bdg = Budget()
    data = json
    bdg.currency = data['currency']
    bdg.brutto_budget = float(data['brutto_budget'])
    bdg.app_percentage = float(data['app_percentage'])
    bdg.budget_optimization = bool(data['budget_optimization'])

    platforms = ['google', 'facebook', 'instagram']
    if 'budget_distribution' in data:
        bdg.budget_distribution = []
        for platform in platforms:
            if platform in data['budget_distribution']:
                bdg_dist_data = data['budget_distribution'][platform]
                for tmp in bdg_dist_data:
                    bdgDistr = BudgetDistribution()
                    bdgDistr.netto_price = float(tmp['netto_price'])
                    bdgDistr.ad_type = tmp['ad_type']
                    bdgDistr.percentage = float(tmp['percentage'])
                    bdgDistr.platform = platform
                    bdg.budget_distribution.append(bdgDistr)
    return bdg


def set_targeting(targeting) -> Targeting:

    target = Targeting()

    # Location
    target.locations = []
    location_types = ['locality', 'country', 'postal_code']
    for location in targeting['locations']:
        loc = Location()
        loc.address_components = []
        for loc_type in location_types:
            if loc_type in location:
                adr_comp = AddressComponent()
                adr_comp.long_name = location[loc_type]
                adr_comp.types.append(loc_type)
                loc.address_components.append(adr_comp)
        target.locations.append(loc)

    # Languages
    target.languages = []
    if 'languages' in targeting:
        for lang in targeting['languages']:
            target.languages.append(lang)

    return target


def set_googleads(json) -> List[GoogleAd]:

    ads = []
    data = json

    for i in range(len(data)):
        ad_data = data[i]
        ad = GoogleAd()

        ad.type = ad_data['ad_type'].lower()
        ad.url = ad_data['url']

        ad.descriptions = []
        for x in ad_data['descriptions']:
            ad.descriptions.append(x)

        if ad.type == "search":
            ad.headline1 = ad_data['headline1']
            ad.headline2 = ad_data['headline2']
            ad.headline3 = ad_data['headline3']

        elif ad.type == "display":

            ad.long_headline = ad_data['long_headline']

            ad.marketing_images = []
            for x in ad_data['marketing_images']:
                ad.marketing_images.append(x)

            ad.short_headlines = []
            for x in ad_data['short_headlines']:
                ad.short_headlines.append(x)

            ad.square_marketing_images = []
            for x in ad_data['square_marketing_images']:
                ad.square_marketing_images.append(x)

            ad.logo_images = []
            for x in ad_data['logo_images']:
                ad.logo_images.append(x)

        ads.append(ad)

    return ads


def set_facebookads(json) -> List[FacebookAd]:

    ads = []
    data = json

    for i in range(len(data)):
        ad_data = data[i]
        ad = FacebookAd()

        ad.type = ad_data['ad_type'].lower()
        ad.url = ad_data['url']

        ad.descriptions = []
        for x in ad_data['descriptions']:
            ad.descriptions.append(x)

        ad.marketing_images = []
        for x in ad_data['marketing_images']:
            ad.marketing_images.append(x)

        ad.title = ad_data['title']
        ad.message = ad_data['message']

        ads.append(ad)

    return ads


def set_instagramads(json) -> List[InstagramAd]:
    ads = []
    data = json

    for i in range(len(data)):
        ad_data = data[i]
        ad = InstagramAd()

        ad.type = ad_data['ad_type'].lower()
        ad.url = ad_data['url']

        ad.descriptions = []
        for x in ad_data['descriptions']:
            ad.descriptions.append(x)

        ad.marketing_images = []
        for x in ad_data['marketing_images']:
            ad.marketing_images.append(x)

        ad.title = ad_data['title']
        ad.message = ad_data['message']

        ads.append(ad)

    return ads


def load_json(camp: Campaign, message):

    if 'name' in message:
        camp.name = message['name']
    if 'description' in message:
        camp.description = message['description']
    if 'start_date' in message:
        camp.start_date = parser.parse(message['start_date'])
    if 'end_date' in message:
        camp.end_date = parser.parse(message['end_date'])
    camp.update_date = datetime.utcnow()
    if 'platforms' in message:
        camp.platforms = []
        for platform in message['platforms']:
                camp.platforms.append(platform)
    if 'budget' in message:
        camp.budget = set_budget(message['budget'])
    if 'targeting' in message:
        camp.targeting = set_targeting(message['targeting'])
    if 'ads' in message:
        ads_json = message['ads']
        camp.facebookads = []
        if 'facebook' in ads_json and 'facebook' in camp.platforms:
            camp.facebookads = set_facebookads(ads_json['facebook'])
        camp.googleads = []
        if 'google' in ads_json and 'google' in camp.platforms:
            camp.googleads = set_googleads(ads_json['google'])
        camp.instagramads = []
        if 'instagram' in ads_json and 'instagram' in camp.platforms:
            camp.instagramads = set_instagramads(ads_json['instagram'])
