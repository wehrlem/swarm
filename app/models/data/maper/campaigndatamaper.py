from ..campaigns import Campaign as CampaignData
# from ...requests import CampaignRequest
from ....services import data_services as db
from .load_json import load_json as camp_load_json
from .to_campaign_request import to_campaign_request


class CampaignDataMaper(object):

    @classmethod
    def load_json(cls, message):
        if 'id' in message:
            camp = db.get_campaign(message['id'])
        else:
            camp = CampaignData()
        # Load JSON to Campaign
        camp_load_json(camp, message)
        # Get default TargetSet
        camp.targeting.target_set = db.get_targetset_default().id
        return camp

    @classmethod
    def ToCampaignRequest(cls, campaign: CampaignData):
        return to_campaign_request(campaign)
