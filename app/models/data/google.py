import mongoengine


class GoogleLang(mongoengine.Document):

    lang_id = mongoengine.IntField(required=True)
    code = mongoengine.StringField()
    name = mongoengine.StringField(required=True)

    meta = {
        'db_alias': 'core',
        'collection': 'google_lang',
    }


class GoogleInterest(mongoengine.Document):

    interest_id = mongoengine.LongField(required=True)
    parent_id = mongoengine.LongField(required=True)
    name = mongoengine.StringField(required=True)
    type = mongoengine.StringField(required=True)

    meta = {
        'db_alias': 'core',
        'collection': 'google_interest',

        'indexes': [
            {'fields': ('interest_id', 'parent_id', 'type'), 'unique': True}
        ]
    }
