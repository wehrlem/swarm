import mongoengine
from app.models.data.campaigns import Campaign
from app.models.data.common.configs import CampaignPlatformChoices


class StatisticData(mongoengine.EmbeddedDocument):
    clicks = mongoengine.IntField(default=0)
    impressions = mongoengine.IntField(default=0)
    averagecpc = mongoengine.FloatField(default=0.0)
    cost = mongoengine.FloatField(default=0)


class Statistic(mongoengine.Document):

    campaign: Campaign = mongoengine.ReferenceField(Campaign, required=True, unique=True)
    google: StatisticData = mongoengine.EmbeddedDocumentField(StatisticData, default=StatisticData())
    facebook: StatisticData = mongoengine.EmbeddedDocumentField(StatisticData, default=StatisticData())

    @property
    def clicks(self):
        result = self.google.clicks + self.facebook.clicks
        return result

    @property
    def impressions(self):
        result = self.google.impressions + self.facebook.impressions
        return result

    @property
    def cost(self):
        result = self.google.cost + self.facebook.cost
        return result

    def ToJson(self):

        budget = None
        if self.campaign is not None and self.campaign.budget is not None:
            budget = self.campaign.budget.netto_budget

        json = {
            'campaign_id': str(self.campaign.id),
            'name': self.campaign.name,
            'all_platform': {
                'budget': budget,
                'click': self.clicks,
                'impressions': self.impressions,
                'cost': self.cost,
            }
        }

        for platform_name in self.campaign.platforms:

            # Get platform budget
            platform_netto_price = None
            if budget is not None:
                platform_budget = self.campaign.budget.budget_distribution.filter(platform=platform_name).first()
                if platform_budget is not None:
                    platform_netto_price = platform_budget.netto_price

            # Get platform statistic
            platform: StatisticData = None
            if platform_name == CampaignPlatformChoices.GOOGLE:
                platform = self.google

            if platform is not None:
                platform_stat = {
                    'netto_price': platform_netto_price,
                    'click': platform.clicks,
                    'impressions': platform.impressions,
                    'averagecpc': platform.averagecpc,
                    'cost': platform.cost,
                }
                json[platform_name] = platform_stat

        return json

    meta = {
        'db_alias': 'core',
        'collection': 'statistics'
    }
