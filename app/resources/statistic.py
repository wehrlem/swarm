import falcon
# from webargs import fields
# from webargs.falconparser import use_args
import app.database as db
from app.services.ad_creator import AdCreator


class StatisticRequest(object):

    def __init__(self):
        self.adcreator = AdCreator()

    def on_get(self, req, resp, campaign_id):

        stat = self.adcreator.GetStatistic(campaign_id)
        response = stat.ToJson()
        resp.context["result"] = response
        resp.status = falcon.HTTP_200
