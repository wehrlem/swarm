import os
import falcon
# import app.util.json as json
from falcon_jinja2 import FalconTemplate
falcon_template = FalconTemplate(path=os.path.dirname(os.path.realpath(__file__)) + '/templates')


class RootResources(object):

    @falcon_template.render('index.html')
    def on_get(self, req, resp):
        resp.context = {'framework': 'Falcon'}


class PingResources(object):

    def on_get(self, req, resp):
        resp.status = falcon.HTTP_200
        resp.context['result'] = {'message': 'ACK'}
