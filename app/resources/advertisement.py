import falcon
import logging
from datetime import datetime
from mongoengine.queryset.visitor import Q
from webargs.falconparser import use_args

# import app.database as db
# from app.util.use_args_with import use_args_with

from app.models.data.campaigns import Campaign

# from app.adplatform.ad_manager import AdPlatformManager

from app.resources.schemas.advertisement_schema import AdvertisementListSchema
from app.resources.schemas.advertisement import AdvertisementUpdateSchema
from app.resources.schemas.campaign_schema import CampaignSchema

ad_info_logger = logging.getLogger('ads_logger')


class AdvertisementCreateAPI(object):

    """
            Create advertisements
    """

    def update_campaigns(self, data, user):
        filters = None
        mapper = {}
        for x in data:
            project = x["project_id"]
            apartment_id = x["apartment"]["reference_id"]
            key = f'{project}-{apartment_id}'
            if key in mapper:
                raise falcon.HTTPBadRequest(f"Apartment: '{apartment_id}' are duplicated!")
            mapper[key] = x
            if filters is None:
                filters = (Q(project_id=project) & Q(apartment__reference_id=apartment_id))
            else:
                filters = filters | (Q(project_id=project) & Q(apartment__reference_id=apartment_id))

        campaigns = Campaign.objects.filter(account_id=user).filter(filters)

        results = []
        for x in campaigns:
            key = f'{x.project_id}-{x.apartment.reference_id}'
            if key in mapper:
                x.update(**mapper[key])
                x.update_date = datetime.now()
                x.save()
                results.append(x)
                mapper.pop(key)
        for key, data in mapper.items():
            x = Campaign(**data)
            x.update_platforms()
            x.account_id = user
            x.update_date = datetime.now()
            x.save()
            results.append(x)
        return results

    @use_args(CampaignSchema(strict=True, many=True))
    def on_post(self, req, resp, data):

        user = req.context['user']
        camps = self.update_campaigns(data, user)

        log_msg = f'[{user.email}] [create] {data}'
        ad_info_logger.info(log_msg)

        # Push campaigns to platforms
        image_pub = req.context['image_pub']
        image_pub.send(user, camps)

        resp.status = falcon.HTTP_200
        resp.context["result"] = {
            "message": "ACK"
        }


class AdvertisementUpdateApi(object):
    '''
        Advertisements update
    '''

    @use_args(AdvertisementUpdateSchema(strict=True, many=False))
    def on_post(self, req, resp, args, project_id, reference_id):
        user = req.context['user']
        image_pub = req.context['image_pub']
        campaign = Campaign.objects(account_id=user, project_id=project_id, apartment__reference_id=reference_id).first()

        log_msg = f'[{user.email}] [update] [{project_id}] [{reference_id}] {args}'
        ad_info_logger.info(log_msg)

        camps = []
        if campaign:
            campaign.update(**args)
            campaign.save()
            camps.append(campaign)
            image_pub.send(user, camps)

        resp.status = falcon.HTTP_200
        resp.context["result"] = {'message': 'ACK'}


class AdvertisementDeactivateApi(object):
    '''
        Advertisement deactivate
    '''
    def on_post(self, req, resp, project_id, reference_id):
        user = req.context['user']
        image_pub = req.context['image_pub']
        apartment: Campaign = Campaign.objects(account_id=user).filter(Q(apartment__reference_id=reference_id) & Q(project_id=project_id)).first()
        camps = []
        if apartment:
            apartment.deactivate()
            camps.append(apartment)
            # ad_manager = AdPlatformManager()
            # ad_manager.publish(user)
            image_pub.send(user, camps)
        resp.status = falcon.HTTP_200
        resp.context["result"] = {'message': 'ACK'}


class AdvertisementRefreshApi(object):
    '''
        Advertisement refresh
    '''

    def on_post(self, req, resp, project_id, reference_id):
        user = req.context['user']
        image_pub = req.context['image_pub']
        apartment: Campaign = Campaign.objects(account_id=user).filter(Q(apartment__reference_id=reference_id) & Q(project_id=project_id)).first()
        camps = []
        if apartment:
            apartment.refresh += 1
            apartment.save()
            camps.append(apartment)
            # ad_manager = AdPlatformManager()
            # ad_manager.publish(user)
            image_pub.send(user, camps)
        resp.status = falcon.HTTP_200
        resp.context["result"] = {'message': 'ACK'}


class AdvertisementImportApi(object):
    """
            Advertisements import
    """

    @use_args(CampaignSchema(strict=True, many=True))
    def on_post(self, req, resp, data):

        user = req.context['user']
        self.update_campaigns(data, user)

        resp.status = falcon.HTTP_200
        resp.context["result"] = {
            "message": "ACK"
        }


class AdvertisementsListApi(object):

    def on_get(self, req, resp, project_id):
        """
            Get all Campaigns by project_id
        """

        # User
        user = req.context['user']

        active_platforms = user.getActivePlatforms()

        camps = Campaign.objects(account_id=user, project_id=project_id).get_published(active_platforms)
        res_data, _ = AdvertisementListSchema(many=True, context={"platforms": active_platforms}).dump(camps)

        resp.status = falcon.HTTP_200
        resp.context["result"] = res_data


class AdvertisementDeleteApi(object):

    def on_delete(self, req, resp, project_id, reference_id):
        """
            Delete Campaign by ID
        """

        user = req.context['user']
        camp = Campaign.objects.filter(account_id=user.id, project_id=project_id, apartment__reference_id=reference_id).first()
        if not camp:
            raise falcon.HTTPBadRequest(f"Advertisement with project: {project_id}, and reference_id: {reference_id} don't exists!")

        camp.delete()

        result_data = {
            "message": "Advertisement with project: '{project_id}' and reference_id: '{reference_id}' was sucessfuly deleted!"
        }

        resp.status = falcon.HTTP_200
        resp.context["result"] = result_data
