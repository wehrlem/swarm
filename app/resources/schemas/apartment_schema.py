from marshmallow import Schema, fields, post_load, pre_load
from marshmallow.validate import Length, Range, Regexp, OneOf
from app.models.data.apartment import Apartment

# categories_choices = ('apartment', 'atelier', 'attic-flat', 'bar', 'cafe', 'car-repair-shop', 'castle', 'chalet', 'earth-sheltered-dwelling', 'detached-house', 'doctors-office', 'duplex-house', 'farm-house', 'furnished-flat', 'granny-flat', 'hobby-room', 'hotel', 'loft', 'maisonette', 'multiplex-house', 'office', 'one-room-flat', 'restaurant', 'retail-location', 'roof-flat', 'row-house', 'rustico', 'single-room', 'stoeckli', 'stepped-flat', 'stepped-house', 'villa', 'workshop', 'parking-space')
type_applicable = ('applies', 'does-not-apply', 'unknown')
type_development = ('full', 'partial', 'undeveloped')
type_heating_distribution = ('radiator', 'floor')
type_heating_generation = ('district', 'electricity', 'gas', 'geothermal-probe', 'heatpump-air-and-water', 'heatpump', 'oil', 'photovoltaics', 'solar-thermal', 'wood-pellet', 'wood')
type_minergie_certification = ('Minergie', 'Minergie-P', 'Minergie-Eco', 'Minergie-P-Eco')
type_price_interval = ('onetime', 'day', 'week', 'month', 'year')
type_price_referring = ('all', 'm2', 'km2')
type_deposit = ('bank-guarantee', 'deposit-guarantee')
type_offery_type = ('parking-exterior-space', 'parking-carport', 'parking-garage', 'parking-garage-connected', 'parking-garage-box', 'parking-garage-underground', 'parking-house', 'parking-duplex', 'parking-double-garage', 'room-workroom', 'room-storage-basement')
type_rent_type = ('buy', 'rent')
type_utilization = ('agricultural', 'commercial', 'construction', 'gastronomy', 'industrial', 'investment', 'parking', 'residential', 'storage', 'vacation')


class GeoCoordinatesSchema(Schema):
    latitude = fields.Float(allow_none=True)
    longitude = fields.Float(allow_none=True)
    elevation = fields.Float(allow_none=True)


# Validacija
# 1. option: CountryCode, locality - required
# 2. option: Geo - required
# 3. option: ???

DATE_FORMAT = '%d.%m.%Y'
DATE_TIME_FORMAT_UTC = "%Y-%m-%dT%H:%M:%SZ"


class BaseAddressSchema(Schema):
    country_code = fields.String(allow_none=True)
    locality = fields.String(allow_none=True)
    region = fields.String(allow_none=True)
    postal_code = fields.String(allow_none=True)
    post_office_box_number = fields.String(allow_none=True)
    street = fields.String(allow_none=True)
    street_number = fields.String(allow_none=True)
    street_addition = fields.String(allow_none=True)
    geo = fields.Nested(GeoCoordinatesSchema)


class AddressSchema(BaseAddressSchema):
    country_code = fields.String(validate=[Regexp("[A-Z]{2}", 0), Length(min=2, max=2)])
    locality = fields.String()
    # region = fields.String(allow_none=True)
    # postal_code = fields.String(allow_none=True)
    # post_office_box_number = fields.String(allow_none=True)
    # street = fields.String(allow_none=True)
    # street_number = fields.String(allow_none=True)
    # street_addition = fields.String(allow_none=True)
    # geo = fields.Nested(GeoCoordinatesSchema)


class AvailabilitySchema(Schema):
    start = fields.DateTime(allow_none=True, format=DATE_FORMAT)
    expiration = fields.DateTime(allow_none=True, format=DATE_FORMAT)
    value = fields.String(required=False)


class BsfSchema(Schema):
    egid = fields.String()
    ewid = fields.String()


class CustomCaracteristicSchema(Schema):
    key = fields.Str(required=True)
    value = fields.Str(required=True)


class CharacteristicsSchema(Schema):

    area_bwf = fields.Float(validate=Range(min=0), allow_none=True)
    area_nwf = fields.Float(validate=Range(min=0), allow_none=True)
    area_property_land = fields.Float(validate=Range(min=0), allow_none=True)
    area_sia_aff = fields.Float(validate=Range(min=0), allow_none=True)
    area_sia_agf = fields.Float(validate=Range(min=0), allow_none=True)
    area_sia_akf = fields.Float(validate=Range(min=0), allow_none=True)
    area_sia_akfn = fields.Float(validate=Range(min=0), allow_none=True)
    area_sia_akft = fields.Float(validate=Range(min=0), allow_none=True)
    area_sia_anf = fields.Float(validate=Range(min=0), allow_none=True)
    area_sia_angf = fields.Float(validate=Range(min=0), allow_none=True)
    area_sia_avf = fields.Float(validate=Range(min=0), allow_none=True)
    area_sia_buf = fields.Float(validate=Range(min=0), allow_none=True)
    area_sia_ff = fields.Float(validate=Range(min=0), allow_none=True)
    area_sia_gf = fields.Float(validate=Range(min=0), allow_none=True)
    area_sia_ggf = fields.Float(validate=Range(min=0), allow_none=True)
    area_sia_gsf = fields.Float(validate=Range(min=0), allow_none=True)
    area_sia_hnf = fields.Float(validate=Range(min=0), allow_none=True)
    area_sia_kf = fields.Float(validate=Range(min=0), allow_none=True)
    area_sia_kfn = fields.Float(validate=Range(min=0), allow_none=True)
    area_sia_kft = fields.Float(validate=Range(min=0), allow_none=True)
    area_sia_nf = fields.Float(validate=Range(min=0), allow_none=True)
    area_sia_ngf = fields.Float(validate=Range(min=0), allow_none=True)
    area_sia_nnf = fields.Float(validate=Range(min=0), allow_none=True)
    area_sia_uf = fields.Float(validate=Range(min=0), allow_none=True)
    area_sia_uuf = fields.Float(validate=Range(min=0), allow_none=True)
    area_sia_vf = fields.Float(validate=Range(min=0), allow_none=True)

    are_pets_allowed = fields.String(validate=OneOf(choices=type_applicable), allow_none=True)

    ceiling_height = fields.Float(validate=Range(min=0), allow_none=True)
    crane_capacity = fields.Float(validate=Range(min=0), allow_none=True)
    elevator_load = fields.Float(validate=Range(min=0), allow_none=True)

    floor = fields.Integer(allow_none=True)

    floor_load = fields.Float(validate=Range(min=0), allow_none=True)
    gross_premium = fields.Float(validate=Range(min=0, max=100), allow_none=True)
    hall_height = fields.Float(validate=Range(min=0), allow_none=True)

    has_attic = fields.String(validate=OneOf(choices=type_applicable), allow_none=True)
    has_balcony = fields.String(validate=OneOf(choices=type_applicable), allow_none=True)
    has_building_law_restrictions = fields.String(validate=OneOf(choices=type_applicable), allow_none=True)
    has_cable_tv = fields.String(validate=OneOf(choices=type_applicable), allow_none=True)
    has_car_port = fields.String(validate=OneOf(choices=type_applicable), allow_none=True)
    has_car_port_double = fields.String(validate=OneOf(choices=type_applicable), allow_none=True)
    has_cellar = fields.String(validate=OneOf(choices=type_applicable), allow_none=True)
    has_charging_station = fields.String(validate=OneOf(choices=type_applicable), allow_none=True)
    has_connected_building_land = fields.String(validate=OneOf(choices=type_applicable), allow_none=True)
    has_demolition_property = fields.String(validate=OneOf(choices=type_applicable), allow_none=True)
    has_dishwasher = fields.String(validate=OneOf(choices=type_applicable), allow_none=True)
    has_elevator = fields.String(validate=OneOf(choices=type_applicable), allow_none=True)
    has_fireplace = fields.String(validate=OneOf(choices=type_applicable), allow_none=True)
    has_flat_sharing_community = fields.String(validate=OneOf(choices=type_applicable), allow_none=True)
    has_foreign_quota = fields.String(validate=OneOf(choices=type_applicable), allow_none=True)
    has_garage = fields.String(validate=OneOf(choices=type_applicable), allow_none=True)
    has_garage_double = fields.String(validate=OneOf(choices=type_applicable), allow_none=True)
    has_garage_underground = fields.String(validate=OneOf(choices=type_applicable), allow_none=True)
    has_garden_shed = fields.String(validate=OneOf(choices=type_applicable), allow_none=True)
    has_lake_view = fields.String(validate=OneOf(choices=type_applicable), allow_none=True)
    has_lifting_platform = fields.String(validate=OneOf(choices=type_applicable), allow_none=True)
    has_mountain_view = fields.String(validate=OneOf(choices=type_applicable), allow_none=True)
    has_nice_view = fields.String(validate=OneOf(choices=type_applicable), allow_none=True)
    has_parking = fields.String(validate=OneOf(choices=type_applicable), allow_none=True)
    has_photovoltaic = fields.String(validate=OneOf(choices=type_applicable), allow_none=True)
    has_playground = fields.String(validate=OneOf(choices=type_applicable), allow_none=True)
    has_ramp = fields.String(validate=OneOf(choices=type_applicable), allow_none=True)
    has_steamer = fields.String(validate=OneOf(choices=type_applicable), allow_none=True)
    has_store_room = fields.String(validate=OneOf(choices=type_applicable), allow_none=True)
    has_supply_gas = fields.String(validate=OneOf(choices=type_applicable), allow_none=True)
    has_supply_power = fields.String(validate=OneOf(choices=type_applicable), allow_none=True)
    has_supply_sewage = fields.String(validate=OneOf(choices=type_applicable), allow_none=True)
    has_supply_water = fields.String(validate=OneOf(choices=type_applicable), allow_none=True)
    has_swimming_pool = fields.String(validate=OneOf(choices=type_applicable), allow_none=True)
    has_thermal_solar_collector = fields.String(validate=OneOf(choices=type_applicable), allow_none=True)
    has_tiled_stove = fields.String(validate=OneOf(choices=type_applicable), allow_none=True)
    has_tumble_dryer = fields.String(validate=OneOf(choices=type_applicable), allow_none=True)
    has_washing_machine = fields.String(validate=OneOf(choices=type_applicable), allow_none=True)
    is_child_friendly = fields.String(validate=OneOf(choices=type_applicable), allow_none=True)
    is_corner_house = fields.String(validate=OneOf(choices=type_applicable), allow_none=True)
    is_demolition_property = fields.String(validate=OneOf(choices=type_applicable), allow_none=True)
    is_dilapidated = fields.String(validate=OneOf(choices=type_applicable), allow_none=True)
    is_first_occupancy = fields.String(validate=OneOf(choices=type_applicable), allow_none=True)
    is_ground_floor = fields.String(validate=OneOf(choices=type_applicable), allow_none=True)
    is_ground_floor_raised = fields.String(validate=OneOf(choices=type_applicable), allow_none=True)
    is_gutted = fields.String(validate=OneOf(choices=type_applicable), allow_none=True)
    is_inNeed_of_renovation = fields.String(validate=OneOf(choices=type_applicable), allow_none=True)
    is_inNeed_of_renovation_partially = fields.String(validate=OneOf(choices=type_applicable), allow_none=True)
    is_like_new = fields.String(validate=OneOf(choices=type_applicable), allow_none=True)
    is_middle_house = fields.String(validate=OneOf(choices=type_applicable), allow_none=True)
    is_modernized = fields.String(validate=OneOf(choices=type_applicable), allow_none=True)
    is_new_construction = fields.String(validate=OneOf(choices=type_applicable), allow_none=True)
    is_old_building = fields.String(validate=OneOf(choices=type_applicable), allow_none=True)
    is_projection = fields.String(validate=OneOf(choices=type_applicable), allow_none=True)
    is_quiet = fields.String(validate=OneOf(choices=type_applicable), allow_none=True)
    is_refurbis_hed = fields.String(validate=OneOf(choices=type_applicable), allow_none=True)
    is_refurbis_hed_partially = fields.String(validate=OneOf(choices=type_applicable), allow_none=True)
    is_secondary_residence_allowed = fields.String(validate=OneOf(choices=type_applicable), allow_none=True)
    is_shell_construction = fields.String(validate=OneOf(choices=type_applicable), allow_none=True)
    is_smoking_allowed = fields.String(validate=OneOf(choices=type_applicable), allow_none=True)
    is_sunny = fields.String(validate=OneOf(choices=type_applicable), allow_none=True)
    is_under_roof = fields.String(validate=OneOf(choices=type_applicable), allow_none=True)
    is_well_tended = fields.String(validate=OneOf(choices=type_applicable), allow_none=True)
    is_wheelchair_accessible = fields.String(validate=OneOf(choices=type_applicable), allow_none=True)
    is_minergie_certified = fields.String(validate=OneOf(choices=type_applicable), allow_none=True)
    is_minergie_general = fields.String(validate=OneOf(choices=type_applicable), allow_none=True)

    number_of_apartements = fields.Integer(validate=Range(min=0), allow_none=True)
    number_of_bathrooms = fields.Integer(validate=Range(min=0), allow_none=True)
    number_of_floors = fields.Integer(validate=Range(min=0), allow_none=True)
    number_of_parcels = fields.Integer(validate=Range(min=0), allow_none=True)
    number_of_rooms = fields.Float(validate=Range(min=0), allow_none=True)
    number_of_showers = fields.Integer(validate=Range(min=0), allow_none=True)
    number_of_toilets = fields.Integer(validate=Range(min=0), allow_none=True)
    number_of_toilets_guest = fields.Integer(validate=Range(min=0), allow_none=True)

    on_even_ground = fields.String(validate=OneOf(choices=type_applicable), allow_none=True)
    on_hillside = fields.String(validate=OneOf(choices=type_applicable), allow_none=True)
    on_hillside_south = fields.String(validate=OneOf(choices=type_applicable), allow_none=True)

    utilization_ratio = fields.Float(validate=Range(min=0, max=1), allow_none=True)
    utilization_ratio_construction = fields.Float(validate=Range(min=0, max=1), allow_none=True)

    volume_gva = fields.Float(validate=Range(min=0), allow_none=True)
    volume_sia = fields.Float(validate=Range(min=0), allow_none=True)
    volume_sia_afv = fields.Float(validate=Range(min=0), allow_none=True)
    volume_sia_akv = fields.Float(validate=Range(min=0), allow_none=True)
    volume_sia_angv = fields.Float(validate=Range(min=0), allow_none=True)
    volume_sia_anv = fields.Float(validate=Range(min=0), allow_none=True)
    volume_sia_avv = fields.Float(validate=Range(min=0), allow_none=True)
    volume_sia_gv = fields.Float(validate=Range(min=0), allow_none=True)

    year_built = fields.Integer(allow_none=True)
    year_last_renovated = fields.Integer(allow_none=True)

    # IDX fields
    has_isdn = fields.String(validate=OneOf(choices=type_applicable), allow_none=True)
    has_railway_terminal = fields.String(validate=OneOf(choices=type_applicable), allow_none=True)
    has_restrooms = fields.String(validate=OneOf(choices=type_applicable), allow_none=True)
    is_under_building_laws = fields.String(validate=OneOf(choices=type_applicable), allow_none=True)

    custom_fields = fields.List(fields.Nested(CustomCaracteristicSchema))


class HeatingSchema(Schema):
    generation = fields.String(validate=OneOf(choices=type_heating_generation))
    distribution = fields.String(validate=OneOf(choices=type_heating_distribution))


# Attachment
class ImageSchema(Schema):
    url = fields.String(allow_none=True)
    title = fields.String(allow_none=True)
    description = fields.String(allow_none=True)
    mime_type = fields.String(validate=Length(min=7))


class LogoSchema(Schema):
    url = fields.String(allow_none=True)
    mime_type = fields.String(allow_none=True)


class LinkSchema(Schema):
    url = fields.String(allow_none=True)
    title = fields.String(allow_none=True)


class DocumentSchema(Schema):
    url = fields.String(allow_none=True)
    title = fields.String(allow_none=True)
    mime_type = fields.String(allow_none=True)


class AttachmentsSchema(Schema):
    images = fields.List(fields.Nested(ImageSchema), allow_none=True)
    documents = fields.List(fields.Nested(DocumentSchema), allow_none=True)
    links = fields.List(fields.Nested(LinkSchema), allow_none=True)
    link_directs = fields.List(fields.Nested(LinkSchema), allow_none=True)
    link_virtual_tours = fields.List(fields.Nested(LinkSchema), allow_none=True)
    link_youtubes = fields.List(fields.Nested(LinkSchema), allow_none=True)
    logos = fields.List(fields.Nested(LogoSchema), allow_none=True)
    plans = fields.List(fields.Nested(DocumentSchema), allow_none=True)


class EventSchema(Schema):
    name = fields.String(required=False)
    start = fields.DateTime(required=True, format=DATE_TIME_FORMAT_UTC)
    end = fields.DateTime(required=False, format=DATE_TIME_FORMAT_UTC)
    location = fields.String()
    summary = fields.String()


class LocalizationSchema(Schema):
    name = fields.String(required=True)
    lang = fields.String(required=True)
    excerpt = fields.String(allow_none=True)
    description = fields.String(allow_none=True)
    location = fields.String(allow_none=True)
    equipment = fields.String(allow_none=True)
    attachments = fields.Nested(AttachmentsSchema)
    visit_information = fields.String(allow_none=True)
    events = fields.List(fields.Nested(EventSchema))

    @post_load()
    def filter_description(self, in_data, **kwargs):
        try:
            if in_data and "description" in in_data:
                in_data["description"] = in_data["description"].replace('\r\n', '<br>')
                in_data["description"] = in_data["description"].replace('\r', '<br>')
                in_data["description"] = in_data["description"].replace('\n', '<br>')
        except Exception:
            pass
        return in_data


class RentSchema(Schema):
    # interval = fields.String(validate=OneOf(choices=type_price_interval))
    # referring = fields.String(validate=OneOf(choices=type_price_referring))
    gross = fields.Integer(validate=Range(min=0), allow_none=True)
    extra = fields.Integer(validate=Range(min=0), allow_none=True)
    net = fields.Integer(validate=Range(min=0), allow_none=True)
    price_unit = fields.Str(allow_none=True, missing="per_month")


class BuySchema(Schema):
    # referring = fields.String(validate=OneOf(choices=type_price_referring))
    price = fields.Integer(validate=Range(min=0), allow_none=True)
    # extra = fields.Integer(validate=Range(min=0))
    # wir_percentage = fields.Float(validate=Range(min=0, max=100))


class DepositSchema(Schema):
    type = fields.String(validate=OneOf(choices=type_deposit))
    value = fields.Integer(validate=Range(min=0), allow_none=True)


class AuctionSchema(Schema):
    start = fields.DateTime(required=True, format=DATE_TIME_FORMAT_UTC)
    expiration = fields.DateTime(required=True, format=DATE_TIME_FORMAT_UTC)
    value = fields.Integer(validate=Range(min=0))


class OfferSchema(Schema):
    interval = fields.String(validate=OneOf(choices=type_price_interval), required=True)
    type = fields.String(validate=OneOf(choices=type_offery_type), required=True)
    value = fields.Integer(validate=Range(min=0), required=True)


class PricesSchema(Schema):
    currency = fields.Str(required=True, validate=Length(min=3, max=3))
    rent = fields.Nested(RentSchema)
    buy = fields.Nested(BuySchema)
    # auction = fields.Nested(AuctionSchema)
    # deposit = fields.Nested(DepositSchema)
    # additional_offers = fields.List(fields.Nested(OfferSchema))


class OptionSchema(Schema):
    key = fields.String(required=True)
    value = fields.String(required=True)
    expiration = fields.DateTime(required=True, format=DATE_TIME_FORMAT_UTC)
    lang = fields.String(required=True)
    start = fields.DateTime(required=True, format=DATE_TIME_FORMAT_UTC)


class PublisherSchema(Schema):
    id = fields.String(required=True)
    options = fields.List(fields.Nested(OptionSchema), required=True, validate=Length(min=1))


class OrganizationSchema(Schema):
    id = fields.String(allow_none=True)
    address = fields.Nested(BaseAddressSchema, allow_none=True)
    brand = fields.String(allow_none=True)
    email = fields.String(allow_none=True)
    email_rem = fields.String(allow_none=True)
    legal_name = fields.String(allow_none=True)
    mobile = fields.String(allow_none=True)
    phone = fields.String(allow_none=True)
    website = fields.String(allow_none=True)

    @pre_load
    def filter_email(self, in_data, **kwargs):
        if "email" in in_data:
            in_data["email"] = None if in_data["email"] == "" else in_data["email"]
        if "email_rem" in in_data:
            in_data["email_rem"] = None if in_data["email_rem"] == "" else in_data["email_rem"]
        return in_data


class PersonSchema(Schema):
    function = fields.String(allow_none=True)
    given_name = fields.String(allow_none=True)
    family_name = fields.String(allow_none=True)
    email = fields.String(allow_none=True)
    mobile = fields.String(allow_none=True)
    phone = fields.String(allow_none=True)
    gender = fields.String(allow_none=True)
    note = fields.String(allow_none=True)

    @pre_load
    def filter_email(self, in_data, **kwargs):
        if "email" in in_data:
            in_data["email"] = None if in_data["email"] == "" else in_data["email"]
        return in_data


class SellerSchema(Schema):
    organization = fields.Nested(OrganizationSchema)
    contact_person = fields.Nested(PersonSchema, allow_none=True)
    inquiry_person = fields.Nested(PersonSchema, allow_none=True)
    visit_person = fields.Nested(PersonSchema, allow_none=True)


class ApartmentSchema(Schema):
    address = fields.Nested(AddressSchema)
    author = fields.String()
    availability = fields.Nested(AvailabilitySchema, required=True)
    bsf = fields.Nested(BsfSchema)
    building_zones = fields.String()
    # categories = fields.List(fields.String(validate=OneOf(choices=categories_choices)), required=True, validate=Length(min=1))
    category = fields.String(required=True)
    object_type = fields.String(allow_none=True)
    characteristics = fields.Nested(CharacteristicsSchema, required=True)
    created = fields.DateTime(dump_only=True)
    development = fields.String(validate=OneOf(choices=type_development))
    heating = fields.Nested(HeatingSchema)
    localizations = fields.List(fields.Nested(LocalizationSchema), required=True, validate=Length(min=1))
    minergie_certification = fields.String(choices=type_minergie_certification)
    modified = fields.Date(dump_only=True)
    parcel_numbers = fields.String(allow_none=True)
    prices = fields.Nested(PricesSchema, required=True)
    # publishers = fields.List(fields.Nested(PublisherSchema), validate=Length(min=1))
    reference_id = fields.String(required=True)
    seller = fields.Nested(SellerSchema)
    type = fields.String(required=True, validate=OneOf(choices=Apartment.AdType.CHOICES))
    # utilizations = fields.List(fields.String(validate=[OneOf(choices=type_utilization), Length(min=1)]))
    visual_reference_id = fields.String(allow_none=True)
