import logging
import falcon
from marshmallow import Schema, fields, post_load, post_dump

from app.models.data.campaigns import Campaign
from app.models.data.common.configs import CampaignPlatformChoices
from app.resources.schemas.campaign_schema import PlatformsSchema, BaseCampaignSchema
from app.models.data.platforms import Platform


class AdvertisementSchema(Schema):

    platforms = fields.Nested(PlatformsSchema, required=True, many=True)
    campaigns = fields.Nested(BaseCampaignSchema, required=True, many=True)

    @post_load()
    def create_campaigns(self, data, **kwargs):

        projects = {}
        new_campaigns = {}
        error_duplicate_list = []
        for x in data["campaigns"]:
            x["platforms"] = data["platforms"]

            project = x["project_id"]
            apartment_id = x["apartment"]["reference_id"]

            # Add in projects
            if project not in projects:
                projects[project] = []
                new_campaigns[project] = {}
            projects[project].append(apartment_id)

            # Add in campaign
            if apartment_id in new_campaigns[project]:
                tmp = "{id}.{project}".format(project=project, id=apartment_id)
                error_duplicate_list.append(tmp)
            else:
                new_campaigns[project][apartment_id] = x

        if len(error_duplicate_list) > 0:
            raise falcon.HTTPBadRequest("Apartments [{}] are duplicated!".format(", ".join(error_duplicate_list)))

        update_list = []

        # Get get Campaigns
        campaigns = Campaign.objects.get_campaigns(projects)

        # If Campaign is active raise exception
        for x in campaigns:
            # Add to Results
            update_list.append({
                "campaign": x,
                "update": new_campaigns[x.project_id][x.apartment.reference_id]
            })
            # Remove from new campaigns
            new_campaigns[x.project_id].pop(x.apartment.reference_id)

        results = []
        # Update
        for x in update_list:
            x["campaign"].update(**x["update"])
            results.append(x["campaign"])

        # Add New Campaign
        for project, project_campaigns in new_campaigns.items():
            for _, x in project_campaigns.items():
                results.append(Campaign(**x))

        return results


class AdvertisementListSchema(Schema):

    id = fields.Str()
    newhome = fields.Method("get_newhome")
    homegate = fields.Method("get_homegate")
    immoscout = fields.Method("get_immoscout")
    title = fields.Str(required=True, attribute="name")
    reference_id = fields.Str(attribute="apartment.reference_id")
    create_date = fields.Date()
    aviability_date = fields.Date(attribute="apartment.availability.start")

    class Meta:
        fields = ['id', 'reference_id', 'project_id', 'title', 'campaign_type', 'create_date', 'aviability_date', 'application_url', 'newhome', 'homegate', 'immoscout']
        ordered = True

    def get_newhome(self, obj):
        return obj.get_platform_status(CampaignPlatformChoices.NEWHOME)

    def get_homegate(self, obj):
        return obj.get_platform_status(CampaignPlatformChoices.HOMEGATE)

    def get_immoscout(self, obj):
        return obj.get_platform_status(CampaignPlatformChoices.IMMOSCOUT)

    @post_dump
    def remove_inactive_platforms(self, data, **kwargs):
        if "platforms" in self.context:
            for x in Platform.objects.get_active_platforms():
                if x not in self.context["platforms"] and x in data.keys():
                    data.pop(x)
