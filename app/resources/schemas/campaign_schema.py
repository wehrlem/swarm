import falcon
# import bson

from marshmallow import Schema, fields, validate, post_dump, validates_schema, pre_dump
from app.resources.schemas.apartment_schema import ApartmentSchema

from app.models.data.platforms import Platform

DATE_TIME_FORMAT_UTC = "%Y-%m-%dT%H:%M:%SZ"


# Location
class LocationSchema(Schema):
    locality = fields.Str(required=True)
    country = fields.Str()
    postal_code = fields.Str()


# Targeting
class TargetingSchema(Schema):
    locations = fields.Nested(LocationSchema, many=True, required=True, validate=validate.Length(min=1))
    languages = fields.List(fields.Str(), required=True, validate=validate.Length(min=1))


# Base Budget
class BaseBudgetDistributionSchema(Schema):
    percentage = fields.Float(required=True)
    netto_price = fields.Float(required=True)


# Budget - Facebook
class FacebookBudgetDistributionSchema(BaseBudgetDistributionSchema):
    ad_type = fields.Str(missing="feed")


# Budget - Google
class GoogleBudgetDistributionSchema(BaseBudgetDistributionSchema):
    ad_type = fields.Str(missing="search")


# Budget Distribution
class BudgetDistributionSchema(Schema):
    facebook = fields.Nested(FacebookBudgetDistributionSchema, many=True, validate=validate.Length(min=1))
    instagram = fields.Nested(FacebookBudgetDistributionSchema, many=True, validate=validate.Length(min=1))
    google = fields.Nested(GoogleBudgetDistributionSchema, many=True, validate=validate.Length(min=1))


# Budget
class BudgetSchema(Schema):
    budget_optimization = fields.Bool(required=True)
    currency = fields.Str(required=True)
    netto_budget = fields.Float(required=True)
    app_percentage = fields.Float(required=True)
    brutto_budget = fields.Float(required=True)
    budget_distribution = fields.Nested(BudgetDistributionSchema, required=True)


# BaseAds
class BaseAdsSchema(Schema):
    url = fields.Str()
    descriptions = fields.List(fields.Str())


# GoogleAds
class GoogleAdsSchema(BaseAdsSchema):
    ad_type = fields.Str(missing="search")
    headline1 = fields.Str(required=True)
    headline2 = fields.Str(missing="")
    headline3 = fields.Str(missing="")


# FacebookAds
class FacebookAdsSchema(BaseAdsSchema):
    ad_type = fields.Str(missing="feed")
    title = fields.Str(required=True)
    message = fields.Str(missing="")
    marketing_images = fields.List(fields.Str())


# Ads
class AdsSchema(Schema):
    google = fields.Nested(GoogleAdsSchema, allow_none=False, many=True, validate=validate.Length(min=1))
    facebook = fields.Nested(FacebookAdsSchema, many=True, validate=validate.Length(min=1))
    instagram = fields.Nested(FacebookAdsSchema, many=True, validate=validate.Length(min=1))


class PlatformsSchema(Schema):
    STATUS_CHOICES = [
        "active",
        "inactive"
    ]
    # TODO Ovdje napraviti validaciju po produktu za platformu
    name = fields.Str(required=True)
    start_date = fields.DateTime(missing=None, format=DATE_TIME_FORMAT_UTC, required=False, allow_none=True)
    end_date = fields.DateTime(missing=None, format=DATE_TIME_FORMAT_UTC, required=False, allow_none=True)
    product = fields.Str(required=True, missing="basic", allow_none=True)
    duration = fields.Integer(required=False, allow_none=True)
    # status = fields.Str(validate=validate.OneOf(STATUS_CHOICES), missing="tmp", required=False)

    @validates_schema
    def validate_product(self, data, **kwargs):
        pname = data["name"]
        product = data["product"]
        platform = Platform.objects(name=pname).first()
        if not platform:
            raise falcon.HTTPBadRequest(f'Invalid platform name - {pname}')
        products = platform.get_products()
        if product and product not in products:
            raise falcon.HTTPBadRequest(f'Product value for {pname} must be one of {products}')


class BaseCampaignSchema(Schema):
    name = fields.Str(required=True)
    campaign_type = fields.Str(missing='single')
    application_url = fields.Str()
    project_id = fields.Str(required=True)
    team = fields.Integer(allow_none=True)
    apartment = fields.Nested(ApartmentSchema, required=True)


class CampaignSchema(BaseCampaignSchema):

    id = fields.Str(dump_only=True)
    platforms = fields.Nested(PlatformsSchema, required=True, many=True)


class PlatformsBaseSchema(Schema):
    name = fields.Str()
    create_date = fields.DateTime(format="%Y-%m-%d")
    start_date = fields.DateTime(format="%Y-%m-%d")
    end_date = fields.DateTime(format="%Y-%m-%d", missing='')
    product = fields.Str()
    duration = fields.Integer(load_from="product_duration")
    is_active = fields.Boolean()
    is_published = fields.Boolean()
    has_promotion = fields.Boolean()

    class Meta:
        fields = ['name', 'create_date', 'start_date', 'end_date', 'product', 'duration', 'is_active', 'is_published', 'has_promotion']
        ordered = True

    @post_dump(pass_many=True)
    def update_platforms(self, data, many):
        active_platforms = self.context.get("active_platforms", [])
        rez = []
        selected = []
        for x in data:
            if x["name"] in active_platforms:
                rez.append(x)
                selected.append(x["name"])
        for x in list(set(active_platforms) - set(selected)):
            rez.append({
                "name": x,
                "create_date": None,
                "start_date": None,
                "end_date": None,
                "product": None,
                "duration": None,
                "is_active": False,
                "is_published": False,
                "has_promotion": False,
            })
        return rez


class PlatformsSideModalSchema(PlatformsBaseSchema):

    @pre_dump(pass_many=True)
    def get_platforms(self, data, many):
        platforms = Platform.objects.filter(active=True).all()
        self.platforms = {}
        for x in platforms:
            self.platforms[x.name] = x

    @post_dump
    def update_platform_data(self, data):
        platform_name = data["name"]
        if data["has_promotion"]:
            data["end_date"] = None
        else:
            data["duration"] = None
        if platform_name in self.platforms:
            package = self.platforms[platform_name].get_package(data["product"])
            if package is not None:
                data["package"] = package["displayValue"]
        data.pop("product")
        return data


class CampaignPlatformsSchema(Schema):
    apartment = fields.Nested(ApartmentSchema, only=('reference_id',))
    project_id = fields.Str()
    platforms = fields.Nested(PlatformsBaseSchema, required=False, many=True)

    @post_dump(pass_many=True)
    def format_data(self, data, many):
        if many:
            for x in data:
                apartment = x.pop('apartment', None)
                x['apartment_id'] = apartment.get('reference_id', None)
