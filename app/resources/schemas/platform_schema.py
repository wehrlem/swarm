from marshmallow import Schema, fields, post_dump, pre_dump
from app.models.data.campaigns import CampaignPlatform
from datetime import datetime
# from marshmallow.validate import Length, Range, Regexp, OneOf


class PackageItemSchema(Schema):
    value = fields.Str(required=True)
    duration = fields.Int(required=True)
    type = fields.Str(required=True)
    min = fields.Int()
    step = fields.Int()
    is_published = fields.Boolean(default=False)

    class Meta:
        fields = ["value", "duration", "type", "is_published", "min", "step"]
        ordered = True


class LinkSchema(Schema):
    title = fields.Str()
    url = fields.Str()


class PackageSchema(Schema):
    '''
        TODO Not used (displayName, description, link)
    '''
    name = fields.Str(required=True)
    options = fields.Nested(PackageItemSchema, many=True)
    is_published = fields.Boolean(default=False)
    displayName = fields.Str(required=True)
    description = fields.Str(required=True)
    link = fields.Nested(LinkSchema)

    class Meta:
        fields = ['name', 'is_published', 'options', 'displayName', 'description', 'link']
        ordered = True


class PlatformsWizardSchema(Schema):
    '''
        TODO not used (logo, displayName)
    '''
    name = fields.Str(required=True)
    is_published = fields.Boolean(missing=False)
    is_active = fields.Boolean(missing=False)
    has_promotion = fields.Boolean(missing=False)
    start_date = fields.DateTime(format="%d.%m.%Y")
    end_date = fields.DateTime(format="%d.%m.%Y")
    packages = fields.Nested(PackageSchema, many=True)
    logo = fields.Str(missing="")
    displayName = fields.Str(required=True)

    def get_platform(self, platform_name) -> CampaignPlatform:
        if "platforms" in self.context and platform_name in self.context["platforms"]:
            return self.context["platforms"][platform_name]
        return False

    def try_activate_package_item(self, package_items, platform: CampaignPlatform):
        _custom = None
        for x in package_items:
            if x.type == "custom":
                _custom = x
            if platform.product_duration and x.value.startswith(platform.product_key) and x.duration == platform.product_duration:
                x.is_published = True
                return True
        if _custom and _custom.value == platform.product:
            _custom.is_published = True
            _custom.duration = platform.product_duration
        return False

    def activate_package(self, packages, platform: CampaignPlatform):
        for x in packages:
            if x.name == platform.product:
                x.is_published = True
            #    break
            if platform.has_promotion and self.try_activate_package_item(x.options, platform):
                x.is_published = True
                break

    @pre_dump
    def load_platform(self, in_data, **kwargs):
        in_data.is_published = False
        in_data.is_active = False
        in_data.has_promotion = False
        in_data.start_date = datetime.now()
        in_data.end_date = None
        # Select platform
        platform = self.get_platform(in_data.name)
        if platform:
            # Activate platform
            in_data.is_published = platform.is_published
            in_data.is_active = platform.is_active
            in_data.has_promotion = platform.has_promotion
            if platform.is_published:
                in_data.start_date = platform.start_date
                in_data.end_date = platform.end_date
                # Activate package
                self.activate_package(in_data.packages, platform)
        return in_data

    class Meta:
        fields = ['name', 'is_published', 'is_active', 'has_promotion', 'start_date', 'end_date', 'packages', 'logo', 'displayName']
        ordered = True
