from marshmallow import Schema, fields
from app.resources.schemas.campaign_schema import BaseCampaignSchema


class BaseAdvertisementPlatform(Schema):
    project_id = fields.Str(required=True)
    reference_id = fields.Str(required=True)


class AdvertisementUpdateSchema(BaseCampaignSchema):
    pass
