from .root import RootResources, PingResources
# from .common import Platform
# from .ads import AdCreateRequest
# from .statistic import StatisticRequest
from app.config import APP_DIR
# from app.services.ad_creator import AdCreator
# from .ads import ImageUploader

# API v.1
from app.resources.advertisement_platforms import PlatformSideModalApi
from app.resources.advertisement_platforms import ActiveAdvertisemensByPlatformsListApi
from app.resources.advertisement_platforms import AdvertisementPlatformsWizardApi
from app.resources.advertisement_platforms import PlatformsWizardApi

from app.resources.advertisement import AdvertisementDeactivateApi
from app.resources.advertisement import AdvertisementRefreshApi
from app.resources.advertisement import AdvertisementUpdateApi
from app.resources.advertisement import AdvertisementCreateAPI
from app.resources.advertisement import AdvertisementImportApi
from app.resources.advertisement import AdvertisementsListApi
from app.resources.advertisement import AdvertisementDeleteApi

from app.mdt_users.api_v1.resources import UserApi, UserPlatformsApi, SubUserPlatformsApi


def setup_routes(app):

    app.add_static_route('/static/js', APP_DIR + '/static/js')
    app.add_static_route('/static/css', APP_DIR + '/static/css')
    app.add_route("/", RootResources())

    app.add_route("/api/ping", PingResources())

    # Create advert
    app.add_route("/api/v1/ads/create", AdvertisementCreateAPI())
    app.add_route("/api/v1/ads/import", AdvertisementImportApi())

    # Get list adverts
    app.add_route("/api/v1/ads/{project_id}/platforms", ActiveAdvertisemensByPlatformsListApi())
    app.add_route("/api/v1/ads/{project_id}", AdvertisementsListApi())

    # Advertisement
    app.add_route("/api/v1/ad/{project_id}/{reference_id}/deactivate", AdvertisementDeactivateApi())
    app.add_route("/api/v1/ad/{project_id}/{reference_id}/refresh", AdvertisementRefreshApi())
    app.add_route("/api/v1/ad/{project_id}/{reference_id}/update", AdvertisementUpdateApi())
    app.add_route("/api/v1/ad/{project_id}/{reference_id}/{team_id}/platforms-wizard", AdvertisementPlatformsWizardApi(), suffix='team')
    app.add_route("/api/v1/ad/{project_id}/{reference_id}/platforms-wizard", AdvertisementPlatformsWizardApi())
    app.add_route("/api/v1/ad/{project_id}/{reference_id}/platforms", PlatformSideModalApi())
    app.add_route("/api/v1/ad/{project_id}/{reference_id}", AdvertisementDeleteApi())

    # Platforms
    app.add_route("/api/v1/platforms", PlatformsWizardApi())
    app.add_route("/api/v1/platforms/{project_id}/{team_id}", PlatformsWizardApi(), suffix='team')
    app.add_route("/api/v1/platform/{platform_name}", PlatformsWizardApi(), suffix='platform')

    # Users
    app.add_route("/api/v1/user/create", UserApi())
    app.add_route("/api/v1/subuser/create", UserApi(), suffix='subuser')
    app.add_route("/api/v1/user/token", UserApi(), suffix='token')
    app.add_route("/api/v1/user/platforms", UserPlatformsApi())
    app.add_route("/api/v1/user/{project_id}/{team_id}/platforms", SubUserPlatformsApi())
