import falcon
from mongoengine.queryset.visitor import Q

from app.models.data.campaigns import Campaign
from app.models.data.platforms import Platform

from app.resources.schemas.campaign_schema import PlatformsSideModalSchema
from app.resources.schemas.campaign_schema import CampaignPlatformsSchema
from app.resources.schemas.platform_schema import PlatformsWizardSchema


class TeamBase(object):

    def get_active_platforms(self, req, project_id, team_id):
        user = req.context['user']
        p1 = user.getActivePlatforms()
        if team_id:
            subuser = user.getSubUserByTeamId(project_id, team_id)
            if subuser:
                p2 = subuser.getActivePlatforms()
                return list(set(p1) | set(p2))
        return p1


class PlatformSideModalApi(object):

    def on_get(self, req, resp, project_id, reference_id):
        user = req.context['user']
        active_platforms = user.getActivePlatforms()
        campaign = Campaign.objects.get_campaign_by_reference_id(user, project_id, reference_id).first()
        campaign_platforms = campaign.platforms if campaign else []
        result = PlatformsSideModalSchema(many=True, context={"active_platforms": active_platforms}).dump(campaign_platforms).data
        resp.status = falcon.HTTP_200
        resp.context["result"] = result


class ActiveAdvertisemensByPlatformsListApi(object):

    def on_get(self, req, resp, project_id):
        user = req.context['user']
        active_platforms = user.getActivePlatforms()
        campaigns = Campaign.objects.filter(account_id=user, project_id=project_id)
        results = CampaignPlatformsSchema(many=True, context={"active_platforms": active_platforms}).dump(campaigns).data
        resp.status = falcon.HTTP_200
        resp.context["result"] = results


class AdvertisementPlatformsWizardApi(TeamBase):

    def _get_platforms(self, project_id, reference_id, active_platforms):
        apartment: Campaign = Campaign.objects(project_id=project_id).filter(apartment__reference_id=reference_id).first()
        platforms: Platform = Platform.objects(platformType="platform").filter(Q(name__in=active_platforms) & Q(active=True))
        apartment_platforms_data = {}
        if apartment:
            for x in apartment.platforms:
                apartment_platforms_data[x.name] = x
        results = PlatformsWizardSchema(many=True, context={"platforms": apartment_platforms_data}).dump(platforms).data
        return results

    def on_get(self, req, resp, project_id, reference_id):
        user = req.context['user']
        active_platforms = user.getActivePlatforms()
        results = self._get_platforms(project_id, reference_id, active_platforms)
        resp.context["result"] = results

    def on_get_team(self, req, resp, project_id, reference_id, team_id):
        active_platforms = self.get_active_platforms(req, project_id, team_id)
        results = self._get_platforms(project_id, reference_id, active_platforms)
        resp.context["result"] = results


class PlatformsWizardApi(TeamBase):

    def _get_platforms(self, active_platforms):
        platforms = Platform.objects(platformType="platform").filter((Q(active=True) & Q(name__in=active_platforms))).all()
        result = PlatformsWizardSchema(many=True).dump(platforms).data
        return result

    def on_get(self, req, resp):
        user = req.context['user']
        active_platforms = user.getActivePlatforms()
        result = self._get_platforms(active_platforms)
        resp.status = falcon.HTTP_200
        resp.context["result"] = result

    def on_get_team(self, req, resp, project_id, team_id):
        active_platforms = self.get_active_platforms(req, project_id, team_id)
        result = self._get_platforms(active_platforms)
        resp.status = falcon.HTTP_200
        resp.context["result"] = result

    def on_get_platform(self, req, resp, platform_name):
        user = req.context['user']
        active_platforms = user.getActivePlatforms()
        result = {}
        if platform_name in active_platforms:
            platform = Platform.objects(platformType="platform").filter(Q(active=True) & Q(name=platform_name)).first()
            if platform is not None:
                result = PlatformsWizardSchema(many=False).dump(platform).data
        else:
            platform = {}
        resp.status = falcon.HTTP_200
        resp.context["result"] = result
