import falcon
import json
import base64
import mimetypes
import uuid
import re
import os
import io
from webargs import fields
from webargs.falconparser import use_args

# Services
from app.services import AdCreator
import app.database as db
# Models
from app.models.requests import CampaignSchema
from app.adplatform.facebook.facebook_image_uploader import FacebookImageUpload

ALLOWED_IMAGE_TYPES = (
    'image/jpeg',
    'image/png',
)


def validate_image_type(req, resp, resource, params):

    if req.content_type not in ALLOWED_IMAGE_TYPES:
        msg = 'Image type not allowed. Must be PNG, JPEG'
        raise falcon.HTTPBadRequest('Bad request', msg)


class AdCreateRequest(object):

    def __init__(self):
        self.adcreator = AdCreator()

    @use_args(CampaignSchema)
    def on_post(self, req, resp, args):

        # User
        user = req.context['user']

        # Create ads
        camp = self.adcreator.CreateAds(args, user)

        # Get write response
        resp.status = falcon.HTTP_200
        resp.context["result"] = camp

    def on_get(self, req, resp):

        # User
        user = req.context['user']

        # Get campaigns
        results = self.adcreator.GetCampaigns(user)

        # Get write response
        resp.status = falcon.HTTP_200
        resp.context["result"] = results


class ImageUploader(object):

    def __init__(self):
        pass

    _CHUNK_SIZE_BYTES = 4096
    _IMAGE_NAME_PATTERN = re.compile('[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}\.[a-z]{2,4}$')

    _UPLOAD_PATH = "app/media"

    @falcon.before(validate_image_type)
    def on_post(self, req: falcon.Request, resp):
        """params for request{
            'name':'file.jpg',
            'facebook':'True',
        }
        """
        resp_msg = dict()
        # User
        user = req.context['user']

        facebook_upload: bool = req.get_param_as_bool("facebook")
        file_name = req.get_param("name")
        file_name = str.lower(file_name)

        is_uploaded = self.upload_image(req, file_name, user)

        resp_msg["name"] = file_name

        if is_uploaded:
            if facebook_upload:
                fuploader = FacebookImageUpload()
                hash, url = fuploader.handle_image_request(self._get_path(file_name, user))
                resp_msg["facebook_hash"] = hash
                resp_msg["facebook_url"] = url

        resp.body = (json.dumps(resp_msg))
        resp.status = falcon.HTTP_200

    def upload_image(self, req: falcon.Request, file_name, user) -> bool:
        "TODO CHECK extensions and upload path"
        try:
            dir_path = os.path.join(os.getcwd(), self._UPLOAD_PATH, str(user.id))
            if not os.path.isdir(dir_path):
                os.mkdir(dir_path)

            image_path = os.path.join(dir_path, file_name)
            with io.open(image_path, 'wb') as image_file:
                while True:
                    chunk = req.stream.read(self._CHUNK_SIZE_BYTES)
                    if not chunk:
                        break

                    image_file.write(chunk)

            return True

        except Exception as e:
            return False

    def _get_path(self, file_name, user):

        dir_path = os.path.join(os.getcwd(), self._UPLOAD_PATH, str(user.id))
        image_path = os.path.join(dir_path, file_name)
        return image_path
