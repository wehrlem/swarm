import ftplib
import tempfile
import datetime
import os
import codecs
# from decimal import *
from decimal import getcontext, Decimal
from app.adplatform.idx.advertisement_idx_schema import AdvertisementIdxSchema
from app.models.data.accounts import FtpConfig
import logging
from app.models.data.campaigns import Campaign
import requests
import io
from PIL import Image, ExifTags

# FTP_HOST = 'ftp.homegate.ch'
DATA_DIR = '/data'
IMAGES_DIR = '/images'
MOVIES_DIR = '/movies'
DOC_DIR = '/doc'

version = "0.0.2"
ad_warning_logger = logging.getLogger('admanager_warning_logger')


class BaseIdx(object):
    '''
    This implements the IDX3.01 API. It is heavily inspired by Homegate library.
    The full documentation can be found here: https://github.com/arteria/python-homegate/
    '''
    _images = ["picture_" + str(x + 1) + "_filename" for x in range(13)]
    agancyID = None
    session = None
    save_in_root = False

    data_dir = DATA_DIR
    image_dir = IMAGES_DIR
    movie_dir = MOVIES_DIR
    doc_dir = DOC_DIR

    def __init__(self, agancyID, host=None, username=None, password=None, save_in_root=False):
        '''
        Establish connection to FTP server.
        '''
        self.host = host
        self.username = username
        self.password = password
        self.agancyID = agancyID
        self.save_in_root=save_in_root
        self.open_session()

    def push(self, idxRecords):
        '''
        Transfer (push, upload) this record(s) and it's file.
        autoUpdate: set
        '''
        if self.session is None:
            return False
        idx_f = tempfile.NamedTemporaryFile(delete=False)
        idx_filename = idx_f.name
        idx_f.close()
        with codecs.open(idx_filename, 'w+b', encoding="ISO-8859-1", errors='ignore') as idx_f:
            last_modified = datetime.datetime.now().strftime("%d.%m.%Y")

            if not isinstance(idxRecords, list):
                idxRecords = [idxRecords, ]

            for idxRecord in idxRecords:
                if self.agancyID != '' and self.agancyID is not None:
                    idxRecord.update({'agency_id': self.agancyID})
                idxRecord.update({'last_modified': last_modified})

                for field in idxRecord.fields:
                    if field[0] in self._images and field[1] != '':
                        # upload images
                        try:
                            fname = ''
                            extension = field[1].split("/")[-1].split(".")[-1].lower()
                            if extension in ['jpg', 'jpeg', 'gif']:
                                fname = "{}_{}".format(idxRecord.prefix, os.path.basename(field[1]))
                            idxRecord.update({field[0]: fname})
                        except Exception:
                            pass

                    elif field[0] == 'movie_filename' and field[1] != '':
                        fname = "{}_{}".format(idxRecord.prefix, os.path.basename(field[1]))
                        idxRecord.update({field[0]: fname})
                    # upload docs
                    elif field[0] == 'document_filename' and field[1] != '':
                        try:
                            fname = ''
                            extension = field[1].split("/")[-1].split(".")[-1]
                            if extension in ['pdf']:
                                fname = "{}_{}".format(idxRecord.prefix, os.path.basename(field[1]))
                            idxRecord.update({field[0]: fname})
                        except Exception:
                            pass
                # append to idx file
                for field in idxRecord.fields:
                    idx_f.write(field[1])
                    idx_f.write("#")
                idx_f.write('\x0d\x0a')  # end of line
                idx_f.flush()

            # upload idx file
            idx_f.close()
            idx_f = open(idx_filename, 'rb')
            if not self.save_in_root:
                self.session.cwd("/{directory}/".format(directory=self.data_dir))
            self.session.storbinary('STOR unload.txt', idx_f)
            os.unlink(idx_filename)
            return True
        return False

    def open_session(self):
        try:
            self.session = ftplib.FTP(self.host, self.username, self.password)
        except Exception:
            pass

    def close_session(self):
        if self.session is not None:
            self.session.quit()

    def __del__(self):
        '''
        Clean up and close.
        '''
        self.close_session()


class IdxRecord(object):
    def __init__(self, prefix=''):
        self.prefix = prefix
        self.fields = [
            ['version', 'IDX3.01'],
            ['sender_id', 'python-homegate-' + version],
            ['object_category', ''],
            ['object_type', ''],
            ['offer_type', ''],
            ['ref_property', ''],
            ['ref_house', ''],
            ['ref_object', ''],
            ['object_street', ''],
            ['object_zip', ''],
            ['object_city', ''],
            ['object_state', ''],
            ['object_country', ''],
            ['region', ''],
            ['object_situation', ''],
            ['available_from', ''],
            ['object_title', ''],
            ['object_description', ''],
            ['selling_price', '', {'type': 'int', 'length': (10, 0)}],
            ['rent_net', '', {'type': 'int', 'length': (10, 0)}],
            ['rent_extra', '', {'type': 'int', 'length': (10, 0)}],
            ['price_unit', ''],
            ['currency', ''],
            ['gross_premium', ''],
            ['floor', '', {'type': 'int', 'length': (6, 0)}],
            ['number_of_rooms', '', {'type': 'int', 'length': (5, 1)}],
            ['number_of_apartments', '', {'type': 'int', 'length': (5, 1)}],
            ['surface_living', '', {'type': 'int', 'length': (10, 0)}],
            ['surface_property', '', {'type': 'int', 'length': (10, 0)}],
            ['surface_usable', '', {'type': 'int', 'length': (10, 0)}],
            ['volume', '', {'type': 'int', 'length': (10, 0)}],
            ['year_built', '', {'type': 'int', 'length': (4, 0)}],
            ['prop_view', ''],
            ['prop_fireplace', ''],
            ['prop_cabletv', ''],
            ['prop_elevator', ''],
            ['prop_child_friendly', ''],
            ['prop_parking', ''],
            ['prop_garage', ''],
            ['prop_balcony', ''],
            ['prop_roof_floor', ''],
            ['distance_public_transport', '', {'type': 'int', 'length': (5, 0)}],
            ['distance_shop', '', {'type': 'int', 'length': (5, 0)}],
            ['distance_kindergarten', '', {'type': 'int', 'length': (5, 0)}],
            ['distance_school1', '', {'type': 'int', 'length': (5, 0)}],
            ['distance_school2', '', {'type': 'int', 'length': (5, 0)}],
            ['picture_1_filename', ''],
            ['picture_2_filename', ''],
            ['picture_3_filename', ''],
            ['picture_4_filename', ''],
            ['picture_5_filename', ''],
            ['picture_1_title', ''],
            ['picture_2_title', ''],
            ['picture_3_title', ''],
            ['picture_4_title', ''],
            ['picture_5_title', ''],
            ['picture_1_description', ''],
            ['picture_2_description', ''],
            ['picture_3_description', ''],
            ['picture_4_description', ''],
            ['picture_5_description', ''],
            ['movie_filename', ''],
            ['movie_title', ''],
            ['movie_description', ''],
            ['document_filename', ''],
            ['document_title', ''],
            ['document_description', ''],
            ['url', ''],
            ['agency_id', ''],
            ['agency_name', ''],
            ['agency_name_2', ''],
            ['agency_reference', ''],
            ['agency_street', ''],
            ['agency_zip', ''],
            ['agency_city', ''],
            ['agency_country', ''],
            ['agency_phone', ''],
            ['agency_mobile', ''],
            ['agency_fax', ''],
            ['agency_email', ''],
            ['agency_logo', ''],
            ['visit_name', ''],
            ['visit_phone', ''],
            ['visit_email', ''],
            ['visit_remark', ''],
            ['publish_until', ''],
            ['destination', ''],
            ['picture_6_filename', ''],
            ['picture_7_filename', ''],
            ['picture_8_filename', ''],
            ['picture_9_filename', ''],
            ['picture_6_title', ''],
            ['picture_7_title', ''],
            ['picture_8_title', ''],
            ['picture_9_title', ''],
            ['picture_6_description', ''],
            ['picture_7_description', ''],
            ['picture_8_description', ''],
            ['picture_9_description', ''],
            ['picture_1_url', ''],
            ['picture_2_url', ''],
            ['picture_3_url', ''],
            ['picture_4_url', ''],
            ['picture_5_url', ''],
            ['picture_6_url', ''],
            ['picture_7_url', ''],
            ['picture_8_url', ''],
            ['picture_9_url', ''],
            ['distance_motorway', ''],
            ['ceiling_height', '', {'type': 'int', 'length': (10, 2)}],
            ['hall_height', '', {'type': 'int', 'length': (10, 2)}],
            ['maximal_floor_loading', '', {'type': 'int', 'length': (10, 1)}],
            ['carrying_capacity_crane', '', {'type': 'int', 'length': (10, 1)}],
            ['carrying_capacity_elevator', '', {'type': 'int', 'length': (10, 1)}],
            ['isdn', ''],
            ['wheelchair_accessible', ''],
            ['animal_allowed', ''],
            ['ramp', ''],
            ['lifting_platform', ''],
            ['railway_terminal', ''],
            ['restrooms', ''],
            ['water_supply', ''],
            ['sewage_supply', ''],
            ['power_supply', ''],
            ['gas_supply', ''],
            ['municipal_info', ''],
            ['own_object_url', ''],
            ['billing_anrede', ''],
            ['billing_first_name', ''],
            ['billing_name', ''],
            ['billing_company', ''],
            ['billing_street', ''],
            ['billing_post_box', ''],
            ['billing_zip', ''],
            ['billing_place_name', ''],
            ['billing_land', ''],
            ['billing_phone_1', ''],
            ['billing_phone_2', ''],
            ['billing_mobile', ''],
            ['billing_language', ''],
            ['publishing_id', ''],
            ['delivery_id', ''],
            ['picture_10_filename', ''],
            ['picture_11_filename', ''],
            ['picture_12_filename', ''],
            ['picture_13_filename', ''],
            ['picture_10_title', ''],
            ['picture_11_title', ''],
            ['picture_12_title', ''],
            ['picture_13_title', ''],
            ['picture_10_description', ''],
            ['picture_11_description', ''],
            ['picture_12_description', ''],
            ['picture_13_description', ''],
            ['picture_10_url', ''],
            ['picture_11_url', ''],
            ['picture_12_url', ''],
            ['picture_13_url', ''],
            ['commission_sharing', ''],
            ['commission_own', ''],
            ['commission_partner', ''],
            ['agency_logo_2', ''],
            ['number_of_floors', '', {'type': 'int', 'length': (2, 0)}],
            ['year_renovated', '', {'type': 'int', 'length': (4, 0)}],
            ['flat_sharing_community', ''],
            ['corner_house', ''],
            ['middle_house', ''],
            ['building_land_connected', ''],
            ['gardenhouse', ''],
            ['raised_ground_floor', ''],
            ['new_building', ''],
            ['old_building', ''],
            ['under_building_laws', ''],
            ['under_roof', ''],
            ['swimmingpool', ''],
            ['minergie_general', ''],
            ['minergie_certified', ''],
            ['last_modified', ''],
            ['advertisement_id', ''],
            ['sparefield_1', ''],
            ['sparefield_2', ''],
            ['sparefield_3', ''],
            ['sparefield_4', ''],
        ]

    def convert(self, value=None, options={}):
        ''' Convert and validate values. '''
        converted = False
        if value is not None and value != '':
            if options.get('type', ) == 'int':
                getcontext().prec = 28
                _, scale = options.get('length', (28, 0))
                value = str(round(Decimal(value) * Decimal(1), scale))
                converted = True
            if options.get('type', ) == 'str':
                value = value[:options.get('length', -1)]
                converted = True
        print
        "converted", converted
        return value

    def update(self, obj):
        '''
        The argument 'obj' should be dictionary containing the new values to overwrites 'self._fields' partially.

        Example: Set city to 'Basel' and country to Switzerland, the result '(2, 0)' says that two fields were
        updated successfully and no errors.

            >>> rec.update({'object_city':'Basel', 'object_country':'CH'})
            (2, 0)
            >>>

        '''
        error_fields = []
        updates = 0

        for o in obj:
            found = False
            for field in self.fields:
                if o == field[0]:
                    if len(field) > 2:
                        field[1] = self.convert(obj[o], field[2])
                    else:
                        field[1] = obj[o]  # self.convert(obj[o], field[2])
                    updates += 1
                    found = True
                    break
            if not found:
                error_fields.append(o)
                # errors += 1  # not found
        return updates, error_fields


class IdxManager(BaseIdx):

    platform_name = None

    def __init__(self, config: FtpConfig, platform_name):
        super().__init__(
            agancyID=config.agency_id,
            host=config.host,
            username=config.username,
            password=config.password,
            save_in_root=config.save_in_root
        )
        self.platform_name = platform_name

    def getPrefix(self, campaign):
        return "{}_{}".format(campaign.project_id, campaign.apartment.reference_id)

    def getRecords(self, campaigns, user):
        # Convert campaign to JSON
        campaign_mapper = AdvertisementIdxSchema(many=True, context={"platform": self.platform_name, 'user': user})
        camps_json = campaign_mapper.dump(campaigns)

        records = []
        for x in camps_json.data:
            try:
                rec = IdxRecord(prefix="{}_{}".format(x['ref_house'], x['apartment_reference_id']))
                rec.update(x)
                records.append(rec)
            except Exception as ex:
                print("Exception {}".format(ex))

        return records, camps_json.data

    def pushImages(self, campaign: Campaign):
        if self.session is None:
            return False
        br = 0
        total_img = 0
        imgs = campaign.apartment.get_images("de")
        for im in imgs:
            if im.url in ['', None]:
                continue
            total_img += 1
            try:
                fname = "{}_{}".format(self.getPrefix(campaign), im.name)
                resp = requests.get(im.url)
                # f = io.BytesIO(resp.content)
                image = Image.open(io.BytesIO(resp.content))
                try:
                    if image._getexif():
                        exif = dict(image._getexif().items())
                        if exif:
                            for orientation in ExifTags.TAGS.keys():
                                if ExifTags.TAGS[orientation] == 'Orientation':
                                    break
                            if orientation is not None and orientation in exif:
                                if exif[orientation] == 3:
                                    image = image.rotate(180, expand=True)
                                elif exif[orientation] == 6:
                                    image = image.rotate(270, expand=True)
                                elif exif[orientation] == 8:
                                    image = image.rotate(90, expand=True)
                except Exception:
                    pass
                rgb_im = image.convert('RGB')
                f = io.BytesIO()
                rgb_im.save(f, 'JPEG')
                f.seek(0)
                if not self.save_in_root:
                    self.session.cwd("/{directory}/".format(directory=IMAGES_DIR))
                self.session.storbinary('STOR {fname}'.format(fname=fname), f)
                br += 1
            except Exception as ex:
                print("File not exists: {}, error {}".format(im.url, ex))
                continue
        if br != total_img:
            msg = "[{}.{}] {}/{} images".format(campaign.project_id, campaign.apartment.reference_id, br, total_img)
            ad_warning_logger.warning(msg)
        return br

    def pushDocuments(self, campaign):
        if self.session is None:
            return False
        br = 0
        total_doc = 0
        docs = campaign.apartment.get_documents("de")
        for doc in docs:
            if doc.url in ['', None]:
                continue
            total_doc += 1
            try:
                fname = "{}_{}".format(self.getPrefix(campaign), doc.name)
                resp = requests.get(doc.url)
                f = io.BytesIO(resp.content)
                if not self.save_in_root:
                    self.session.cwd("/{directory}/".format(directory=DOC_DIR))
                self.session.storbinary('STOR {fname}'.format(fname=fname), f)
                br += 1
            except Exception:
                print("File not exists: {}".format(doc.url))
                continue
        if br != total_doc:
            msg = "[{}.{}] {}/{} documents".format(campaign.project_id, campaign.apartment.reference_id, br, total_doc)
            ad_warning_logger.warning(msg)
        return br
