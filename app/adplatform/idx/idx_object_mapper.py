class IDXObjectCategory:
    AGRI = 'AGRI'
    APPT = 'APPT'
    GASTRO = 'GASTRO'
    HOUSE = 'HOUSE'
    INDUS = 'INDUS'
    PARK = 'PARK'
    PROP = 'PROP'
    SECONDARY = 'SECONDARY'
    GARDEN = 'GARDEN'


class CategoryMapper(object):
    APARTMENT = 'apartment'
    HOUSE = 'house'
    PARKINGSPACE = 'parkingspace'
    CRAFT = 'craft'
    OTHER = 'other'
    PLOT = 'plot'

    _CATEGORY_MAPPER = {
        APARTMENT: IDXObjectCategory.APPT,
        HOUSE: IDXObjectCategory.HOUSE,
        PARKINGSPACE: IDXObjectCategory.PARK,
        CRAFT: IDXObjectCategory.INDUS,
        OTHER: IDXObjectCategory.SECONDARY,
        PLOT: IDXObjectCategory.PROP,
    }

    _GASTRO_TYPES = ['hotel', 'restaurant', 'coffeehouse', 'bar', 'disco', 'sporthall', 'swimmingpool', 'pub']
    _INDUS_TYPES = ['office', 'advertisingarea', 'shop', 'commercial', 'storageroom', 'factory', 'workshop', 'displaywindow', 'commerce', 'arcade']
    _OTHER_SECONDARY_TYPES = ['hobbyroom', 'additionalroom', 'atticcompartment']
    _OTHER_INDUS_TYPES = ['basementroom']
    _OTHER_GARDEN_TYPES = ['alottmentgarden']
    _OTHER_PROP_TYPES = ['buildingland', 'agriculturalland', 'commercialland', 'industrialland']

    @classmethod
    def get_category(cls, instance):
        try:
            category = instance.apartment.category
            category_type = instance.apartment.object_type
            if category in [cls.APARTMENT, cls.HOUSE, cls.PARKINGSPACE, cls.PLOT]:
                return cls._CATEGORY_MAPPER[category]
            elif category == cls.CRAFT:
                if category_type in cls._GASTRO_TYPES:
                    return IDXObjectCategory.GASTRO
                elif category_type in cls._INDUS_TYPES:
                    return IDXObjectCategory.INDUS
                else:
                    return IDXObjectCategory.GASTRO
            elif category == cls.OTHER:
                if category_type in cls._OTHER_SECONDARY_TYPES:
                    return IDXObjectCategory.SECONDARY
                elif category_type in cls._OTHER_INDUS_TYPES:
                    return IDXObjectCategory.INDUS
                elif category_type in cls._OTHER_GARDEN_TYPES:
                    return IDXObjectCategory.GARDEN
                elif category_type in cls._OTHER_PROP_TYPES:
                    return IDXObjectCategory.PROP
                else:
                    IDXObjectCategory.INDUS
            else:
                return IDXObjectCategory.HOUSE
        except Exception:
            return IDXObjectCategory.HOUSE

    _APARTMENT_OBJECT_TYPE = {
        'apartment': '1',
        'subsidized_apartment': '1',
        'elderly_apartment': '1',
        'loft_apartment': '1',
        'retirement_apartment': '1',
        'cluster_apartment': '1',
        'duplex': '2',
        'maisonette_apartment': '2',
        'attica_apartment': '3',
        'roofflat': '4',
        'studio': '5',
        'singleroom': '6',
        'furnishedflat': '7',
        'atticflat': '11',
    }

    _HOUSE_OBJECT_TYPE = {
        'singlehouse': '1',
        'rowhouse': '2',
        'villa': '5',
        'farmhouse': '6',
        'multipledwelling': '7',
        'rustichouse': '13',
    }

    _PARKINGSPACE_OBJECT_TYPE = {
        'openslot': '1',
        'openslot_electro': '1',
        'coveredslot': '2',
        'singlegarage': '3',
        'doublegarage': '4',
        'undergroundslot': '5',
        'undergroundslot_electro': '5',
        'coveredslotbike': '9',
        'outdoorslotbike': '10',
    }

    _CRAFT_OBJECT_TYPE = {
        # GASTRO
        'hotel': '1',
        'restaurant': '2',
        'coffeehouse': '3',
        'bar': '4',
        'disco': '5',
        'sporthall': '11',
        'swimmingpool': '13',
        'pub': '17',

        # INDUS
        'office': '1',
        'shop': '2',
        'advertisingarea': '3',
        'commercial': '4',
        'storageroom': '5',
        'factory': '16',
        'arcade': '18',
        'workshop': '28',
        'commerce': '34',
        'displaywindow': '36',
    }

    _OTHER_OBJECT_TYPE = {
        # SECONDARY
        'hobbyroom': '0',
        'additionalroom': '0',
        'atticcompartment': '2',

        # INDUS
        'basementroom': '5',

        # GARDEN
        'alottmentgarden': '0',

        # PROP
        'buildingland': '1',
        'agriculturalland': '2',
        'commercialland': '3',
        'industrialland': '4',
    }

    _PLOT_OBJECT_TYPE = {
        # PROP
        'buildingland': '1',
    }

    _CATEGORY_TYPES = {
        **_APARTMENT_OBJECT_TYPE,
        **_HOUSE_OBJECT_TYPE,
        **_PARKINGSPACE_OBJECT_TYPE,
        **_CRAFT_OBJECT_TYPE,
        **_OTHER_OBJECT_TYPE,
        **_PLOT_OBJECT_TYPE
    }

    @classmethod
    def get_category_type(cls, instance):
        try:
            category_type = instance.apartment.object_type
            return cls._CATEGORY_TYPES[category_type]
        except Exception:
            return '1'

    @classmethod
    def get_price_unit(cls, instance):
        _PRICE_UNIT_RENT = {
            'per_m2_year': 'M2YEARLY',
            'per_month': 'MONTHLY',
            'per_year': 'YEARLY'
        }
        try:
            if instance.apartment.prices.rent.price_unit:
                return _PRICE_UNIT_RENT.get(instance.apartment.prices.rent.price_unit, 'MONTHLY')
            return 'MONTHLY'
        except Exception:
            return 'MONTHLY'

    @classmethod
    def get_offer_type(cls, instance):
        _OFFER_TYPE = {
            "rent": "RENT",
            "buy": "SALE"
        }
        try:
            if instance.apartment.type:
                return _OFFER_TYPE.get(instance.apartment.type, 'RENT')
            return 'RENT'
        except Exception:
            return 'RENT'


base_data_mapper = {
    "applies": "Y",
    "does-not-apply": "N",
    "unknown": None
}
