from app.models.data.accounts import Account
from marshmallow import post_dump, Schema, pre_dump, fields
from app.adplatform.idx.idx_object_mapper import CategoryMapper, IDXObjectCategory
import json
from datetime import timedelta
from app.models.data.common.configs import CampaignPlatformChoices


def get_platform_product(obj, platform):
    if platform.name == CampaignPlatformChoices.NEWHOME:
        value = platform.product
        if platform.duration:
            value = f'{value[:5]}{platform.duration}'
        return json.dumps([{"product": value, "reference": obj.apartment.reference_id}])
    elif platform.name == CampaignPlatformChoices.IMMOSCOUT:
        if not platform.product_duration:
            return None
        start_promotion = platform.start_date.replace(hour=0, minute=0, second=0, microsecond=0)
        end_promotion = start_promotion + timedelta(days=platform.product_duration - 1)
        return "IS24-TL:{}".format(end_promotion.strftime("%d.%m.%Y"))
    return platform.product


class FeatureFieldSchema(fields.Field):
    def _serialize(self, value, attr, obj):
        if value == 'applies':
            return 'Y'
        return None


class AdvertisementIdxSchema(Schema):

    id = fields.Str()
    version = fields.Str(max_length=50)
    sender_id = fields.Str(max_length=50)
    object_category = fields.Method("get_object_category", required=True)
    object_type = fields.Method("get_object_type", required=True)
    offer_type = fields.Method("get_offer_type", required=True)
    ref_property = fields.Str(attribute="apartment_id", required=True)
    ref_house = fields.Str(attribute="project_id", required=True)
    apartment_reference_id = fields.Str(attribute="apartment.reference_id")
    ref_object = fields.Str(attribute="apartment.visual_reference_id")
    object_street = fields.Method("get_object_street", required=True)
    object_zip = fields.Str(attribute="apartment.address.postal_code", required=True)
    object_city = fields.Str(attribute="apartment.address.locality", required=True)
    # object_state = fields.Method("get_object_state")
    object_country = fields.Str(attribute="apartment.address.country_code", required=True)
    region = fields.Str(attribute="apartment.address.region", required=True)
    object_situation = fields.Str(attribute="apartment.address.street_addition")
    available_from = fields.DateTime(attribute="apartment.availability.start", format="%d.%m.%Y")
    object_title = fields.Method("get_object_title", required=True)
    object_description = fields.Method("get_object_description", required=True)

    selling_price = fields.Method("get_selling_price", required=True)
    rent_net = fields.Method("get_rent_net")
    rent_extra = fields.Method("get_rent_extra")
    price_unit = fields.Method("get_price_unit", required=True)

    currency = fields.Str(attribute="apartment.prices.currency", required=True)
    # gross_premium = fields.Str(max_length=19)

    floor = fields.Float(attribute="apartment.characteristics.floor")
    number_of_rooms = fields.Float(attribute="apartment.characteristics.number_of_rooms")
    number_of_apartments = fields.Float(attribute="apartment.characteristics.number_of_apartements")
    # surface_living = fields.Int(attribute="apartment.characteristics.area_nwf")
    surface_living = fields.Method("get_surface_living")
    # surface_property = fields.Int(attribute="apartment.characteristics.area_bwf")
    surface_property = fields.Method("get_surface_property")
    surface_usable = fields.Method("get_surface_usable")

    volume = fields.Float(attribute="apartment.characteristics.volume_gva")
    year_built = fields.Integer(attribute="apartment.characteristics.year_built")

    # Boolean features
    prop_view = FeatureFieldSchema(attribute="apartment.characteristics.has_nice_view")
    prop_fireplace = FeatureFieldSchema(attribute="apartment.characteristics.has_fireplace")
    prop_cabletv = FeatureFieldSchema(attribute="apartment.characteristics.has_cable_tv")
    prop_elevator = FeatureFieldSchema(attribute="apartment.characteristics.has_elevator")
    prop_child_friendly = FeatureFieldSchema(attribute="apartment.characteristics.is_child_friendly")
    prop_parking = FeatureFieldSchema(attribute="apartment.characteristics.has_parking")
    prop_garage = FeatureFieldSchema(attribute="apartment.characteristics.has_garage")
    prop_balcony = FeatureFieldSchema(attribute="apartment.characteristics.has_balcony")
    # prop_roof_floor = fields.Method("get_prop_roof_floor")
    # distance_public_transport = fields.Int()
    # distance_shop = fields.Int()
    # distance_kindergarten = fields.Int()
    # distance_school1 = fields.Int()
    # distance_school2 = fields.Int()
    # distance_motorway = fields.Int()
    ceiling_height = fields.Float(attribute="apartment.characteristics.ceiling_height")
    hall_height = fields.Float(attribute="apartment.characteristics.hall_height")
    maximal_floor_loading = fields.Float(attribute="apartment.characteristics.floor_load")
    carrying_capacity_crane = fields.Float(attribute="apartment.characteristics.crane_capacity")
    carrying_capacity_elevator = fields.Float(attribute="apartment.characteristics.elevator_load")
    isdn = FeatureFieldSchema(attribute="apartment.characteristics.has_isdn")
    wheelchair_accessible = FeatureFieldSchema(attribute="apartment.characteristics.is_wheelchair_accessible")
    minergie_certified = FeatureFieldSchema(attribute="apartment.characteristics.is_minergie_certified")
    animal_allowed = FeatureFieldSchema(attribute="apartment.characteristics.are_pets_allowed")
    ramp = FeatureFieldSchema(attribute="apartment.characteristics.has_ramp")
    lifting_platform = FeatureFieldSchema(attribute="apartment.characteristics.has_lifting_platform")
    railway_terminal = FeatureFieldSchema(attribute="apartment.characteristics.has_railway_terminal")
    restrooms = FeatureFieldSchema(attribute="apartment.characteristics.has_restrooms")
    water_supply = FeatureFieldSchema(attribute="apartment.characteristics.has_supply_water")
    sewage_supply = FeatureFieldSchema(attribute="apartment.characteristics.has_supply_sewage")
    power_supply = FeatureFieldSchema(attribute="apartment.characteristics.has_supply_power")
    gas_supply = FeatureFieldSchema(attribute="apartment.characteristics.has_supply_gas")

    # Numeric features
    number_of_floors = fields.Integer(attribute="apartment.characteristics.number_of_floors")
    year_renovated = fields.Integer(attribute="apartment.characteristics.year_last_renovated")

    flat_sharing_community = FeatureFieldSchema(attribute="apartment.characteristics.has_flat_sharing_community")
    corner_house = FeatureFieldSchema(attribute="apartment.characteristics.is_corner_house")
    middle_house = FeatureFieldSchema(attribute="apartment.characteristics.is_middle_house")
    building_land_connected = FeatureFieldSchema(attribute="apartment.characteristics.has_connected_building_land")
    gardenhouse = FeatureFieldSchema(attribute="apartment.characteristics.has_garden_shed")
    raised_ground_floor = FeatureFieldSchema(attribute="apartment.characteristics.is_ground_floor_raised")
    new_building = FeatureFieldSchema(attribute="apartment.characteristics.is_new_construction")
    old_building = FeatureFieldSchema(attribute="apartment.characteristics.is_old_building")
    under_building_laws = FeatureFieldSchema(attribute="apartment.characteristics.is_under_building_laws")
    under_roof = FeatureFieldSchema(attribute="apartment.characteristics.is_under_roof")
    swimmingpool = FeatureFieldSchema(attribute="apartment.characteristics.has_swimming_pool")
    minergie_general = FeatureFieldSchema(attribute="apartment.characteristics.is_minergie_general")
    minergie_certified = FeatureFieldSchema(attribute="apartment.characteristics.is_minergie_certified")

    last_modified = fields.DateTime(attribute="update_date", format="%d.%m.%Y %H:%M:%S")

    # PACKAGES
    sparefield_1 = fields.Method("get_sparefield_1")
    # VIRTUAL LINK
    sparefield_2 = fields.Method("get_sparefield_2")

    # MOVIE
    # movie_filename = fields.Str(max_length=200)
    # movie_title = fields.Str(max_length=200)
    # movie_description = fields.Str(max_length=1800)

    # DOCUMENTS
    document_filename = fields.Method("get_document_filename")
    document_title = fields.Method("get_document_title")
    # document_description = fields.Str(max_length=200)

    # NOT USED
    # billing_anrede = fields.Int()
    # billing_first_name = fields.Str(max_lenght=200)
    # billing_name = fields.Str(max_lenght=200)
    # billing_company = fields.Str(max_lenght=200)
    # billing_street = fields.Str(max_lenght=200)
    # billing_post_box = fields.Str(max_lenght=200)
    # billing_zip = fields.Str(max_lenght=10)
    # billing_place_name = fields.Str(max_lenght=200)
    # billing_land = fields.Str(max_lenght=200)
    # billing_phone_1 = fields.Str(max_lenght=200)
    # billing_phone_2 = fields.Str(max_lenght=200)
    # billing_mobile = fields.Str(max_lenght=200)
    # billing_language = fields.Int()

    url = fields.Method("get_url")
    # agency_id = fields.Str(max_length=10)
    agency_name = fields.Str(attribute="apartment.seller.organization.legal_name")
    agency_name_2 = fields.Str(attribute="apartment.seller.organization.brand")
    agency_reference = fields.Method("get_agency_reference")
    agency_street = fields.Method("get_agency_street")
    agency_zip = fields.Str(attribute="apartment.seller.organization.address.postal_code")
    agency_city = fields.Str(attribute="apartment.seller.organization.address.locality")
    agency_country = fields.Str(attribute="apartment.seller.organization.address.country_code")
    agency_phone = fields.Str(attribute="apartment.seller.contact_person.phone")
    # agency_mobile = fields.Str(attribute="apartment.seller.organization.mobile")
    agency_email = fields.Method("get_email")
    # agency_logo = fields.Str(max_length=200)

    visit_name = fields.Method("get_visit_name")
    visit_phone = fields.Str(attribute="apartment.seller.visit_person.phone")
    visit_email = fields.Str(attribute="apartment.seller.visit_person.email")
    visit_remark = fields.Method("get_visit_remark")

    # NOT USED
    # publish_until = fields.Date()
    # destination = fields.Str(max_length=200)
    # municipal_info = fields.Str(max_length=1)
    # own_object_url = fields.Str(attribute="application_url")

    # UNKNOWN
    # publishing_id = fields.Int()
    # delivery_id = fields.Int()

    # NOT USED
    # commission_sharing = fields.Int()
    # commission_own = fields.Str(max_length=10)
    # commission_partner = fields.Str(max_length=10)
    # agency_logo_2 = fields.Str(max_length=200)

    # Get localisation by default lang
    def __get_localisation_by_lang(self, campaign):
        if "lang" in self.context:
            locs = [x for x in campaign.apartment.localizations if x.lang == self.context["lang"]]
            if len(locs) > 0:
                return locs[0]
            else:
                return None
        elif len(campaign.apartment.localizations) > 0:
            return campaign.apartment.localizations[0]
        else:
            return None

    # Get localisation by selected lang
    def __get_localisation(self, obj):
        if str(obj.id) in self.loc:
            return self.loc[str(obj.id)]
        else:
            return None

    def __get_resources(self, data, resource_type):
        tmp_loc = self.loc[str(data["id"])]
        if tmp_loc is not None and hasattr(tmp_loc, "attachments") and hasattr(tmp_loc.attachments, resource_type):
            if getattr(tmp_loc.attachments, resource_type) is not None:
                return getattr(tmp_loc.attachments, resource_type)
        return []

    def __set_images_for_record(self, data):
        images = self.__get_resources(data, "images")
        i = 1
        for img in images:
            if i > 13:
                break
            name = "picture_{}_filename".format(i)
            img_title = "picture_{}_title".format(i)
            if img.url is not None:
                data[name] = img.name
                if img.title is not None:
                    data[img_title] = img.title
                i += 1

    @pre_dump(pass_many=True)
    def set_localisation(self, obj, many):
        self.loc = {}
        if many:
            for x in obj:
                self.loc[str(x.id)] = self.__get_localisation_by_lang(x)
        else:
            self.loc[str(obj.id)] = self.__get_localisation_by_lang(obj)

    # @pre_dump(pass_many=True)
    # def process_netto_price_front(self, obj, many):
    #     self.netto_price_front = False
    #     user = self.context.get('user', None)
    #     if isinstance(user, Account) and hasattr(user, 'saved_data'):
    #         self.netto_price_front = user.saved_data.get('netto_price_front', False)

    @post_dump()
    def remove_skip_values(self, data):
        SKIP_VALUES = set([None])
        return {
            key: value for key, value in data.items()
            if value not in SKIP_VALUES
        }

    @post_dump(pass_many=True)
    def add_pictures(self, data, many):
        if many:
            for x in data:
                self.__set_images_for_record(x)
        else:
            self.__set_images_for_record(data)

    def get_document_title(self, data):
        documents = self.__get_resources(data, "documents")
        if len(documents) > 0:
            return documents[0].title
        return ""

    def get_document_filename(self, data):
        documents = self.__get_resources(data, "documents")
        if len(documents) > 0:
            return documents[0].name
        return ""

    def get_surface_living(self, obj):
        try:
            object_category = CategoryMapper.get_category(obj)
            if object_category in [IDXObjectCategory.APPT, IDXObjectCategory.HOUSE]:
                return float(obj.apartment.characteristics.area_nwf)
            return ''
        except Exception:
            return ''

    def get_surface_property(self, obj):
        try:
            object_category = CategoryMapper.get_category(obj)
            if object_category not in [IDXObjectCategory.APPT, IDXObjectCategory.HOUSE, IDXObjectCategory.PARK, IDXObjectCategory.AGRI, IDXObjectCategory.GASTRO, IDXObjectCategory.INDUS]:
                return float(obj.apartment.characteristics.area_nwf)
            return ''
        except Exception:
            return ''

    def get_surface_usable(self, obj):
        try:
            object_category = CategoryMapper.get_category(obj)
            if object_category in [IDXObjectCategory.AGRI, IDXObjectCategory.GASTRO, IDXObjectCategory.INDUS, IDXObjectCategory.PARK]:
                return float(obj.apartment.characteristics.area_nwf)
            return ''
        except Exception:
            return ''

    def get_object_category(self, obj):
        return CategoryMapper.get_category(obj)

    def get_object_type(self, obj):
        return CategoryMapper.get_category_type(obj)

    def get_offer_type(self, obj):
        return CategoryMapper.get_offer_type(obj)

    def get_object_street(self, obj):
        s = obj.apartment.address.street
        sn = obj.apartment.address.street_number
        if s and sn:
            return "{}, {}".format(s, sn)
        if s:
            return "{}".format(s)

    def get_email(self, obj):
        try:
            _email = obj.apartment.seller.contact_person.email
            if _email and "platform" in _email:
                platform_name = self.context["platform"]
                if platform_name == CampaignPlatformChoices.IMMOSCOUT:
                    platform_name = "{}24".format(CampaignPlatformChoices.IMMOSCOUT)
                _email = _email.format(platform=platform_name)
            return _email
        except Exception:
            return ''

    def get_object_title(self, obj):
        return self.loc[str(obj.id)].name

    def get_object_description(self, obj):
        if self.loc[str(obj.id)].description:
            return self.loc[str(obj.id)].description.replace("#", '')
        return ''

    def get_selling_price(self, obj):
        if self.get_offer_type(obj) == "SALE":
            return obj.apartment.prices.buy.price
        elif self.get_offer_type(obj) == "RENT":
            return obj.apartment.prices.rent.gross

    def get_rent_net(self, obj):
        if self.get_offer_type(obj) == "RENT":
            return obj.apartment.prices.rent.net

    def get_rent_extra(self, obj):
        if self.get_offer_type(obj) == "RENT":
            return obj.apartment.prices.rent.extra

    def get_price_unit(self, obj):
        if self.get_offer_type(obj) == "RENT":
            try:
                return CategoryMapper.get_price_unit(obj)
            except Exception as ex:
                print("get_price_unit mapper error {}".format(ex))
                return 'MONTHLY'
        elif self.get_offer_type(obj) == "SALE":
            return "SELL"

    def get_url(self, obj):
        try:
            links = self.__get_resources(obj, "links")
            if links and len(links) > 0:
                return links[0]["url"]
            return ''
        except Exception as ex:
            print("get_url error {}".format(ex))
            return ''

    def get_agency_reference(self, obj):
        cp = obj.apartment.seller.contact_person
        if cp:
            return "{} {}".format(cp.given_name, cp.family_name)

    def get_agency_street(self, obj):
        s = obj.apartment.seller.organization.address.street
        sn = obj.apartment.seller.organization.address.street_number
        if s and sn:
            return "{}, {}".format(s, sn)
        elif s:
            return s

    def get_visit_name(self, obj):
        rez = []
        gn = obj.apartment.seller.visit_person.given_name
        fn = obj.apartment.seller.visit_person.family_name
        if gn:
            rez.append(gn)
        if fn:
            rez.append(fn)
        return " ".join(rez)

    def get_visit_remark(self, obj):
        try:
            res = []
            events = self.loc[str(obj.id)].events
            for x in events:
                date_str = None
                t1 = None
                if x.start is not None:
                    date_str = x.start.strftime("%d.%m.%Y")
                    t1 = x.start.strftime("%H:%M")
                t2 = None
                if x.end is not None:
                    t2 = x.end.strftime("%H:%M")
                if date_str is not None and t1 is not None and t2 is not None:
                    res.append("{} {} - {}".format(date_str, t1, t2))
                elif date_str is not None and t1 is not None:
                    res.append("{} {}".format(date_str, t1))
            if len(res) > 0:
                result = "</br>".join(res)
                return result
            return ""
        except Exception as ex:
            print("get_visit_remark error {}".formt(ex))
            return ""

    def get_sparefield_1(self, obj):
        if 'platform' in self.context:
            platform = obj.get_platform(self.context["platform"])
            if platform and platform.has_promotion:
                return get_platform_product(obj, platform)

    def get_sparefield_2(self, obj):
        if 'platform' in self.context:
            platform = obj.get_platform(self.context["platform"])
            if platform:
                localisation = self.__get_localisation(obj)
                if hasattr(localisation.attachments, "link_virtual_tours") and localisation.attachments.link_virtual_tours is not None:
                    for x in localisation.attachments.link_virtual_tours:
                        return x.url
