from facebook_business.api import FacebookAdsApi
from facebook_business.adobjects.adaccount import AdAccount
from facebook_business.adobjects.campaign import Campaign
from app.models.data.campaigns import Campaign as CampaignRequest
from app.models.data.campaign_settings import FacebookCampaignSetting
from app.models.data.campaign_platform_mapper import FacebookCampaignMapper
from app.models.data.accounts import Account as UserAccount


class CampaignHandler(object):

    def __init__(self,  ad_account: AdAccount):

        self.req_info = None
        self.user = None
        self.ad_account = ad_account


    def process_request(self, req_info: CampaignRequest, user: UserAccount):

        self.req_info = req_info
        self.user = user

        try:
            mapper: FacebookCampaignMapper = self.req_info.campaign_platform_mapper.facebook

            if mapper.campaign_id in (None, ""):
                self.create_campaign()
            else:
                self.update_campaign()

        except Exception as exc:
            print(exc)
            "TODO Exception handling"
            pass

        finally:
            return None

    def create_campaign(self):

        settings: FacebookCampaignSetting = self.req_info.campaign_settings.facebook

        params = {
            'name': self.req_info.name,
            'objective': settings.objective,
            'status': settings.status

        }

        try:
            campaign_res = self.ad_account.create_campaign(params=params)
            camp_id = campaign_res.get_id()
            if camp_id:

                self.req_info.campaign_platform_mapper.facebook.campaign_id = camp_id
                self.req_info.save()

            print("Campaign: {0} started and has id {1}".format(params['name'], campaign_res.get_id()))

            return campaign_res.get_id()

        except Exception as e:
            print(e)
            return None

    def update_campaign(self):

        camp_id = self.req_info.campaign_platform_mapper.facebook.campaign_id
        camp = Campaign(camp_id)

        fields = [
            Campaign.Field.name
        ]

        camp.api_get(fields=fields)

        if camp["name"] != self.req_info.name:
            camp.api_update(params={"name": self.req_info.name})

    def start_campaign(self):

        info = self.req_info.campaign_platform_mapper.facebook

        camp = Campaign(info.campaign_id)
        fields = [
            Campaign.Field.status
        ]
        camp.api_get(fields=fields)

        if camp["name"] != info.name:
            camp.api_update(params={"status": "ACTIVE"})
            info.campaign_status = "ACTIVE"
            self.req_info.save()

    def stop_campaign(self):

        info = self.req_info.campaign_platform_mapper.facebook

        camp = Campaign(info.campaign_id)
        fields = [
            Campaign.Field.status
        ]
        camp.api_get(fields=fields)

        if camp["name"] != info.name:
            camp.api_update(params={"status": "PAUSE"})
            info.campaign_status = "PAUSE"
            self.req_info.save()

    def get_campaign(self, ):

        info = self.req_info.campaign_platform_mapper.facebook

        camp = Campaign(info.campaign_id)
        fields = [
            Campaign.Field.status
        ]
        camp.api_get(fields=fields)

        if camp["name"] != info.name:
            camp.api_update(params={"status": "PAUSE"})
            info.campaign_status = "PAUSE"
            self.req_info.save()


