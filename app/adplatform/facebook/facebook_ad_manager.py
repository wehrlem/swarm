from facebook_business.api import FacebookAdsApi
from facebook_business.adobjects.adaccount import AdAccount
from facebook_business.adobjects.campaign import Campaign
from facebook_business import FacebookSession

from app.models.data.campaigns import Campaign as CampaignRequest
from app.models.data.config import Config as AccessConfig
from app.models.data.accounts import Account as UserAccount
from app.models.data.statistic import Statistic

from time import time
import app.database as db

from .facebook_campaign import CampaignHandler
from .facebook_adset import AdSetHandler
from .facebook_adcreative import AdHandler
from .facebook_stats import FacebookStats



class FacebookAdManager(object):

    def __init__(self, config: AccessConfig):
        self.request: CampaignRequest = None
        self.current_user: UserAccount = None
        self.config = config

        self.session = self.start_session()
        self.api = FacebookAdsApi(self.session)
        FacebookAdsApi.set_default_api(self.api)

        self.account = AdAccount(self.config.facebook.ad_account_id)

        self.camp_handler = CampaignHandler(self.account)
        self.adset_handler = AdSetHandler(self.account)
        self.ad_handler = AdHandler(self.account)

    def _reload_config(self):
        conf = db.facebook_config.get_default_config()
        if conf is not None:
            self.config = conf

    def start_session(self):
        """ Setup session and api objects"""
        session = FacebookSession(
            self.config.facebook.app_id,
            self.config.facebook.app_secret,
            self.config.facebook.access_token
        )

        return session

    def set_request(self, request: CampaignRequest, user: UserAccount):

        self.request = request
        self.current_user = user

    def handle_ad_request(self, request: CampaignRequest, user: UserAccount):

        if request and user:
            self.set_request(request, user)
        else:
            return

        start = time()

        self.camp_handler.process_request(request, user)
        self.adset_handler.process_request(request, user)
        self.ad_handler.process_request(request, user)

        end = time() - start
        print("Time {}".format(end))

    def get_campaign_stats(self, camp: CampaignRequest, stat: Statistic):

        mapper = camp.campaign_platform_mapper.facebook
        fs = FacebookStats(self.account)

        insights = fs.stats_by_campaign(camp)

        if insights:
            stat.facebook.clicks = int(insights['clicks'])
            stat.facebook.impressions = int(insights['impressions'])
            stat.facebook.averagecpc = float(insights['cpc'])
            stat.facebook.cost = float(insights['cost_per_result'])

        return insights






