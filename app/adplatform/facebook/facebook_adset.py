from facebook_business.adobjects.adset import AdSet
from facebook_business.adobjects.targetingsearch import TargetingSearch
from app.models.data.campaign_settings import FacebookTargetSet
from app.models.data.locations import Location
from facebook_business.adobjects.adaccount import AdAccount
from app.models.data.campaigns import Campaign as CampaignRequest
from app.models.data.campaign_platform_mapper import FacebookCampaignMapper
from app.models.data.accounts import Account as UserAccount

class AdSetHandler:

    def __init__(self, ad_account: AdAccount):

        self.req_info = None
        self.user = None
        self.ad_account = ad_account

    def process_request(self, req_info: CampaignRequest, user_account: UserAccount):

        self.req_info = req_info
        self.user = user_account

        try:
            mapper: FacebookCampaignMapper = self.req_info.campaign_platform_mapper.facebook

            if mapper.campaign_id not in (None, ""):
                if mapper.ad_set_id in (None, ""):
                    self.create_ad_set()
                else:
                    self.update_ad_set()

            else:
                raise Exception("Cannot create adset before campaign is started!")

        except Exception as exc:
            print(exc)
            "TODO Exception handling"
            pass

        finally:
            return None

    def create_ad_set(self):

        ad_setting = self.req_info.campaign_settings.facebook.ad_setting
        target_setting = self.req_info.campaign_settings.facebook.target_set
        mapper = self.req_info.campaign_platform_mapper.facebook


        try:
            if not self.req_info.targeting.locations or self.req_info.budget in (None, ""):
                raise Exception("Targeting or budget are not defined!")

            ad_set = AdSet(parent_id=self.ad_account.get_id_assured())

            lifetime_budget = None
            daily_budget = None

            if ad_setting.budget_type == "LIFETIME":
                lifetime_budget = self.get_lifetime_budget()
                if ad_setting.billing_event == AdSet.BillingEvent.impressions or ad_setting.billing_event == AdSet.BillingEvent.link_clicks:
                    lifetime_budget = lifetime_budget*100
                daily_budget = None
            else:
                lifetime_budget = None
                daily_budget = self.get_daily_budget()
                if ad_setting.billing_event == AdSet.BillingEvent.impressions or ad_setting.billing_event == AdSet.BillingEvent.link_clicks:
                    daily_budget = daily_budget*100

            params = {

                'campaign_id': mapper.campaign_id,
                'name': str.format('AdSet {0}', "Link AD"),
                'start_time': str(self.req_info.start_date),
                'end_time': str(self.req_info.end_date),
                'bid_strategy': ad_setting.bid_strategy,
                'billing_event':  ad_setting.billing_event,
                'optimization_goal': ad_setting.optimization_goal,
                'destination_type': ad_setting.destination_type,
                'lifetime_budget': lifetime_budget,
                'daily_budget': daily_budget,
                'targeting': self.set_targeting(target_setting, self.req_info.targeting)

            }
            print(params)
            adset_object = ad_set.remote_create(params=params)
            mapper.ad_set_id = adset_object.get_id()
            self.req_info.save()

        except Exception as e:
            print(e)
            return None

    def set_targeting(self, target_set: FacebookTargetSet, target_info, updating=False) -> dict:

        country_codes = []

        for loc in target_info.locations:
            loc_info = self.get_locality(loc)
            params = {
                "q": loc_info["country"],
                "type": "adcountry",

            }
            resp: TargetingSearch = TargetingSearch.search(params=params)
            country_codes.append(resp[0]["country_code"])

        print(country_codes)

        targ_dict = {
            'age_min': target_set.age_min,
            'age_max': target_set.age_max,
            'publisher_platforms': target_set.publisher_platforms,
            # 'keywords': trgConf.keywords,
            # 'interests': trgConf.interest,
            # 'behaviors': [{'id': '6002714898572', 'name': 'Small business owners'}],
            # 'languages': trgConf.languages,
            'device_platforms': target_set.device_platforms,
            'facebook_positions': target_set.facebook_positions,

            'geo_locations':
            {
                'countries': country_codes
            }
        }

        return targ_dict

    def update_ad_set(self):

        ad_setting = self.req_info.campaign_settings.facebook.ad_setting
        target_setting = self.req_info.campaign_settings.facebook.target_set
        mapper = self.req_info.campaign_platform_mapper.facebook

        adset = AdSet(mapper.ad_set_id)

        fields = [
            AdSet.Field.campaign_id,
            AdSet.Field.start_time,
            AdSet.Field.end_time,
            AdSet.Field.daily_budget,
            AdSet.Field.lifetime_budget,
            AdSet.Field.targeting

        ]
        adset.api_get(fields=fields)

        print(adset)

        if ad_setting.budget_type == "LIFETIME":
            lifetime_budget = self.get_lifetime_budget()
            if ad_setting.billing_event == AdSet.BillingEvent.impressions or ad_setting.billing_event == AdSet.BillingEvent.link_clicks:
                lifetime_budget = lifetime_budget * 100
            daily_budget = None
        else:
            lifetime_budget = None
            daily_budget = self.get_daily_budget()
            if ad_setting.billing_event == AdSet.BillingEvent.impressions or ad_setting.billing_event == AdSet.BillingEvent.link_clicks:
                daily_budget = daily_budget * 100

        if str(mapper.campaign_id) == adset["campaign_id"]:
            params = {
                "start_time": str(self.req_info.start_date),
                "end_time": str(self.req_info.end_date),
                'daily_budget': daily_budget,
                'lifetime_budget': lifetime_budget,

            }

            adset[AdSet.Field.start_time] = str(self.req_info.start_date)
            adset[AdSet.Field.end_time] = str(self.req_info.end_date)
            adset[AdSet.Field.daily_budget] = daily_budget
            adset[AdSet.Field.lifetime_budget] = lifetime_budget

            adset.remote_update(params ={
                'status': AdSet.Status.paused,
            })

        print(adset)

        # TODO Targeting

    def get_lifetime_budget(self):
        bdg = self.req_info.budget
        for i in range(len(bdg.budget_distribution)):
            if bdg.budget_distribution[i].platform == "facebook":
                return int(bdg.budget_distribution[i].netto_price)

        return None

    def get_daily_budget(self):
        life_budget = self.get_lifetime_budget()
        if life_budget:
            return life_budget / self.req_info.duration_in_days
        else:
            return None

    def get_locality(self, loc: Location) -> dict():

        locations = dict()

        for adr_comp in loc.address_components:
            locations[adr_comp.types[0]] = adr_comp.long_name

        return locations
