
from facebook_business.adobjects.adaccount import AdAccount
from app.models.data.config import Config as AccessConfig
from app.models.data.accounts import Account as UserAccount
from facebook_business.adobjects.adsinsights import AdsInsights
from facebook_business.adobjects.campaign import Campaign
from facebook_business.adobjects.adset import AdSet
from app.models.data.campaigns import Campaign as CampaignRequest


class FacebookStats(object):

    def __init__(self, ad_account: AdAccount):
        self.req_info = None
        self.user = None
        self.ad_account = ad_account

    def stats_by_campaign(self, camp: CampaignRequest):

        mapper = camp.campaign_platform_mapper.facebook
        if mapper.campaign_id not in (None, ""):

            campaign = Campaign(mapper.campaign_id)
            fields = [
                'cpc',
                'impressions',
                'reach',
                'cost_per_result',
                'cpp',
                'cpm',
                'actions:post_engagement',
                'cost_per_action_type:post_engagement',
                'actions:link_click',
                'website_ctr:link_click',
                'clicks',
                'unique_clicks',
                'account_name',
                'campaign_id',
                'campaign_name',
                'account_id',
                'campaign_start',
                'campaign_end',
                'spend',
            ]

            params = {
                'filtering': [],
                'breakdowns': [],
                'date_preset': 'lifetime',
            }

            report_data = campaign.get_insights(fields=fields, params=params)

        else:
            return None

    def retrieve_statistics(self) -> AdsInsights:

        fields = [
            'objective',
            'start_time',
            'stop_time',
            'clicks',
            'unique_clicks',
            'ctr',
            'actions:link_click',
            'website_ctr:link_click',
            'cost_per_action_type:like',
            'cost_per_action_type:post_engagement',
        ]

        #TODO select range#
        params = {
            'level': 'account',
            'filtering': [],
            'breakdowns': [],
            'time_range': {'since': '2019-09-08', 'until': '2019-10-08'},
        }

        ad_insight = AdAccount(self.ad_account.get_id()).get_insights(
            fields=fields,
            params=params,
        )

        return ad_insight


