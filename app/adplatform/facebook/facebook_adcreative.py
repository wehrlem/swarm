from facebook_business.adobjects.adaccount import AdAccount
from facebook_business.adobjects.ad import Ad
from facebook_business.adobjects.adcreative import AdCreative
from facebook_business.adobjects.adcreativelinkdata import AdCreativeLinkData
from facebook_business.adobjects.adcreativeobjectstoryspec import AdCreativeObjectStorySpec
from app.models.data.campaigns import Campaign as CampaignRequest
from app.models.data.campaigns import FacebookAd
from app.models.data.campaign_platform_mapper import FacebookCampaignMapper
from app.models.data.accounts import Account as UserAccount


class AdHandler:

    def __init__(self, account: AdAccount = None):

        self.req_info = None
        self.current_user: UserAccount = None
        self.account = account

    def process_request(self, req_info: CampaignRequest, user: UserAccount):

        self.req_info = req_info
        self.current_user = user

        try:
            mapper: FacebookCampaignMapper = self.req_info.campaign_platform_mapper.facebook

            if mapper.campaign_id not in (None, "") and mapper.ad_set_id not in (None, ""):
                if mapper.ad_ids not in (None, ""):
                    self.create_ads()
                else:
                    self.update_ad()

            else:
                raise Exception("Cannot create ad before adset is created!")

        except Exception as exc:
            print(exc)
            "TODO Exception handling"
            pass

        finally:
            return None

    def create_ads(self):

        ad_setting = self.req_info.campaign_settings.facebook.ad_setting
        mapper = self.req_info.campaign_platform_mapper.facebook

        fads: list[FacebookAd] = self.req_info.facebookads

        try:

            for fad in fads:

                link_data = AdCreativeLinkData()
                link_data[AdCreativeLinkData.Field.message] = fad.message
                link_data[AdCreativeLinkData.Field.image_hash] = fad.marketing_images[0]
                call_to_action = {
                    'type': str(ad_setting.call_to_action_type),
                    'value': {
                        'link': fad.url,
                    },
                }
                link_data[AdCreativeLinkData.Field.call_to_action] = call_to_action
                link_data[AdCreativeLinkData.Field.link] = fad.url

                object_story_spec = AdCreativeObjectStorySpec()
                object_story_spec[AdCreativeObjectStorySpec.Field.page_id] = self.current_user.platforms.facebook.facebook_page_id
                object_story_spec[AdCreativeObjectStorySpec.Field.link_data] = link_data

                creative = AdCreative(parent_id=self.account.get_id())
                print(creative)
                creative[AdCreative.Field.name] = 'AdCreative for Link Ad'
                creative[AdCreative.Field.object_story_spec] = object_story_spec
                creative[AdCreative.Field.title] = fad.title
                creative[AdCreative.Field.body] = fad.message
                # creative[AdCreative.Field.actor_id] = str(self.current_user.facebook_page_id)
                creative[AdCreative.Field.link_url] = fad.url
                #creative[AdCreative.Field.object_type] = AdCreative.ObjectType.domain

                creative.remote_create()

                print("creative:", creative.get_id())

                ad = Ad(None, self.account.get_id())
                ad[Ad.Field.name] = 'Some Ad Name'
                ad[Ad.Field.adset_id] = mapper.ad_set_id
                ad[Ad.Field.creative] = {'creative_id': creative.get_id()}
                ad.remote_create(params={
                    'status': Ad.Status.paused,
                })

                ad_id = str(ad[Ad.Field.id])

                mapper.ad_ids.append(ad_id)
                self.req_info.save()

        except Exception as e:
            print(e)

    def update_ad(self):

        mapper = self.req_info.campaign_platform_mapper.facebook
        ad_setting = self.req_info.campaign_settings.facebook.ad_setting

        try:
            fads: list[FacebookAd] = self.req_info.facebookads
            fields = [
                Ad.Field.name,
                Ad.Field.adset_id,
                Ad.Field.creative
            ]
            for i in range(len(fads)):
                ad = Ad(mapper.ad_ids[i])
                ad.api_get(fields=fields)

                link_data = AdCreativeLinkData()
                link_data[AdCreativeLinkData.Field.message] = fads[i].message
                link_data[AdCreativeLinkData.Field.image_hash] = fads[i].marketing_images[0]
                call_to_action = {
                    'type': str(ad_setting.call_to_action_type),
                    'value': {
                        'link': fads[i].url,
                    },
                }
                link_data[AdCreativeLinkData.Field.call_to_action] = call_to_action
                link_data[AdCreativeLinkData.Field.link] = fads[i].url

                object_story_spec = AdCreativeObjectStorySpec()
                object_story_spec[AdCreativeObjectStorySpec.Field.page_id] = self.current_user.platforms.facebook.facebook_page_id
                object_story_spec[AdCreativeObjectStorySpec.Field.link_data] = link_data

                creative: AdCreative = ad["creative"]
                creative[AdCreative.Field.object_story_spec] = object_story_spec
                creative[AdCreative.Field.title] = fads[i].title
                creative[AdCreative.Field.body] = fads[i].message
                creative[AdCreative.Field.link_url] = fads[i].url
                creative[AdCreative.Field.image_hash] = fads[i].marketing_images[0]

                creative.api_update()
                ad.api_update(fields=fields)

        except Exception as e:
            print(e)
