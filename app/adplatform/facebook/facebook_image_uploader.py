from facebook_business.api import FacebookAdsApi
from facebook_business.adobjects.adaccount import AdAccount
from facebook_business.adobjects.adimage import AdImage
from facebook_business import FacebookSession
from app.models.data.config import Config as AccessConfig
from app.models.data.accounts import Account as UserAccount
from time import time
import app.database as db

class FacebookImageUpload(object):

    def __init__(self):

        self.current_user: UserAccount = None
        self.config = self._get_config()

        self.session = self.start_session()
        self.api = FacebookAdsApi(self.session)
        FacebookAdsApi.set_default_api(self.api)
        self.account = AdAccount(self.config.facebook.ad_account_id)

    def start_session(self):
        """ Setup session and api objects"""
        session = FacebookSession(
            self.config.facebook.app_id,
            self.config.facebook.app_secret,
            self.config.facebook.access_token
        )

        return session

    def _get_config(self):
        conf = db.config.get_default_config()
        return conf

    def handle_image_request(self, image_path: str):

        start = time()

        img = AdImage(None, self.account.get_id_assured())

        img[AdImage.Field.filename] = image_path
        img.remote_create()

        end = time() - start

        hash = img["hash"]
        url = img["url"]

        return hash, url





