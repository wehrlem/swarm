import ftplib
import xml.etree.cElementTree as ET
import xmlschema
import xml.dom.minidom as minidom
import io
import datetime
from app.resources.schemas.campaign_schema import CampaignSchema
from .mapper import ATTR, LIST_MAPPER, LIST_KEY_MAPPER, KEY_MAPPER, attachments_doc_types


class SwissRetsCommunicator:

    def __init__(self, conn):

        self.connection = False

        assert "host" in conn
        assert "user" in conn
        assert "password" in conn
        assert "export_name" in conn
        assert "organisation_id" in conn
        assert "publisher_id" in conn
        assert "platform_name" in conn

        self.__host = conn['host']
        self.__user = conn["user"]
        self.__pass = conn["password"]
        self.__export_name = conn["export_name"]
        self.__organisation_id = conn["organisation_id"]
        self.__publisher_id = conn["publisher_id"]
        self.__platform_name = conn["platform_name"]

    def __connect(self):

        try:
            self.ftp = ftplib.FTP(self.__host, self.__user, self.__pass)
            self.connection = True
        except Exception:
            self.connection = False
            raise Exception("SwissRETS Communicator: Connection error")

    def __close_connection(self):
        if self.connection is True:
            self.ftp.quit()
            self.connection = False

    def json_to_xml2(self, json, element_name):

        # Map names
        if element_name in KEY_MAPPER.keys():
            element_name = KEY_MAPPER[element_name]

        root = ET.Element(element_name)

        if type(json) is dict:
            for key, value in json.items():
                if value is not None:
                    if key == "attachments":
                        # print("{}: {}".format(key, value))
                        attachments = ET.SubElement(root, "attachments")
                        for tkey, tvalues in value.items():
                            if tvalues is not None:
                                for x in tvalues:
                                    new_element = self.json_to_xml2(x, attachments_doc_types[tkey])
                                    if new_element is not None:
                                        attachments.append(new_element)
                    elif element_name in ATTR.keys() and key in ATTR[element_name]:
                        root.attrib[key] = value
                    elif key == 'value':
                        root.text = str(value)
                    else:
                        new_element = self.json_to_xml2(value, key)
                        if new_element is not None:
                            root.append(new_element)
        elif type(json) is list:
            if len(json) <= 0:
                return None
            n_element_name = None
            if element_name in LIST_KEY_MAPPER.keys():
                n_element_name = LIST_KEY_MAPPER[element_name]
            elif element_name in LIST_MAPPER.keys():
                n_element_name = LIST_MAPPER[element_name]
            if n_element_name is not None:
                for item in json:
                    new_element = self.json_to_xml2(item, n_element_name)
                    if new_element is not None:
                        root.append(new_element)
        else:
            root.text = str(json)
        return root

    def __get_base_export(self):

        # Create export
        root = ET.Element("export")

        # Created
        created = ET.SubElement(root, "created")
        x = datetime.datetime.now()
        created.text = x.strftime("%Y-%m-%dT%H:%M:%S")

        # Generator
        generator = ET.SubElement(root, "generator")
        generator.attrib["version"] = "1.0"
        generator.text = "SWARM"

        # Properties
        properties = ET.SubElement(root, "properties")

        return root, properties

    def __get_platform_data(self, campaign):
        try:
            for x in campaign["platforms"]:
                if x["name"] == self.__platform_name:
                    return x
        except:
            print("Can't find platform: {} data".format(self.__platform_name))
            return None

    def __add_property_options(self, p, campaign):
        # Add property ID
        p.attrib['id'] = campaign["apartment"]["reference_id"]

        # Add organisation ID
        org = p.find("seller/organization")
        if org is None:
            seller = ET.SubElement(p, "seller")
            org = ET.SubElement(seller, "organization")
        org.attrib["id"] = self.__organisation_id

        # Add publishers
        publishers = ET.SubElement(p, "publishers")
        publisher = ET.Element("publisher")
        publisher.attrib["id"] = self.__publisher_id
        publishers.append(publisher)

        # Get platform data
        platform_data = self.__get_platform_data(campaign)
        # Options package
        if platform_data is not None and "product" in platform_data:
            options = ET.SubElement(publisher, "options")
            option = ET.Element("option")
            option.attrib["key"] = "channel_{}_status".format(platform_data["product"])
            option.text = "[CDATA[active]]"
            options.append(option)

            # Chanels
            channels = ET.SubElement(publisher, "channels")
            channel = ET.Element("channel")

            if "start_date" in platform_data and platform_data["start_date"] is not None:
                channel.attrib["start"] = platform_data["start_date"]
            if "end_date" in platform_data and platform_data["end_date"] is not None:
                channel.attrib["expiration"] = platform_data["end_date"]
            channel.text = "[CDATA[{}]]".format(platform_data["product"])
            channels.append(channel)

    def __validate_schema(self, root):
        # Validate schema
        try:
            schema = xmlschema.XMLSchema('app/adplatform/swissrets/schema/schema.xsd')
            schema.validate(root)
        except Exception as ex:
            raise Exception("SwissRETS Communicator: XML document validation error {}".format(ex))

    def __parse_to_string(self, root):

        # Convert xml to string
        xml_str = ET.tostring(root, encoding='utf-8')
        reparsed = minidom.parseString(xml_str)
        export = reparsed.toprettyxml(indent="  ")
        export = export.replace("[CDATA[", "<![CDATA[")
        export = export.replace("]]", "]]>")
        return export

    def getExport(self, campaigns):

        ad_schema = CampaignSchema(strict=True, many=True)
        camp_res = ad_schema.dump(campaigns).data

        # Get export header
        root, props = self.__get_base_export()

        # Comvert SWAR Campaign to SwissRETS Property
        try:
            for x in camp_res:
                # Get property
                p = self.json_to_xml2(x["apartment"], "property")
                # Add options to property
                self.__add_property_options(p, x)
                props.append(p)
        except Exception:
            raise Exception("SwissRETS Communicator: JSON to XML error")

        ptree = ET.ElementTree(root)

        # Validate schema
        try:
            schema = xmlschema.XMLSchema('app/adplatform/swissrets/schema/schema.xsd')
            schema.validate(ptree)
        except Exception as ex:
            raise Exception("SwissRETS Communicator: XML document validation error {}".format(ex))

        root.attrib["xmlns:xsi"] = "http://www.w3.org/2001/XMLSchema-instance"
        root.attrib["xsi:noNamespaceSchemaLocation"] = "https://swissrets.ch/dist/v2.0.0/schema.xsd"

        return self.__parse_to_string(root)

    # def campaign_to_xml(self, campaign):
    #     # Get export header
    #     root, props = self.__get_base_export()
    #     # Comvert SWAR Campaign to SwissRETS Property
    #     try:
    #         p = self.json_to_xml2(campaign["apartment"], "property")
    #         # Add options to property
    #         self.__add_property_options(p, campaign)
    #         props.append(p)
    #     except Exception:
    #         raise Exception("SwissRETS Communicator: JSON to XML error")

    #     # Validate schema
    #     self.__validate_schema(root)

    #     return self.__parse_to_string(root)

    def push(self, xml_data):

        self.__connect()

        # Send to ftp
        try:
            export_byte = xml_data.encode('utf-8')
            tmp = io.BytesIO(export_byte)
            res = self.ftp.storbinary('STOR {}.xml'.format(self.__export_name), tmp)
            return True
        except Exception as ex:
            self.__close_connection()
            print(ex)
            return False

    def __del__(self):
        self.__close_connection()
