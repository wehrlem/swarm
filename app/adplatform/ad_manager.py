from app.database.campaign import Campaign
from app.database.account import Account
from app.adplatform.idx.base_idx import IdxManager
# from app.adplatform.swissrets.swisrets import SwissRetsCommunicator
from app.models.data.accounts import BasePlatformConfig
from app.models.data.common.configs import CampaignPlatformChoices
import logging

# Log message
image_publisher_logger = logging.getLogger('image_publisher_logger')
ad_info_logger = logging.getLogger('admanager_info_logger')
ad_error_logger = logging.getLogger('admanager_error_logger')
ad_warning_logger = logging.getLogger('admanager_warning_logger')


class AdPlatformManager(object):

    def __split_campaign_by_platforms(self, platforms_dict, campaigns):
        for x in campaigns:
            for p in x.active_platforms:
                if p.name not in platforms_dict.keys():
                    platforms_dict[p.name] = []
                platforms_dict[p.name].append(x)

    def __update_campaign_status(self, campaigns, platform_name):
        # Set platform to published
        if campaigns is not None:
            for x in campaigns:
                x.update_platform_status(platform_name)
                x.save()

    def __validate_config(self, user, platform_name, standard, platform_config):

        try:
            # standard, platform_config = user.getPlatformConfig(platform_name)
            if standard is None or platform_config is None:
                msg = "# {} {} User config is missing".format(user.email, platform_name)
                ad_warning_logger.warning(msg)
                print(msg)
                return False
            assert platform_config.host is not None
            assert platform_config.host != ''
            assert platform_config.username is not None
            assert platform_config.username != ''
            assert platform_config.password is not None
            assert platform_config.password != ''
            return True
        except Exception as ex:
            msg = "# user {}, platform '{}' Error in User Config, error msg: {}".format(user.email, platform_name, ex)
            print(msg)
            ad_error_logger.error(msg)
            return False

    # ------------------------------------------------------------------------
    # Pricate method: Publish data to platform
    # ------------------------------------------------------------------------

    def __publish_data(self, user, campaigns, platform_name):

        '''
            Publish data to platform from selected campaigns

            Args:
            user: Account
            campaigns: List<Campaigns>
            platform_name: Platform name
        '''

        # Get platform config
        standard, platform_config = user.getPlatformConfig(platform_name)

        # Validate config
        if not self.__validate_config(user, platform_name, standard, platform_config):
            return False

        # If platform is inactive send empty file
        if not user.getPlatformStatus(platform_name):
            campaigns = []

        # Send adds to platform
        try:
            # Process campaigns by IDX standard
            if standard == BasePlatformConfig.Standard.IDX:
                manager = IdxManager(platform_config, platform_name)
                records, _ = manager.getRecords(campaigns, user)
                if manager.push(records):
                    self.__update_campaign_status(campaigns, platform_name)
                    ids = ["{}.{}".format(x.project_id, x.apartment.reference_id) for x in campaigns]
                    msg = "[{}] [{}] [{} Campaigns] {}".format(user.username, platform_name, len(campaigns), ids)
                    ad_info_logger.info(msg)
                    return True
                else:
                    msg = "Error {} in sending ads to '{}' via standard {}".format(user.username, platform_name, standard)
                    ad_error_logger.warning(msg)
                    return False
            msg = "{} Method for standard '{}' does not implemented!".format(user.username, standard)
            ad_warning_logger.warning(msg)
            return False
        except Exception as ex:
            msg = "{} Error in sending ads to {}, error message: {}".format(user.username, platform_name, ex)
            ad_error_logger.error(msg)
            return False

    # ----------------------------------------------------------------------------------------
    # Pricate method: Publish images to platform
    # ----------------------------------------------------------------------------------------

    def __publish_images(self, user, camps, platform_name):
        """
            Publish images to platform from selected campaigns

            Args:
            user: Account
            camps: List<Campaigns>
            platform_name: Platform name
        """

        # Check if platform is active
        if not user.getPlatformStatus(platform_name):
            print("# '{}' is not active for user {}".format(platform_name, user.username))
            return False

        # Get platform config
        standard, config = user.getPlatformConfig(platform_name)

        # Validate config
        if not self.__validate_config(user, platform_name, standard, config):
            return False

        # Process images by IDX standard
        if standard == BasePlatformConfig.Standard.IDX:
            try:
                # Push Files
                # br_img, br_doc = 0, 0
                manager = IdxManager(config, platform_name)
                for x in camps:
                    br_img = manager.pushImages(x)
                    br_doc = manager.pushDocuments(x)
                    tmp_id = "{}.{}".format(x.apartment.reference_id, x.project_id)
                    msg = f'[{user.username}] [{tmp_id}] [{platform_name}] images: {br_img} documents: {br_doc}'
                    image_publisher_logger.info(msg)
                return True
            except Exception as ex:
                msg = "Error in sending files to {}, error message: {}".format(platform_name, ex)
                image_publisher_logger.error(msg)
                return False
        return False

    # ------------------------------------------------------------------------
    # Pricate method: Get team
    # ------------------------------------------------------------------------

    def get_team(self, user, all_teams=True):
        '''
            Select Master User, and Subuser
            Description:
            Master user is always in campaign,
            Sub User is selected by team from Campaign
        '''
        master_user = user.parrent if user.parrent else user
        sub_users = []
        if all_teams:
            sub_users = master_user.getActiveSubAccounts()
        elif user.parrent:
            sub_users = [user]
        return master_user, sub_users

    def publish(self, user: Account, images=False, all_team=True):
        '''
            Publish All Campaigns without images
        '''
        # Get Active Campaigns
        if not user:
            return False

        # if user.team:
        #    master_user, sub_users = None, [user]
        # else:
        #    master_user, sub_users = user, []

        master_user, sub_users = self.get_team(user, all_teams=all_team)

        response = {}
        res = {}
        # Publish to Master account
        if master_user:
            for platform_name in master_user.getActivePlatforms():
                res[platform_name] = {"data": None, "images": None, "campaigns": None}
                if platform_name == CampaignPlatformChoices.FLATFOX:
                    camps = Campaign.objects(account_id=master_user).get_published_by_platform(platform_name)
                else:
                    camps = Campaign.objects(account_id=master_user).exclude_active_teams(master_user, platform_name).get_published_by_platform(platform_name)
                res[platform_name]['data'] = self.__publish_data(master_user, camps, platform_name)
                res[platform_name]['campaigns'] = len(camps)
                if images:
                    res[platform_name]['images'] = self.__publish_images(master_user, camps, platform_name)
            response[master_user.username] = res
        # Publish to Subaccounts
        for sub_user in sub_users:
            res = {}
            for platform_name in sub_user.getActivePlatforms():
                if platform_name == CampaignPlatformChoices.FLATFOX:
                    continue
                res[platform_name] = {"data": None, "images": None}
                camps = Campaign.objects(account_id=master_user).add_team(sub_user).get_published_by_platform(platform_name)
                res[platform_name]['data'] = self.__publish_data(sub_user, camps, platform_name)
                res[platform_name]['campaigns'] = len(camps)
                if images:
                    res[platform_name]['images'] = self.__publish_images(sub_user, camps, platform_name)
            response[sub_user.username] = res
        return response

    def publishToPlatform(self, user, platform, images=False, all_team=False):
        '''
            Publish All Campaigns without images to platform
        '''
        if not user:
            return False

        master_user, sub_users = self.get_team(user, all_teams=all_team)

        response = {}

        # Publish to master User
        if master_user:
            platforms = master_user.getActivePlatforms()
            if platform in platforms:
                response[platform] = {}
                if platform == CampaignPlatformChoices.FLATFOX:
                    camps = Campaign.objects(account_id=master_user).get_published_by_platform(platform)
                else:
                    camps = Campaign.objects(account_id=master_user).exclude_active_teams(master_user, platform).get_published_by_platform(platform)
                response[platform]['data'] = self.__publish_data(master_user, camps, platform)
                if images:
                    response[platform]['images'] = self.__publish_images(master_user, camps, platform)

        # Publish to Subaccounts
        for sub_user in sub_users:
            if platform in sub_user.getActivePlatforms():
                if platform == CampaignPlatformChoices.FLATFOX:
                    continue
                response[platform] = {"data": None, "images": None}
                camps = Campaign.objects(account_id=master_user).add_team(sub_user).get_published_by_platform(platform)
                response[platform]['data'] = self.__publish_data(sub_user, camps, platform)
                if images:
                    response[platform]['images'] = self.__publish_images(sub_user, camps, platform)
        return response
    # ------------------------------------------------------------------------
    # Public methods (Publish Images for selected campaigns)
    # ------------------------------------------------------------------------

    def publishImages(self, campaigns, platform=None):
        '''
            Publish images for selected campaigns
        '''
        for camp in campaigns:
            for p_name, p_user in camp.get_published_platforms(platform).items():
                self.__publish_images(p_user, [camp], p_name)
