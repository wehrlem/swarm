import uuid
from app.models.helper.adsexception import AdsException
from app.models.data.campaigns import Campaign


class AdGroup(object):

    def __init__(self, client):
        self.client = client

    @classmethod
    def AdGroupOperations(cls, request: Campaign, campaign_id):

        # get setting
        max_cpc = request.campaign_settings.google.max_cpc_bid

        # Targeting settings
        targeting_settings = []
        for x in request.campaign_settings.google.targeting_settings:
            targeting_settings.append({
                'xsi_type': 'TargetingSettingDetail',
                'criterionTypeGroup': x.name,
                'targetAll': x.target_all
            })

        # Construct operations and add ad groups.
        operations = [{
            'operator': 'ADD',
            'operand': {
                'campaignId': campaign_id,
                'name': 'Group #{}'.format(uuid.uuid4()),
                'status': 'ENABLED',
                'biddingStrategyConfiguration': {
                    'bids': [
                        {
                            'xsi_type': 'CpcBid',
                            'bid': {
                                'microAmount': int(1000000 * max_cpc),
                            },
                        }
                    ]
                },
                'settings': [
                    {
                        # Targeting restriction settings.
                        # USER_INTEREST_AND_LIST - Targeting
                        # GENDER - Observation
                        # PARENT - Observation
                        # INCOME_RANGE - Observation
                        # AGE_RANGE - Observation
                        'xsi_type': 'TargetingSetting',
                        'details': [
                            {
                                'xsi_type': 'TargetingSettingDetail',
                                'criterionTypeGroup': 'USER_INTEREST_AND_LIST',
                                'targetAll': 'false',
                            },
                            {
                                'xsi_type': 'TargetingSettingDetail',
                                'criterionTypeGroup': 'GENDER',
                                'targetAll': 'true',
                            },
                            {
                                'xsi_type': 'TargetingSettingDetail',
                                'criterionTypeGroup': 'PARENT',
                                'targetAll': 'true',
                            },
                            {
                                'xsi_type': 'TargetingSettingDetail',
                                'criterionTypeGroup': 'INCOME_RANGE',
                                'targetAll': 'true',
                            },
                            {
                                'xsi_type': 'TargetingSettingDetail',
                                'criterionTypeGroup': 'AGE_RANGE',
                                'targetAll': 'true',
                            }
                        ]
                    }
                ]
            }
        }]

        return operations

    def UpdateAdGroupOperation(self, request: Campaign, ad_group_id):

        # get setting
        max_cpc = request.campaign_settings.google.max_cpc_bid

        # Targeting settings
        targeting_settings = []
        for x in request.campaign_settings.google.targeting_settings:
            targeting_settings.append({
                'xsi_type': 'TargetingSettingDetail',
                'criterionTypeGroup': x.name,
                'targetAll': x.target_all
            })

        # Construct operations and add ad groups.
        operand = {
            'id': ad_group_id,
            'status': 'ENABLED',
            'biddingStrategyConfiguration': {
                'bids': [
                    {
                        'xsi_type': 'CpcBid',
                        'bid': {
                            'microAmount': int(1000000 * max_cpc),
                        },
                    }
                ]
            },
            'settings': [
                {
                    # Targeting restriction settings.
                    # USER_INTEREST_AND_LIST - Targeting
                    # GENDER - Observation
                    # PARENT - Observation
                    # INCOME_RANGE - Observation
                    # AGE_RANGE - Observation
                    'xsi_type': 'TargetingSetting',
                    'details': targeting_settings
                }
            ]
        }
        return operand

    # Add ad groups to a campaign
    def AddAdGroup(self, request: Campaign, campaign_id):

        try:
            # Initialize appropriate service.
            ad_group_service = self.client.GetService('AdGroupService', version='v201809')

            # Add new group or update
            operations = AdGroup.AdGroupOperations(request, campaign_id)

            # Send request
            ad_groups = ad_group_service.mutate(operations)

            # Display results.
            for ad_group in ad_groups['value']:
                print('AdGroup with Id "{}" and name "{}" and was added.'.format(
                    ad_group['id'], ad_group['name']))

            # Return value
            return ad_groups['value'][0]['id']

        except Exception as e:
            raise AdsException("GoogleAddGroup error ", e.args)

    # Update Adgroup
    def UpdateAdGroup(self, request: Campaign, adgroup_id):

        try:
            # Initialize appropriate service.
            ad_group_service = self.client.GetService('AdGroupService', version='v201809')

            operations = [{
                'operator': 'SET',
                'operand': self.UpdateAdGroupOperation(request, adgroup_id)
            }]

            ad_groups = ad_group_service.mutate(operations)

            # Display results.
            for ad_group in ad_groups['value']:
                print('AdGroup with Id "{}" and name "{}" and was updated.'.format(
                    ad_group['id'], ad_group['name']))

            # Return value
            return ad_groups['value'][0]['id']

        except Exception as e:
            raise AdsException("GoogleAddGroup error ", e.args)
