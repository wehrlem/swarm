import uuid
from app.models.helper.adsexception import AdsException
from app.models.data.campaigns import Campaign as CampaignRequest


class Campaign(object):

    def __init__(self, client):
        self.client = client

    @classmethod
    def CampaignValue(cls, request: CampaignRequest):

        # Create campaign for search ADs
        campaign = {
            'name': '{0} #{1}'.format(request.name, uuid.uuid4()),
            # Recommendation: Set the campaign to PAUSED when creating it to
            # stop the ads from immediately serving. Set to ENABLED once you've
            # added targeting and the ads are ready to serve.
            'advertisingChannelType': 'SEARCH',
            'biddingStrategyConfiguration': {
                'biddingStrategyType': 'MANUAL_CPC',
            },
            'endDate': request.end_date.date(),
            'networkSetting': {
                'targetGoogleSearch': 'true',                   # Google.com search results
                'targetSearchNetwork': 'false',                 # Partner site in Google Search Network
                'targetContentNetwork': 'false',                # Google Display network
                'targetPartnerSearchNetwork': 'false'           # Trget partner search network
            },
            'settings': [
                {
                    'xsi_type': 'GeoTargetTypeSetting',
                    'positiveGeoTargetType': 'DONT_CARE',
                    'negativeGeoTargetType': 'DONT_CARE'
                }
            ]
        }

        return campaign

    @classmethod
    def CampaignAddOperation(cls, request: CampaignRequest, budget_id):

        campaign = cls.CampaignValue(request)
        # campaign['name'] = '{0} #{1}'.format(request.name, uuid.uuid4()),
        campaign['status'] = 'PAUSED'
        campaign['budget'] = {'budgetId': budget_id}
        campaign['startDate'] = request.start_date.date()

        # Construct campaing operation
        campaign_operations = [
            {
                'xsi_type': 'CampaignOperation',
                'operator': 'ADD',
                'operand': campaign
            }
        ]

        return campaign_operations

    @classmethod
    def CampaignUpdateperation(cls, request: CampaignRequest, campaign_id):

        campaign = cls.CampaignValue(request)
        campaign['id'] = campaign_id

        # Construct campaing operation
        campaign_operations = [
            {
                'xsi_type': 'CampaignOperation',
                'operator': 'SET',
                'operand': campaign
            }
        ]

        return campaign_operations

    @classmethod
    def CampaignRunOperation(cls, campaign_id):

        operations = [{
            'operator': 'SET',
            'operand': {
                'id': campaign_id,
                'status': 'ENABLED',
            }
        }]

        return operations

    @classmethod
    def CampaignStopOperation(cls, campaign_id):

        operations = [{
            'operator': 'SET',
            'operand': {
                'id': campaign_id,
                'status': 'PAUSED',
            }
        }]

        return operations

    def AddCampaign(self, request: CampaignRequest, budget_id):

        try:
            # Campaing service
            campaign_service = self.client.GetService('CampaignService', version='v201809')

            # Send campaign request
            operations = Campaign.CampaignAddOperation(request, budget_id)

            # Send request
            result = campaign_service.mutate(operations)['value'][0]

            # Display results
            print('Campaign with Id "{}" and name "{}" ({}) was added'.format(result['id'], result['name'], result['status']))

            return result['id']

        except Exception as e:
            raise AdsException("ERROR AddCampaign", e.args)

    def UpdateCampaign(self, request: CampaignRequest, campaign_id):

        try:
            # Campaing service
            campaign_service = self.client.GetService('CampaignService', version='v201809')

            # Send campaign request
            operations = Campaign.CampaignUpdateperation(request, campaign_id)

            # Send request
            result = campaign_service.mutate(operations)['value'][0]

            # Display results
            print('Campaign with Id "{}" and name "{}" ({}) was updated.'.format(result['id'], result['name'], result['status']))

            return result['id']

        except Exception as e:
            raise AdsException("ERROR AddCampaign", e.args)

    def RunCampaign(self, campaign_id):

        try:
            # Campaing service
            campaign_service = self.client.GetService('CampaignService', version='v201809')

            # Send campaign run request
            operations = Campaign.CampaignRunOperation(campaign_id)
            campaign = campaign_service.mutate(operations)['value'][0]

            # Display results
            print('Campaign with Id "{}" and name {} was {}'.format(
                campaign['id'], campaign['name'], campaign['status']))

        except Exception as e:
            raise AdsException("RunCampaign", e.args)

    def StopCampaign(self, campaign_id):

        try:
            # Campaing service
            campaign_service = self.client.GetService('CampaignService', version='v201809')

            # Send campaign run request
            operations = Campaign.CampaignStopOperation(campaign_id)
            campaign = campaign_service.mutate(operations)['value'][0]

            # Display results
            print('Campaign with Id "{}" and name {} was {}'.format(
                campaign['id'], campaign['name'], campaign['status']))

        except Exception as e:
            raise AdsException("RunCampaign", e.args)
