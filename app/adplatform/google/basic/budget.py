import uuid
from app.models.helper.adsexception import AdsException
from app.models.data.campaigns import Campaign


class Budget(object):

    def __init__(self, client):
        self.client = client

    @classmethod
    def BudgetOperation(cls, request: Campaign):

        # Get google Daily budget
        value = request.campaign_settings.google.daily_budget

        # Budget value
        bdg = None
        if request.budget is not None:
            bdg = [x for x in request.budget.budget_distribution if x.ad_type == "search" and x.platform == "google"][0]
        if bdg is not None:
            if bdg.netto_price is not None and request.duration_in_days is not None:
                duration = request.duration_in_days
                value = bdg.netto_price / duration

        # Micro Amount
        microAmount = int(value * 1000000)

        # Budget
        budget = {
            'amount': {
                'microAmount': microAmount
            }
        }

        return budget

    @classmethod
    def BudgetAddOperation(cls, request):

        budget = cls.BudgetOperation(request)
        budget['name'] = 'Budget #{}'.format(uuid.uuid4())
        budget['deliveryMethod'] = 'STANDARD'
        # Budget is not share with others campaigns
        budget['isExplicitlyShared'] = 'false'

        # Budget operation
        budget_operations = [{
            'xsi_type': 'BudgetOperation',
            'operator': 'ADD',
            'operand': budget
        }]

        return budget_operations


    @classmethod
    def BudgetUpdateOperation(cls, request, budget_id):

        # Budget
        budget = cls.BudgetOperation(request)
        budget['budgetId'] = budget_id

        # Budget operation
        budget_operations = [{
            'xsi_type': 'BudgetOperation',
            'operator': 'SET',
            'operand': budget
        }]

        return budget_operations

    def AddBudget(self, request):

        try:
            # Budget Service
            budget_service = self.client.GetService('BudgetService', version='v201809')

            # Add the budget
            operations = Budget.BudgetAddOperation(request)

            # Send request
            response = budget_service.mutate(operations)['value'][0]

            # Display results
            print('Budget with Id "{}" and name "{}" ({}) was added.'.format(
                response['budgetId'], response['name'], response['status']))

            # Return Budget Id
            return response['budgetId']

        except Exception:
            # Log error
            raise Exception("Google AddBudget error")

    def UpdateBudget(self, request, budget_id):

        try:
            # Budget Service
            budget_service = self.client.GetService('BudgetService', version='v201809')

            # Add the budget
            operations = Budget.BudgetUpdateOperation(request, budget_id)

            # Send request
            response = budget_service.mutate(operations)['value'][0]

            # Display results
            print('Budget with Id "{}" and name "{}" ({}) was updated.'.format(
                response['budgetId'], response['name'], response['status']))

        except Exception:
            # Log error
            raise Exception("Google UpdateBudget error ")
