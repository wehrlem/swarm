from . import textad


class Ads(object):

    def __init__(self, client):
        self.client = client

    def AddTextAds(self, ad, ad_group_id):
        return textad.AddTextAd(self.client, ad, ad_group_id)

    def UpdateTextAds(self, ad, text_ad_id):
        textad.UpdateTextAds(self.client, ad, text_ad_id)
