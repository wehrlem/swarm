from app.models.data.googleads import GoogleAd
from app.models.helper.adsexception import AdsException


def TexAd(ad: GoogleAd):

    ad = {
        'xsi_type': 'ExpandedTextAd',
        'headlinePart1': ad.headline1,
        'headlinePart2': ad.headline2,
        'headlinePart3': ad.headline3,
        'description': ad.descriptions[0],
        'finalUrls': ad.url,
    }

    return ad


def TexAdAddOperation(ad: GoogleAd, ad_group_id):

    ad = TexAd(ad)
    operation = {
        'operator': 'ADD',
        'operand': {
            'xsi_type': 'AdGroupAd',
            'adGroupId': ad_group_id,
            'ad': ad,
            # Optional fields.
            'status': 'ENABLED'
        }
    }

    return operation


def TextAdUpdateOperation(ad: GoogleAd, text_ad_id):

    text_ad = TexAd(ad)
    text_ad['id'] = text_ad_id
    operation = {
        'operator': 'SET',
        'operand': text_ad
    }
    return operation


def AddTextAd(client, ad: GoogleAd, ad_group_id):

    try:
        operations = []
        operation = TexAdAddOperation(ad, ad_group_id)
        operations.append(operation)
        # Add Ads
        if len(operations) > 0:
            # Add service
            service = client.GetService('AdGroupAdService', version='v201809')
            # Send Add request
            response = service.mutate(operations)
            # Display results.
            ads_id = []
            for res in response['value']:
                print('Ad with ID "{}" Headline1: "{}" was added.'.format(res['ad']['id'], res['ad']['headlinePart1']))
                ads_id.append(res['ad']['id'])

            # Ads config
            return ads_id[0]

    except Exception as e:
            raise AdsException("Error AddTextAd", e)


def UpdateTextAds(client, ad: GoogleAd, text_ad_id):

    operations = []
    operation = TextAdUpdateOperation(ad, text_ad_id)
    operations.append(operation)
    # Update Ads
    if len(operations) > 0:
        # Update service
        service = client.GetService('AdService', version='v201809')
        # Send update request
        response = service.mutate(operations)
        # Display results.
        for res in response['value']:
            print('Ad with ID "{}" Headline1: "{}" was updated.'.format(
                res['id'], res['headlinePart1']))
