import requests


def __UploadImageAsset(client, url):

    # Initialize appropriate service.
    asset_service = client.GetService('AssetService', version='v201809')

    # Download the image.
    image_request = requests.get(url).content

    # Create the image asset.
    image_asset = {
        'xsi_type': 'ImageAsset',
        'imageData': image_request,
        # This field is optional, and if provided should be unique.
        # 'assetName': 'Image asset ' + str(uuid.uuid4()),
    }

    # Create the operation.
    operation = {
        'operator': 'ADD',
        'operand': image_asset
    }

    # Create the asset and return the ID.
    result = asset_service.mutate([operation])['value'][0]

    print('Assert type {} with Id {} upload'.format(result['assetSubtype'], result['assetId']))

    return result['assetId']


def __CreateAssetResponsiveDisplayAd(client, ad_group_id, ad):
    # Create the ad.
    multi_asset_responsive_display_ad = {
        'xsi_type': 'MultiAssetResponsiveDisplayAd',
        'headlines': [{
            'asset': {
                'xsi_type': 'TextAsset',
                'assetText': ad['short_headlines'][i]
            }
        }for i in range(len(ad['short_headlines']))],
        'descriptions': [{
            'asset': {
                'xsi_type': 'TextAsset',
                'assetText': ad['descriptions'][i],
            }
        }for i in range(len(ad['descriptions']))],
        'businessName': ad['business_name'],
        'longHeadline': {
            'asset': {
                'xsi_type': 'TextAsset',
                'assetText': ad['long_headline'],
            }
        },
        # This ad format does not allow the creation of an image asset by setting
        # the asset.imageData field. An image asset must first be created using
        # the AssetService, and asset.assetId must be populated when creating
        # the ad.
        'marketingImages': [{
            'asset': {
                'xsi_type': 'ImageAsset',
                'assetId': __UploadImageAsset(client, ad['marketing_images'][i])
            }
        }for i in range(len(ad['marketing_images']))],
        'squareMarketingImages': [{
            'asset': {
                'xsi_type': 'ImageAsset',
                'assetId': __UploadImageAsset(client, ad['square_marketing_images'][i])
            }
        }for i in range(len(ad['square_marketing_images']))],
        # Optional values
        'finalUrls': [ad['url']],
        # 'callToActionText': 'Shop Now',
        # Set color settings using hexadecimal values. Set allowFlexibleColor to
        # false if you want your ads to render by always using your colors
        # strictly.
        'mainColor': '#0000ff',
        'accentColor': '#ffff00',
        'allowFlexibleColor': False,
        'formatSetting': 'NON_NATIVE',
        # Set dynamic display ad settings, composed of landscape logo image,
        # promotion text, and price prefix.
        'dynamicSettingsPricePrefix': 'as low as',
        'dynamicSettingsPromoText': 'Free shipping!',
        'logoImages': [{
            'asset': {
                'xsi_type': 'ImageAsset',
                'assetId': __UploadImageAsset(client, ad['logo_images'][i])
            }
        }for i in range(len(ad['logo_images']))]
    }

    # Create ad group ad.
    ad_group_ad = {
        'adGroupId': ad_group_id,
        'ad': multi_asset_responsive_display_ad,
        # Optional.
        'status': 'ENABLED'
    }

    return ad_group_ad


def Create(client, ad_group_id, ads):

    # Initialize appropriate service.
    ad_group_ad_service = client.GetService('AdGroupAdService', version='v201809')

    # Create ads
    operations = [{
        'operator': 'ADD',
        'operand': __CreateAssetResponsiveDisplayAd(client, ad_group_id, ads[i]),
    }for i in range(len(ads))]

    # Publish ads
    ads = ad_group_ad_service.mutate(operations)

    # Display results.
    if 'value' in ads:
        for ad in ads['value']:
            print('Added new responsive display ad ad with ID "{}" and long headline "{}".'.format(
                ad['ad']['id'], ad['ad']['longHeadline']['asset']['assetText']))
    else:
        print('No ads were added.')
