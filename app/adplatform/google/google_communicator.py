from .google_base import GoogleBase
from app.database.config import get_default_config
from .targeting import languages
from .targeting import user_interest


class GoogleCommunicator(GoogleBase):

    def __init__(self):
        config = get_default_config()
        super().__init__(config)

    def GetLanguages(self):
        return languages.GetLanguages(self.client)

    def GetUserInterest(self):
        return user_interest.GetUserInterest(self.client)

    def GetUserAffinity(self):
        return user_interest.GetUserAffinity(self.client)
