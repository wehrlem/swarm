from typing import List
from app.models.data.google import GoogleInterest
from app.models.helper.adsexception import AdsException
from app.models.data.campaign_platform_mapper import GoogleEntityMapper
from app.models.data.common.configs import Status


def __GetUserInterest(client, interest_type):

    # Initialize appropriate service.
    service = client.GetService('ConstantDataService', version='v201809')

    # Send request
    response = service.getUserInterestCriterion(interest_type)
    interests = []
    for res in response:
        tmp = GoogleInterest()
        tmp.interest_id = int(res['userInterestId'])
        tmp.parent_id = int(res['userInterestParentId'])
        tmp.name = res['userInterestName']
        tmp.type = interest_type
        interests.append(tmp)
        # print("{} {} {} {}".format(tmp.interest_id, tmp.type, tmp.interest_id, tmp.name))

    return interests


def GetUserInterest(client):
    return __GetUserInterest(client, 'IN_MARKET')


def GetUserAffinity(client):
    return __GetUserInterest(client, 'BRAND')


def AddUserInterest(client, ad_group_id, user_interest_list: List[GoogleEntityMapper]):

    try:

        # Initialize appropriate service.
        service = client.GetService('AdGroupCriterionService', version='v201809')

        send_user_interest = []
        operations = []

        for x in user_interest_list:
            if x.status is Status.ADD:
                # Add to send list
                send_user_interest.append(x)
                operation = {
                    'operator': 'ADD',
                    'operand': {
                        'xsi_type': 'BiddableAdGroupCriterion',
                        'adGroupId': ad_group_id,
                        'criterion': {
                            'xsi_type': 'CriterionUserInterest',
                            'userInterestId': x.entity.interest_id
                        }
                    }
                }
                operations.append(operation)
            elif x.status is Status.REMOVE:
                # Add to send list
                send_user_interest.append(x)
                operation = {
                    'operator': 'REMOVE',
                    'operand': {
                        'xsi_type': 'BiddableAdGroupCriterion',
                        'adGroupId': ad_group_id,
                        'criterion': {
                            'xsi_type': 'CriterionUserInterest',
                            'id': x.google_id
                        }
                    }
                }
                operations.append(operation)

        if len(operations) > 0:

            # Send request
            results = service.mutate(operations)['value']

            i = 0
            for res in results:
                # Criterion id
                id = res['criterion']['id']
                # Update status
                if send_user_interest[i].status is Status.ADD:
                    send_user_interest[i].status = Status.ACTIVE
                    send_user_interest[i].google_id = id
                    print("Add {} {} {}".format(res['criterion']['type'], res['criterion']['userInterestId'], res['criterion']['userInterestName']))
                elif send_user_interest[i].status is Status.REMOVE:
                    user_interest_list.remove(send_user_interest[i])
                    print("Remove {} {} {}".format(res['criterion']['type'], res['criterion']['userInterestId'], res['criterion']['userInterestName']))
                i += 1
        else:
            print("User Interest was up to date")

    except Exception as e:
        print("User interest error {} ".format(e))
