
def __GetAgeGroups(age_from, age_to):

    AGE_RANGE_18_24 = 503001
    AGE_RANGE_25_34 = 503002
    AGE_RANGE_35_44 = 503003
    AGE_RANGE_45_54 = 503004
    AGE_RANGE_55_64 = 503005
    AGE_RANGE_65_UP = 503006
    AGE_RANGE_UNDETERMINED = 503999

    # Pozitive AgeGroup List
    positive = [
        AGE_RANGE_18_24,
        AGE_RANGE_25_34,
        AGE_RANGE_35_44,
        AGE_RANGE_45_54,
        AGE_RANGE_55_64,
        AGE_RANGE_65_UP,
        AGE_RANGE_UNDETERMINED]

    # Negative AgeGroup List
    negative = []

    # Selection
    if age_from > 24 or age_to < 18:
        negative.append(AGE_RANGE_18_24)
        positive.remove(AGE_RANGE_18_24)
    if age_from > 34 or age_to < 25:
        negative.append(AGE_RANGE_25_34)
        positive.remove(AGE_RANGE_25_34)
    if age_from > 44 or age_to < 35:
        negative.append(AGE_RANGE_35_44)
        positive.remove(AGE_RANGE_35_44)
    if age_from > 54 or age_to < 45:
        negative.append(AGE_RANGE_45_54)
        positive.remove(AGE_RANGE_45_54)
    if age_from > 64 or age_to < 55:
        negative.append(AGE_RANGE_55_64)
        positive.remove(AGE_RANGE_55_64)
    if age_to < 65:
        negative.append(AGE_RANGE_65_UP)
        positive.remove(AGE_RANGE_65_UP)

    # Return Lists
    return positive, negative


def __GetAdGroupCriterionLists(service, ad_group_id):
    # Selector
    selector = {
        'fields': ['AdGroupId', 'Id', 'CriteriaType'],
        'predicates': [
            {
                'field': 'AdGroupId',
                'operator': 'EQUALS',
                'values': [ad_group_id]
            },
            {
                'field': 'CriteriaType',
                'operator': 'IN',
                'values': ['AGE_RANGE']
            },
        ],
        'paging': {
            'startIndex': str(0),
            'numberResults': str(10)
        }
    }

    # Get Request
    page = service.get(selector)

    # Filter pozitive and negative age groups
    negative = [adc['criterion']['id'] for adc in page['entries'] if adc['criterionUse'] == 'NEGATIVE']
    pozitive = [adc['criterion']['id'] for adc in page['entries'] if adc['criterionUse'] == 'BIDDABLE']

    # Return
    return pozitive, negative


def __RemoveAgeGroupCriterion(service, ad_group_id):

    # Get AgeGroup pozitive and negative list criterion
    pozitive, negative = __GetAdGroupCriterionLists(service, ad_group_id)

    # Operations for pozitive AgeGroups
    if len(pozitive) > 0:
        poz_operations = [{
            'operator': 'REMOVE',
            'operand': {
                'xsi_type': 'BiddableAdGroupCriterion',
                'adGroupId': ad_group_id,
                'criterion': {
                    'xsi_type': 'AgeRange',
                    'id': pozitive[i]
                }
            }
        }for i in range(len(pozitive))]

        res_poz = service.mutate(poz_operations)['value']
        for res in res_poz:
            print('Remove Positive {} Criterium'.format(res['criterion']['ageRangeType']))

    # Operations for pozitive AgeGroups
    if len(negative) > 0:
        neg_operations = [{
            'operator': 'REMOVE',
            'operand': {
                'xsi_type': 'NegativeAdGroupCriterion',
                'adGroupId': ad_group_id,
                'criterion': {
                    'xsi_type': 'AgeRange',
                    'id': negative[i]
                }
            }
        }for i in range(len(negative))]

        res_neg = service.mutate(neg_operations)['value']
        for res in res_neg:
            print('Remove Negative {} Criterium'.format(res['criterion']['ageRangeType']))


def AddAgeRange(self, ad_group_id, age_from, age_to):

    # AdGroup service
    ad_group_criterion_service = self.client.GetService('AdGroupCriterionService', version='v201809')

    # Remove Criterium
    __RemoveAgeGroupCriterion(ad_group_criterion_service, ad_group_id)

    # AgeGroup Pozitive and negative list
    poz, neg = __GetAgeGroups(age_from, age_to)

    # Add pozitive Criterion
    operations = [{
        'operator': 'ADD',
        'operand': {
            'xsi_type': 'BiddableAdGroupCriterion',
            'adGroupId': ad_group_id,
            'criterion': {
                'xsi_type': 'AgeRange',
                'id': poz[i]
            }
        }
    }for i in range(len(poz))]

    # Add negative criterion
    for criterion_id in neg:
        operations.append({
            'operator': 'ADD',
            'operand': {
                'xsi_type': 'NegativeAdGroupCriterion',
                'adGroupId': ad_group_id,
                'criterion': {
                    'xsi_type': 'AgeRange',
                    'id': criterion_id
                }
            }
        })

    response = ad_group_criterion_service.mutate(operations)['value']

    # Print results
    for x in response:
        negative = ' negative' if x['AdGroupCriterion.Type'] == 'NegativeAdGroupCriterion' else ''
        print('Add{} {}'.format(negative, x['criterion']['ageRangeType']))
