MALE = 10
FEMALE = 11
UNDETERMINED = 20


def __GetAdGroupCriterionLists(service, ad_group_id):
    # Selector
    selector = {
        'fields': ['AdGroupId', 'Id', 'CriteriaType'],
        'predicates': [
            {
                'field': 'AdGroupId',
                'operator': 'EQUALS',
                'values': [ad_group_id]
            },
            {
                'field': 'CriteriaType',
                'operator': 'IN',
                'values': ['GENDER']
            },
        ],
        'paging': {
            'startIndex': str(0),
            'numberResults': str(10)
        }
    }

    # Get Request
    page = service.get(selector)

    # Filter pozitive and negative age groups
    negative = [adc['criterion']['id'] for adc in page['entries'] if adc['criterionUse'] == 'NEGATIVE']
    pozitive = [adc['criterion']['id'] for adc in page['entries'] if adc['criterionUse'] == 'BIDDABLE']

    # Return
    return pozitive, negative


def __RemoveAgeGroupCriterion(service, ad_group_id):

    # Get AgeGroup pozitive and negative list criterion
    pozitive, negative = __GetAdGroupCriterionLists(service, ad_group_id)

    # Operations for pozitive AgeGroups
    if len(pozitive) > 0:
        poz_operations = [{
            'operator': 'REMOVE',
            'operand': {
                'xsi_type': 'BiddableAdGroupCriterion',
                'adGroupId': ad_group_id,
                'criterion': {
                    'xsi_type': 'Gender',
                    'id': pozitive[i]
                }
            }
        }for i in range(len(pozitive))]

        res_poz = service.mutate(poz_operations)['value']
        for res in res_poz:
            print('Remove Positive {} Criterium'.format(res['criterion']['genderType']))

    # Operations for pozitive AgeGroups
    if len(negative) > 0:
        neg_operations = [{
            'operator': 'REMOVE',
            'operand': {
                'xsi_type': 'NegativeAdGroupCriterion',
                'adGroupId': ad_group_id,
                'criterion': {
                    'xsi_type': 'Gender',
                    'id': negative[i]
                }
            }
        }for i in range(len(negative))]

        res_neg = service.mutate(neg_operations)['value']
        for res in res_neg:
            print('Remove Negative {} Criterium'.format(res['criterion']['genderType']))


def AddGender(creator, ad_group_id, gender):

    # AdGroup service
    ad_group_criterion_service = creator.client.GetService('AdGroupCriterionService', version='v201809')

    # Remove Criterium
    __RemoveAgeGroupCriterion(ad_group_criterion_service, ad_group_id)

    poz = [UNDETERMINED]
    neg = [MALE, FEMALE]
    for tmp in gender:
        if tmp == 'MALE':
            poz.append(MALE), neg.remove(MALE)
        elif tmp == 'FEMALE':
            poz.append(FEMALE), neg.remove(FEMALE)

    operations = []

    # Add pozitive Gender Criterion
    for gender in poz:
        operations.append({
            'operator': 'ADD',
            'operand': {
                'xsi_type': 'BiddableAdGroupCriterion',
                'adGroupId': ad_group_id,
                'criterion': {
                    'xsi_type': 'Gender',
                    'id': gender
                }
            }
        })

    # Add Negative Gender Criterion
    for gender in neg:
        operations.append({
            'operator': 'ADD',
            'operand': {
                'xsi_type': 'NegativeAdGroupCriterion',
                'adGroupId': ad_group_id,
                'criterion': {
                    'xsi_type': 'Gender',
                    'id': gender
                }
            }
        })

    response = ad_group_criterion_service.mutate(operations)['value']
    for x in response:
        negative = ' negative' if x['AdGroupCriterion.Type'] == 'NegativeAdGroupCriterion' else ' positive'
        print('Add{} {}'.format(negative, x['criterion']['genderType']))
