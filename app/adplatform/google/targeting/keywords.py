from app.models.helper.adsexception import AdsException
from app.models.data.campaign_platform_mapper import GoogleEntityMapper
from app.models.data.common.configs import Status
from typing import List


def KeywordOperation(ad_group_id, keywords: List[GoogleEntityMapper]):

    # Construct operations and add ad group criteria.
    operations = []
    keys = {}
    for keyword in keywords:
        if keyword.status == Status.ADD:
            operation = {
                'operator': 'ADD',
                'operand': {
                    'xsi_type': 'BiddableAdGroupCriterion',
                    'adGroupId': ad_group_id,
                    'criterion': {
                        'xsi_type': 'Keyword',
                        'matchType': 'EXACT',
                        'text': keyword.name
                    }
                }
            }
            operations.append(operation)
            keys[keyword.name] = keyword
        elif keyword.status == Status.REMOVE:
            operation = {
                'operator': 'REMOVE',
                'operand': {
                    'xsi_type': 'BiddableAdGroupCriterion',
                    'adGroupId': ad_group_id,
                    'criterion': {
                        'id': keyword.google_id
                    }
                }
            }
            operations.append(operation)
            keys[keyword.name] = keyword
    return operations, keys


def AddKeywords(client, ad_group_id, keywords: List[GoogleEntityMapper]):

    try:
        # Initialize appropriate service.
        ad_group_criterion_service = client.GetService('AdGroupCriterionService', version='v201809')

        # Add keywords request
        operations, keys = KeywordOperation(ad_group_id, keywords)

        if len(operations) <= 0:
            print("Keywords was up to date")
            return

        # Send request
        ad_group_criteria = ad_group_criterion_service.mutate(operations)['value']

        # Display results.
        for criterion in ad_group_criteria:

            # Get value
            value = criterion['criterion']['text']

            msg = "added"
            # Update keyword status
            if keys[value].status == Status.ADD:
                keys[value].google_id = criterion['criterion']['id']
                keys[value].status = Status.ACTIVE
            elif keys[value].status == Status.REMOVE:
                keywords.remove(keys[value])
                msg = "removed"

            # Display message
            print('Keyword criteria with Id "{}", text "{}", match type "{}" was {} in AdGroup with Id "{}",'.format(
                criterion['criterion']['id'],
                criterion['criterion']['text'],
                criterion['criterion']['matchType'],
                msg,
                criterion['adGroupId']))

    except Exception as e:
        raise AdsException("AddKeywords error", e)


def UpdateKeyword(client, ad_group_id, keyword_to_remove, keyword_to_add):

    # Initialize appropriate service.
    ad_group_criterion_service = client.GetService('AdGroupCriterionService', version='v201809')

    # Construct operations and add ad group criteria.
    operations = []

    for keyword in keyword_to_add:
        operand = {
            'operator': 'ADD',
            'operand': {
                'xsi_type': 'BiddableAdGroupCriterion',
                'adGroupId': ad_group_id,
                'criterion': {
                    'xsi_type': 'Keyword',
                    'matchType': 'EXACT',
                    'text': keyword
                }
            }
        }
        operations.append(operand)

    for keyword in keyword_to_remove:
        operand = {
            'operator': 'REMOVE',
            'operand': {
                'xsi_type': 'BiddableAdGroupCriterion',
                'adGroupId': ad_group_id,
                'criterion': {
                    'id': keyword.id
                }
            }
        }
        operations.append(operand)

    if len(operations) > 0:

        # Send request
        ad_group_criteria = ad_group_criterion_service.mutate(operations)['value']

        # Display results.
        for criterion in ad_group_criteria:
            removed = 'ADD'
            if criterion['userStatus'] is not None:
                removed = "REMOVE"
            print('{} Keyword criteria with Id "{}", text "{}", match type "{}" - AdGroup with Id "{}",'.format(
                removed,
                criterion['criterion']['id'],
                criterion['criterion']['text'],
                criterion['criterion']['matchType'],
                criterion['adGroupId']))
    else:
        print("Keywords was up to date")
