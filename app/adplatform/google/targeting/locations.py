from typing import List
from app.models.helper.adsexception import AdsException
from app.models.data.locations import Location
from ..helper.location import Location as LocationHelper


def GetLocationDetails(client, location_ids):

    # AdGroup service
    service = client.GetService('LocationCriterionService', version='v201809')

    # Select location name
    selector = {
        'fields': ['Id', 'LocationName', 'DisplayType', 'CanonicalName', 'ParentLocations', 'Reach', 'TargetingStatus'],
        'predicates': [{
            'field': 'Id',
            'operator': 'IN',
            'values': location_ids
        }, {
            'field': 'Locale',
            'operator': 'EQUALS',
            'values': ['en']
        }]
    }

    # Get Request
    response = service.get(selector)

    # Display results
    print(response)


def GetLocationIds(client, locations: List[Location]):

    # Map to LocationHelper class
    loc_results = LocationHelper.MapLocations(locations)

    # AdGroup service
    service = client.GetService('LocationCriterionService', version='v201809')

    # Results
    location_ids = []

    for location in loc_results:

        if location is None or location.name is None:
            pass

        selector = {
            'fields': ['Id', 'LocationName', 'DisplayType', 'CanonicalName', 'ParentLocations', 'Reach', 'TargetingStatus'],
            'predicates': [{
                'field': 'LocationName',
                'operator': 'EQUALS',
                'values': location.name
            }, {
                'field': 'Locale',
                'operator': 'EQUALS',
                'values': ['en']
            }]
        }

        # Get Request
        response = service.get(selector)

        result_locations = []
        # Get Locations
        for tmp in response:
            if 'location' in tmp and tmp['location']:
                result_locations.append(LocationHelper(
                    id=tmp['location']['id'],
                    name=tmp['location']['locationName'],
                    loc_type=tmp['location']['displayType']))

        # Display request
        # print("Request {}".format(location.toJSON()))

        # for res in result_locations:
        #    print(res.toJSON())

        # Get Best Score Location
        best_score_location = location.GetBestRatio(result_locations)

        # Add Location ID to array
        if best_score_location is not None:
            location_ids.append(best_score_location.id)

            # Display Results
            # print("Results {}".format(best_score_location.ToJson()))

    # Return results
    return location_ids


def LocationOperations(campaign_id, location_ids):

    # Location List
    location_list = [{
        'campaignId': campaign_id,
        'criterion': {
            'xsi_type': 'Location',
            'id': location_id,
        }
    }for location_id in location_ids]

    # Location operations
    operations = [{
        'operator': 'ADD',
        'operand': tmp
    }for tmp in location_list]

    return operations


def AddLocations(client, config, campaign_id, locations: List[Location]):

    try:
        location_ids = GetLocationIds(client, locations)

        # Get Campaign criterion service
        campaign_criterion_service = client.GetService('CampaignCriterionService', version='v201809')

        if len(location_ids) > 0:

            # Location operations
            operations = LocationOperations(campaign_id, location_ids)

            # Send request
            response = campaign_criterion_service.mutate(operations)['value']

            # Display results
            for lc in response:
                print('Location criteria with Id {} {} ({}) was added in Campaign with Id {}.'.format(
                    lc['criterion']['id'],
                    lc['criterion']['locationName'],
                    lc['criterion']['displayType'],
                    lc['campaignId']))

    except Exception as e:
        raise AdsException("AddLocations", e.args)
