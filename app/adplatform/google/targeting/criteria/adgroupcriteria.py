from ...helper.keyword import Keyword
from typing import List

PAGE_SIZE = 500


def GetAdGroupCriteria(client, ad_group_id: int) -> List[Keyword]:

    # Initialize appropriate service.
    servise = client.GetService('AdGroupCriterionService', version='v201809')

    # Construct selector and get all campaign targets.
    offset = 0
    selector = {
        'fields': ['AdGroupId', 'Id', 'CriteriaType', 'KeywordText'],
        'predicates': [
            {
                'field': 'AdGroupId',
                'operator': 'EQUALS',
                'values': [ad_group_id]
            },
            {
                'field': 'CriteriaType',
                'operator': 'IN',
                'values': ['KEYWORD']
            },
        ],
        'paging': {
            'startIndex': str(offset),
            'numberResults': str(PAGE_SIZE)
        }
    }

    results = servise.get(selector)

    keywords = []
    for res in results['entries']:
        if res['criterionUse'] == 'BIDDABLE':
            keywords.append(Keyword.GetKeyword(res['criterion']))

    return keywords
