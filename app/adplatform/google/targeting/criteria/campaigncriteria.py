from typing import List
# from .. import locations
from ...helper.location import Location as LocationHelper
from ...helper.language import Language as LanguageHelper

PAGE_SIZE = 500


def GetCampaignCriteria(client, config, campaign_id: int) -> [List[LocationHelper], List[LanguageHelper]]:

    # Initialize appropriate service.
    service = client.GetService('CampaignCriterionService', version='v201809')

    # Selector
    selector = {
        'fields': [
            'CampaignId', 'Id', 'CriteriaType', 'LanguageCode',
            'LanguageName', 'LocationName', 'DisplayType',
            'ParentCriterionId', 'ParentType', 'ParentLocations'],
        'predicates': [
            {
                'field': 'CampaignId',
                'operator': 'EQUALS',
                'values': [campaign_id]
            },
            {
                'field': 'CriteriaType',
                'operator': 'IN',
                'values': ['LANGUAGE', 'LOCATION']
            }
        ],
        'paging': {
            'startIndex': str(0),
            'numberResults': str(PAGE_SIZE)
        }
    }

    # Get Request
    response = service.get(selector)

    # print(response)

    # Value list
    langs = []
    locs = []

    # Location mapper
    # location_mapper = []

    for tmp in response["entries"]:
        tmp_type = tmp['criterion']['type']
        if tmp_type == 'LANGUAGE':
            langs.append(LanguageHelper.GetLanguage(tmp['criterion']))
        elif tmp_type == 'LOCATION':
            location = LocationHelper.GetLocation(tmp['criterion'])
            locs.append(location)
            # parent_ids = LocationHelper.GetParentIds(tmp['criterion'])
            # for parent_id in parent_ids:
            #     location_mapper.append({
            #         'id': location.id,
            #         'parentid': parent_id,
            #         'type': None,
            #         'name': None
            #     })

    # Get Ids
    # parent_ids = [x['parentid'] for x in location_mapper]

    # print(parent_ids)
    # locations.GetLocationDetails(client, parent_ids)

    return langs, locs
