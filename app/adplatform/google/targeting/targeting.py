from . import languages
from . import keywords
from . import radius
from . import user_interest


class Targeting(object):

    def __init__(self, client):
        self.client = client

    def AddKeywords(self, ad_group_id, keywords_list):
        keywords.AddKeywords(self.client, ad_group_id, keywords_list)

    def AddLanguages(self, campaign_id, languages_list):
        languages.AddLanguages(self.client, campaign_id, languages_list)

    def AddRadius(self, campaign_id, locs, locations_list):
        radius.AddRadius(self.client, campaign_id, locs, locations_list)

    def AddUserInterest(self, ad_group_id, user_interest_list):
        user_interest.AddUserInterest(self.client, ad_group_id, user_interest_list)
