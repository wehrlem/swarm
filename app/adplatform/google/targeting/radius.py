from typing import List
from app.models.data.locations import Location
from app.models.data.campaign_platform_mapper import GoogleEntityMapper
from app.models.data.common.configs import Status


def RadiusAddOperations(location, campaign_id):

    operation = {
        'operator': 'ADD',
        'operand': {
            'campaignId': campaign_id,
            'criterion': {
                'xsi_type': 'Proximity',
                'radiusDistanceUnits': 'KILOMETERS',
                'radiusInUnits': 50,
                'address': location
            }
        }
    }
    return operation


def RadiusRemoveOperations(location_id, campaign_id):

    operation = {
        'operator': 'REMOVE',
        'operand': {
            'campaignId': campaign_id,
            'criterion': {
                'id': location_id,
                'xsi_type': 'Proximity'
            }
        }
    }
    return operation


def AddRadius(client, campaign_id, locations: List[Location], loc_config: List[GoogleEntityMapper] = []):

    service = client.GetService('CampaignCriterionService', version='v201809')

    # Location dict
    # locs = {}
    # for loc in locations:
    #    locs[str(loc.place_id)] = loc

    # Get operations
    operations = []
    send_config = []
    for loc in loc_config:
        if loc.status == Status.REMOVE:
            operator = RadiusRemoveOperations(loc.google_id, campaign_id)
            operations.append(operator)
            send_config.append(loc)
        elif loc.status == Status.ADD:
            # operator = RadiusAddOperations(locs[str(loc.name)], campaign_id)
            operator = RadiusAddOperations(loc.name, campaign_id)
            operations.append(operator)
            send_config.append(loc)

    if len(operations) <= 0:
        print("Locations was up to date")
        return
    try:
        # Send request
        response = service.mutate(operations)['value']
        # Display results
        i = 0
        for lc in response:
            cfg = send_config[i]
            # Remove location
            if cfg.status == Status.REMOVE:
                loc_config.remove(cfg)
            # Set location to active
            elif cfg.status == Status.ADD:
                cfg.status = Status.ACTIVE
                cfg.google_id = lc['criterion']['id']
            print('Location criteria with Id {} Address: {} ({} {}) was {} Campaign with Id {}.'.format(
                lc['criterion']['id'],
                lc['criterion']['address']['streetAddress'],
                lc['criterion']['radiusInUnits'],
                lc['criterion']['radiusDistanceUnits'],
                cfg.status,
                lc['campaignId']))
            i += 1
    except Exception as e:
        for error in e.errors:
            index = None
            if 'fieldPathElements' in error:
                for element in error['fieldPathElements']:
                    if element['field'] == 'operations':
                        index = element['index']
            if index is not None and error['ApiError.Type'] == 'EntityNotFound':
                # send_config[index].status = Status.REMOVED
                loc_config.remove(send_config[index])
