from app.models.data.google import GoogleLang
from app.models.helper.adsexception import AdsException
from typing import List
from app.models.data.campaign_platform_mapper import GoogleLocationMapper
from app.models.data.common.configs import Status

error_reasons = ['INVALID_CRITERION_ID']


def GetLanguages(client) -> List[GoogleLang]:

    # Initialize appropriate service.
    service = client.GetService('ConstantDataService', version='v201809')

    # Send request
    response = service.getLanguageCriterion()

    languages = []
    # Map results
    for res in response:
        lang = GoogleLang()
        lang.code = res['code']
        lang.name = res['name']
        lang.lang_id = res['id']
        languages.append(lang)

    # langs = [Language(id=value['id'], code=value['code'], name=value['name']) for value in languages]

    # Display results.
    # for lang in languages:
    #    print('Language with name "{}" and ID "{}" was found.'.format(lang.name, lang.lang_id))

    return languages


def LanguageOperations(campaing_id, languages: List[GoogleLocationMapper]):

    operations = []
    send_languages = []

    for lang in languages:
        if lang.status == Status.ADD:
            # Add to list
            send_languages.append(lang)
            # Add to operations
            operations.append({
                'operator': 'ADD',
                'operand': {
                    'campaignId': campaing_id,
                    'criterion': {
                        'xsi_type': 'Language',
                        'id': lang.entity.lang_id
                    }
                }
            })
        elif lang.status == Status.REMOVE:
            # Add to list
            send_languages.append(lang)
            # Add to operations
            operations.append({
                'operator': 'REMOVE',
                'operand': {
                    'campaignId': campaing_id,
                    'criterion': {
                        'xsi_type': 'Language',
                        'id': lang.entity.lang_id
                    }
                }
            })

    return operations, send_languages


def AddLanguages(client, campaing_id, langs: List[GoogleLocationMapper] = []):

    try:
        # AdGroup service
        service = client.GetService('CampaignCriterionService', version='v201809')

        operations, send_languages = LanguageOperations(campaing_id, langs)

        if len(operations) > 0:

            # Send request
            response = service.mutate(operations)['value']

            # Display results
            i = 0
            for x in response:
                # Lang Id
                id = x['criterion']['id']

                # Update language status
                if send_languages[i].status == Status.REMOVE:
                    langs.remove(send_languages[i])
                elif send_languages[i].status == Status.ADD:
                    send_languages[i].status = Status.ACTIVE

                # Display message
                print('Language criteria with Id {} and name "{} ({})" was {}, Campaign with Id {}'.format(
                    x['criterion']['id'],
                    x['criterion']['name'],
                    x['criterion']['code'],
                    send_languages[i].status,
                    x['campaignId']
                ))
                i += 1
        else:
            print("Language was up to date")

    except Exception as e:
        # Remove language from list if error was occured
        for error in e.errors:
            index = None
            if 'fieldPathElements' in error:
                for element in error['fieldPathElements']:
                    if element['field'] == 'operations':
                        index = element['index']
            if index is not None and error['reason'] in error_reasons:
                langs.remove(send_languages[index])
