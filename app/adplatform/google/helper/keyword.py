from difflib import SequenceMatcher
from app.util.json import ToJson


class Keyword(ToJson):

    # @classmethod
    # def GetKeyword(cls, value):
    #    return Keyword(id=value['id'], text=value['text'], match_type=value['matchType'])

    def __init__(self, id=None, text=None, match_type=None):
        self.id = id
        self.text = text
        self.matchType = match_type

    def Ratio(self, keyword) -> float:
        if(keyword is not None and self.text is not None):
            return SequenceMatcher(None, self.text, keyword).ratio()
        else:
            return float(0)
