from difflib import SequenceMatcher
import operator
from app.util.json import ToJson

MIN_SIMILARITY_INDEX = 0.8


class Language(ToJson):

    def __init__(self, id=None, code=None, name=None):
        self.id = id
        self.code = code
        self.name = name

    def Ratio(self, language):
        if(language.name is not None and self.name is not None):
            return SequenceMatcher(None, self.name, language.name).ratio()
        else:
            return float(0)

    def GetBestRatio(self, languages):
        results = []
        for language in languages:
            ratio = self.Ratio(language)
            results.append(ratio)
        max_index, max_value = max(enumerate(results), key=operator.itemgetter(1))
        # print("Scores {} ".format(results))
        # print("Max Index {}, Value {}".format(max_index, max_value))
        if max_value > MIN_SIMILARITY_INDEX:
            return languages[max_index]
        else:
            return None
