from difflib import SequenceMatcher
import operator
from statistics import mean
from app.util.json import ToJson
from ....models.requests.location import Location as LocationRequest
from typing import List


class Location(ToJson):
    @classmethod
    def MapLocations(cls, locations: List[LocationRequest]):
        locs: List[Location] = []
        loc_types_priority = ['locality', 'country', 'postal_code']
        for location in locations:
            z = False
            for loc_type in loc_types_priority:
                for x in location.address_components:
                    if loc_type in x.types:
                        locs.append(Location(name=x.long_name, loc_type=loc_type))
                        z = True
                if z is True:
                    break
        return locs

    def __init__(self, id=None, name=None, loc_type=None):
        self.id = id
        self.name = name
        self.type = loc_type

    def Ratio(self, location):
        results = []
        if self.name is not None and location.name is not None:
            results.append(SequenceMatcher(None, self.name, location.name).ratio())
        else:
            results.append(0)
        if self.type is not None and location.type is not None:
            results.append(SequenceMatcher(None, self.type, location.type).ratio())
        else:
            results.append(0)
        return round(mean(results), 2)

    def GetBestRatio(self, locations):
        results = []
        for location in locations:
            ratio = self.Ratio(location)
            results.append(ratio)
        max_index, max_value = max(enumerate(results), key=operator.itemgetter(1))
        # print("Scores {} ".format(results))
        # print("Max Index {}, Value {}".format(max_index, max_value))
        return locations[max_index]
