from app.adplatform.google.google_base import GoogleBase
from app.models.helper.adsexception import AdsException

# Google Ads
from .basic.campaign import Campaign
from .basic.group import AdGroup
from .basic.budget import Budget
from .targeting import Targeting
from .ads import Ads
from . import statistics
from .google_update_targeting import update_targeting

# Campaing request
from app.models.data.campaigns import Campaign as CampaignRequest
from app.models.data.statistic import Statistic
from app.models.data.config import Config
from app.models.data.common.configs import GoogleAdType
from app.models.data.cp_config import GoogleCPConfig


class GoogleAdCreator(GoogleBase):

    def __init__(self, config: Config):

        super().__init__(config)

        # Basic component
        self.budget = Budget(self.client)
        self.campaign = Campaign(self.client)
        self.ad_group = AdGroup(self.client)
        self.targeting = Targeting(self.client)
        self.ads = Ads(self.client)

    def CreateTextAds(self, request: CampaignRequest, new_camp: bool):

        # Filter search ads
        ads = [ad for ad in request.googleads if ad.type == 'search']

        # Get Google Search Ads
        google_mapper = request.campaign_platform_mapper.google
        ad_cfg = next(x for x in google_mapper.ads if x.type == GoogleAdType.SEARCH)

        # Upsert Budget
        if ad_cfg.budget_id is None:
            ad_cfg.budget_id = self.budget.AddBudget(request)
        else:
            self.budget.UpdateBudget(request, ad_cfg.budget_id)

        # Upsert Campaign
        if ad_cfg.campaign_id is None:
            ad_cfg.campaign_id = self.campaign.AddCampaign(request, ad_cfg.budget_id)
        else:
            self.campaign.UpdateCampaign(request, ad_cfg.campaign_id)

        # AdGroup
        if ad_cfg.ad_group_id is None:
            ad_cfg.ad_group_id = self.ad_group.AddAdGroup(request, ad_cfg.campaign_id)
        else:
            self.ad_group.UpdateAdGroup(request, ad_cfg.ad_group_id)

        # TextAds
        if len(ads) > 0:
            if ad_cfg.ad_id is None:
                ad_cfg.ad_id = self.ads.AddTextAds(ads[0], ad_cfg.ad_group_id)
            else:
                self.ads.UpdateTextAds(ads[0], ad_cfg.ad_id)

        # Targeting
        if request.targeting is not None:

            # Update targeting
            update_targeting(request)

            if google_mapper.keywords is not None:
                self.targeting.AddKeywords(ad_cfg.ad_group_id, google_mapper.keywords)

            # Languages
            if google_mapper.languages is not None:
                self.targeting.AddLanguages(ad_cfg.campaign_id, google_mapper.languages)

            # Locations
            if google_mapper.locations is not None:
                self.targeting.AddRadius(ad_cfg.campaign_id, request.targeting.locations, google_mapper.locations)

            # Add interest
            if google_mapper.interest is not None:
                self.targeting.AddUserInterest(ad_cfg.ad_group_id, google_mapper.interest)

    # Start Campaign
    def StartCampaign(self, campaign_id):
        if campaign_id is not None:
            self.campaign.RunCampaign(campaign_id)

    def StopCampaign(self, campaign_id):
        if campaign_id is not None:
            self.campaign.StopCampaign(campaign_id)

    # Get Statistic by Campaign Id
    def GetCampaignStatistic(self, camp: CampaignRequest, stat: Statistic):

        # Get first campaign id
        ad = camp.campaign_platform_mapper.google.ads.first()

        if ad is not None and ad.campaign_id is not None:
            statistics.GetCampaignStatistics(self.client, ad.campaign_id, stat)
