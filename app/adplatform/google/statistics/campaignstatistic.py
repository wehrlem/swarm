from app.models.data.statistic import Statistic


def GetCampaignStatistics(client, campaign_id, stat: Statistic):

    report_downloader = client.GetReportDownloader(version='v201809')

    # Create report definition.
    report = {
        'reportName': 'Last 7 days CAMPAIGN_PERFORMANCE_REPORT',
        'dateRangeType': 'ALL_TIME',
        'reportType': 'CAMPAIGN_PERFORMANCE_REPORT',
        'downloadFormat': 'CSV',
        'selector': {
            'fields': [
                'CampaignId',
                'CampaignName',
                'CampaignStatus',
                'Clicks',
                'Impressions',
                'AverageCpc',
                'Cost'],
            'predicates': [
                {
                    'field': 'CampaignId',
                    'operator': 'EQUALS',
                    'values': [campaign_id],
                }
            ]
        }
    }

    # Get report
    results = report_downloader.DownloadReportAsString(
        report, skip_report_header=True, skip_column_header=True,
        skip_report_summary=True, include_zero_impressions=True)

    results = results.rstrip().split(',')

    data = {
        'campaignid': results[0],
        'name': results[1],
        'status': results[2],
        'clicks': results[3],
        'impressions': results[4],
        'averagecpc': results[5],
        'cost': results[6],
    }

    stat.google.clicks = int(data['clicks'])
    stat.google.impressions = int(data['impressions'])
    stat.google.averagecpc = float(data['averagecpc'])
    stat.google.cost = float(data['cost'])
