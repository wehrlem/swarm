from app.models.data.campaigns import Campaign
from app.models.data.cp_config import LocationConfig, LanguageConfig, KeywordConfig, UserInterestConfig
from app.models.data.google import GoogleLang
from app.models.data.campaign_platform_mapper import GoogleEntityMapper, GoogleLocationMapper
from app.models.data.locations import Location
from app.models.data.common.configs import Status
from app.database.google_language import GetGoogleLangId
from typing import List
# from app.models.data.common.configs import Platforms


def update_targeting(camp: Campaign):

    __keywords(camp)
    __language(camp)
    __location(camp)
    __interest(camp)


def __keywords(camp: Campaign):

    # User defined keywords
    new_keywords = camp.targeting.keywords

    # Target set keywords
    new_target_set_keywords = camp.campaign_settings.google.target_set.keywords
    
    new = set(new_keywords).union(new_target_set_keywords)

    # source
    source = camp.campaign_platform_mapper.google.keywords
    items = update_google_entity(new, source)

    # Add new items
    for item in items:
        tmp = GoogleEntityMapper()
        tmp.name = item
        tmp.status = Status.ADD
        source.append(tmp)


def __language(camp: Campaign):

    new_languages = camp.targeting.languages
    source = camp.campaign_platform_mapper.google.languages
    items = update_google_entity(new_languages, source)

    if len(items) > 0:
        glangs = GoogleLang.objects.all()
        for item in items:
            glang = GetGoogleLangId(item, glangs)
            if glang is not None:
                tmp = GoogleEntityMapper()
                tmp.name = item
                tmp.entity = glang
                tmp.status = Status.ADD
                source.append(tmp)


def __location(camp: Campaign):

    new = camp.targeting.locations
    source = camp.campaign_platform_mapper.google.locations
    update_google_location(new, source)


def __interest(camp: Campaign):

    new = []
    map_interest = {}
    for x in camp.campaign_settings.google.target_set.interest:
        new.append(x.name)
        map_interest[x.name] = x

    # new = camp.campaign_settings.google.target_set.interest
    source = camp.campaign_platform_mapper.google.interest
    items = update_google_entity(new, source)

    # Add new items
    for item in items:
        tmp = GoogleEntityMapper()
        tmp.name = item
        tmp.entity = map_interest[item]
        tmp.status = Status.ADD
        source.append(tmp)


def update_google_entity(new_data, source: List[GoogleEntityMapper]):

    items_to_add = []
    for x in new_data:
        tmp = source.filter(name=x)
        if len(tmp) > 0:
            # print("{} - {} - {}".format(x, tmp.first().name, tmp.first().status))
            if tmp.first().status != Status.ACTIVE:
                tmp.first().status = Status.ADD
        else:
            items_to_add.append(x)
    # Remove
    for x in source:
        if x.name not in new_data:
            x.status = Status.REMOVE

    return items_to_add


def update_google_location(new_data: List[Location], source: List[GoogleEntityMapper]):

    for x in new_data:
        tmp = source.filter(name=x.formatted_address)
        if len(tmp) > 0:
            if tmp.first().status != Status.ACTIVE:
                tmp.first().status = Status.ADD
        else:
            tmp = GoogleEntityMapper()
            tmp.name = x.formatted_address
            tmp.status = Status.ADD
            source.append(tmp)

    # Remove
    for x in source:
        if len(new_data.filter(formatted_address=x.name)) == 0:
            x.status = Status.REMOVE
