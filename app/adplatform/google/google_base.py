import yaml
from googleads import adwords
from app.models.data.config import Config


class GoogleBase(object):

    def __init__(self, config: Config):

        # Config file
        cfg = {
            'adwords': {
                "developer_token": config.google.developer_token,
                "client_customer_id": config.google.client_customer_id,
                "client_id": config.google.client_id,
                "client_secret": config.google.client_secret,
                "refresh_token": config.google.refresh_token
            }
        }

        # Google AdWords client
        self.client = adwords.AdWordsClient.LoadFromString(yaml.dump(cfg))
