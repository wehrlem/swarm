from .google_adcreator import BaseGoogleAdCreator
from .ads.textad import TexAdOperations
from app.adplatform.google.basic.budget import Budget
from app.adplatform.google.basic.campaign import Campaign
from app.adplatform.google.basic.group import AdGroup

import random
import time
from urllib.request import urlopen


PENDING_STATUSES = ('ACTIVE', 'AWAITING_FILE', 'CANCELING')
MAX_POLL_ATTEMPTS = 5


class GoogleBatchAdCreator(BaseGoogleAdCreator):

    def CreateBatchJob(self):
        # Initialize appropriate service.
        batch_job_service = self.client.GetService('BatchJobService', version='v201809')
        # Create a BatchJob.
        batch_job_operations = [{
            'operand': {},
            'operator': 'ADD'
        }]
        # Send request
        batch_job = batch_job_service.mutate(batch_job_operations)['value'][0]
        # Retrieve the URL used to upload the BatchJob operations.
        upload_url = batch_job['uploadUrl']['url']
        batch_job_id = batch_job['id']
        # print('Created BatchJob with ID "%d", status "%s", and upload URL "%s"' % (
        #     batch_job['id'], batch_job['status'], batch_job['uploadUrl']['url']))
        print('Created BatchJob with ID "%d", status "%s"' % (batch_job['id'], batch_job['status'] ))
        return batch_job_id, upload_url

    def GetBatchJob(self, batch_job_id):
        # BatchJob service
        batch_job_service = self.client.GetService('BatchJobService', 'v201809')
        # Selector
        selector = {
            'fields': ['Id', 'Status', 'DownloadUrl'],
            'predicates': [
                {
                    'field': 'Id',
                    'operator': 'EQUALS',
                    'values': [batch_job_id]
                }
            ]
        }
        results = batch_job_service.get(selector)
        # print(results)
        return results['entries'][0]
        # return batch_job_service.get(selector)['entries'][0]

    def CancelBatchJob(self, batch_job, max_poll_attempts=MAX_POLL_ATTEMPTS):
        batch_job_service = self.client.GetService('BatchJobService', 'v201809')
        batch_job['status'] = 'CANCELING'

        operation = {
            'operator': 'SET',
            'operand': batch_job
        }

        batch_job_service.mutate([operation])

        # Verify that the Batch Job cancels.
        poll_attempt = 0

        while (poll_attempt in range(max_poll_attempts) and batch_job['status'] != 'CANCELED'):
            sleep_interval = (30 * (2 ** poll_attempt) + (random.randint(0, 10000) / 1000))
            print('Batch Job not finished canceling, sleeping for %s seconds.' % sleep_interval)
            time.sleep(sleep_interval)
            batch_job = self.GetBatchJob(batch_job['id'])
            poll_attempt += 1

        if batch_job['status'] == 'CANCELED':
            print('Batch Job with ID "%d" has been successfully canceled.' % batch_job['id'])
        else:
            print('Batch Job with ID "%d" failed to cancel after polling %d times.' % (batch_job['id'], max_poll_attempts))

    def GetBatchJobDownloadUrlWhenReady(self, batch_job_id, max_poll_attempts=MAX_POLL_ATTEMPTS):

        batch_job = self.GetBatchJob(batch_job_id)

        if batch_job['status'] == 'CANCELED':
            raise Exception('Batch Job with ID "%s" was canceled before completing.' % batch_job_id)

        poll_attempt = 0

        while (poll_attempt in range(max_poll_attempts) and batch_job['status'] in PENDING_STATUSES):
            # sleep_interval = (30 * (2 ** poll_attempt) + (random.randint(0, 10000) / 1000))
            sleep_interval = 10
            print('Batch Job not ready, sleeping for %s seconds.' % sleep_interval)
            time.sleep(sleep_interval)
            batch_job = self.GetBatchJob(batch_job_id)
            poll_attempt += 1

            if 'downloadUrl' in batch_job and batch_job['downloadUrl'] is not None:
                url = batch_job['downloadUrl']['url']
                print('Batch Job with Id "%s", Status "%s", and DownloadUrl "%s" ready.' % (batch_job['id'], batch_job['status'], url))
                return url

        print('BatchJob with ID "%s" is being canceled because it was in a pending state after polling %d times.' % (batch_job_id, max_poll_attempts))
        self.CancelBatchJob(batch_job)

    def CreateBatchJobHelper(self):
        return self.client.GetBatchJobHelper(version='v201809')

    def CreateAds(self, request):
        # Initialize BatchJobHelper.
        batch_job_helper = self.CreateBatchJobHelper()
        # Create Batch Job
        # batch_job_id, upload_url = self.CreateBatchJob()

        upload_url = "https://batch-job-upload-prod-ebe9b43.storage.googleapis.com/347272690/2019603311.operations.xml?GoogleAccessId=926043054183-rbu1gkdushnqrfcf47gser4u8qse2ig4@developer.gserviceaccount.com&Expires=1571670414&Signature=XH2tf6jVJ5XQs5bv2rYgZ9EnTQuj5Y6EtUNDur76ICPZVS5z28FFcrz0nPM24d0vWJauK8ARyL2qH0syg6%2BiTch8mUM%2BF8xSiz3wYVDbFsrqplGYR794JY99GxhCxEgnjR8SSwWag8%2FFgjzI%2FYzJsvqKXfSUXmZcW5F9k2zofoCIp%2FOXFNyau1DJHe%2FnIYyOSy0AZ2yMk3xfQYAbjDVqtCxcSK7snh11b%2BIl%2FLTCNJvP34AoZEOZPuaKVG1y1PwuMYsAOseXLQe7tQdMBw1qWOirB4egjU%2F2%2FFfjDSC61rIOTsNT5oY49pwnOoi3qmfei3Sm7Mt3dpn0afq7l98Ivw%3D%3D"
        batch_job_id = "2019223666"

        batch_job = self.GetBatchJob(batch_job_id)
        # print(batch_job)
        download_url = batch_job['downloadUrl']['url']
        # print(download_url)

        # Budget operations
        budget_id = batch_job_helper.GetId()
        budget = Budget.BudgetOperation(request)
        budget[0]['operand']['budgetId'] = budget_id

        # Campaign operations
        campaign_id = batch_job_helper.GetId()
        campaign = Campaign.CampaignCreateOperation(request, budget_id)
        campaign[0]['operand']['id'] = campaign_id

        # AdGroup
        # ad_group_id = batch_job_helper.GetId()
        # ad_group = AdGroup.AdGroupOperations(campaign_id, self.config)
        # ad_group[0]['operand']['id'] = ad_group_id

        # Upload operations
        # batch_job_helper.UploadOperations(upload_url, budget, campaign)

        # self.GetBatchJob(batch_job_id)

        # Download URL
        # download_url = self.GetBatchJobDownloadUrlWhenReady(batch_job_id)

        response_xml = urlopen(download_url,).read()
        response = batch_job_helper.ParseResponse(response_xml)

        print(response)
        if 'rval' in response['mutateResponse']:
            for data in response['mutateResponse']['rval']:
                if 'errorList' in data:
                    print('Operation %s - FAILURE:' % data['index'])
                    print('\terrorType=%s' % data['errorList']['errors']['ApiError.Type'])
                    print('\ttrigger=%s' % data['errorList']['errors']['trigger'])
                    print('\terrorString=%s' % data['errorList']['errors']['errorString'])
                    print('\tfieldPath=%s' % data['errorList']['errors']['fieldPath'])
                    print('\treason=%s' % data['errorList']['errors']['reason'])
                if 'result' in data:
                    print('Operation %s - SUCCESS.' % data['index'])
