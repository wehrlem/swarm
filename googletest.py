import json
from time import time

# Db service
import app.models.data.common.mongo_setup as mongo_setup
import app.database as db
# from app.services.ad_creator import AdCreator
# from app.adplatform.google.google_communicator import GoogleCommunicator
import app.adplatform.google.targeting.user_interest as interest
# from app.adplatform.google.google_update_targeting import update_targeting
# from app.models.data.google import GoogleInterest
from app.models.data.accounts import Account
# from app.models.requests.property_schema import Property
# from app.models.data.apartment import Apartment
from app.models.data.campaigns import Campaign
# from app.database.campaign_settings import get_default_settings
# import app.adplatform.swissrets.swisrets as swiss
# from app.models.requests.campaign_schema import AdvertisementSchema, AdvertisementListSchema
# from app.adplatform.swissrets.swisrets import SwissRetsCommunicator
# import xml.dom.minidom as minidom
# import simplejson
from datetime import datetime
from marshmallow import pprint
import sys


def string_compare():

    lang = ["English", "Spanish", "Bosanski"]
    new_lang = ["English", "Bosanski"]

    delete_items = list(set(lang) - set(new_lang))
    add_items = list(set(new_lang) - set(lang))

    print(delete_items)
    print(add_items)


def test_token():

    # db.users.insert_test_account()
    usr = db.users.find_user_by_token('tdE608T07rzbDdG7uyOGtHrz9hmtbyd_Nr2cmXDlRVo')
    print(usr.to_json())


def create_google_campaign():

    # Read sample data
    # with open('app/models/requests/sampleRequest.json') as json_file:
    #     data = json.load(json_file)

    # Upsert campaign
    # request = db.campaign.upsert_campaign(data)

    # Google Ad Creator
    # google = GoogleAdCreator()
    # google.CreateTextAds(request)

    # Update Config
    # db.cp_config.update(request.config)
    pass


def get_user():
    # Get user
    user = db.account.find_user_by_token("EJ1bJhluCnWhiDX5-o9ao11WU6YGFP0PxCw-FRZWGTM")
    return user


def create_campaign():

    with open('app/models/requests/sampleRequest.json') as json_file:
        data = json.load(json_file)

    user = get_user()

    # get default config
    # config = db.config.get_default_config()

    # Create campaign
    if user is not None:
        creator = AdCreator()
        creator.CreateAds(data, user)
    else:
        print("User not exist")


def get_statistic(camp_id):

    try:
        # get default config
        # config = db.config.get_default_config()
        com = AdCreator()
        tmp = com.GetStatistic(camp_id)
        if tmp is not None:
            print(tmp.ToJson())
    except Exception as ex:
        print(ex)
        pass


def user_interese():

    google = GoogleCommunicator()
    # google.GetUserInterest()
    interest.AddUserInterest(google.client, 84262261595, [80155, 80161])


def get_campaigns():

    user = get_user()

    campaigns = db.campaign.get_campaigns(user)

    for x in campaigns:
        print(x)


def print_xml():

    # user = db.account.find_user_by_email("test@gmail.com")

    # all_camp = Campaign.objects.all().first()

    # # Create schema
    # ad_schema = AdvertisementSchema(strict=True, many=False)
    # res = ad_schema.dump(all_camp)

    # # Connect to newHome
    # connection_string = db.account.get_platform_connection_string(user, "newhome")
    # swiss_conn = swiss.SwissRetsCommunicator(connection_string)
    # xml_data = swiss_conn.campaign_to_xml(res.data)

    # print(xml_data)
    pass


def send_to_newhome():

    user = db.account.find_user_by_email("test@gmail.com")

    # Get all data
    all_camp = Campaign.objects.all()

    # Create schema
    # from app.models.requests.campaign_schema import CampaignSchema
    # ad_schema = CampaignSchema(strict=True, many=True)
    # res = ad_schema.dump(all_camp)

    # print(res.data)

    # Connect to newHome
    connection_string = db.account.get_platform_connection_string(user, "newhome")
    swiss_conn = swiss.SwissRetsCommunicator(connection_string)
    xml_data = swiss_conn.getExport(all_camp)
    swiss_conn.push(xml_data)
    # print(xml_data)


def validate_propery():

    with open('app/models/requests/advertisement.json') as json_file:
        data = json.load(json_file)

    # Create property from JSON
    # prop = Property.from_json(json.dumps(data), False)
    # print(prop.address.locality)
    # prop = Property(**data)

    # print(data[])

    # prop.save()

    # prop.validate()


def create_advertisement():

    from app.models.requests.advertisement_schema import AdvertisementSchema
    from app.models.requests.campaign_schema import CampaignSchema

    with open('app/models/requests/advertisement.json') as json_file:
        data = json.load(json_file)

    # Addount
    acc = Account.objects.all().first()

    # pprint(data)

    # Load Advertisement
    ad_ser = AdvertisementSchema(strict=True)
    camps, _ = ad_ser.load(data)

    for x in camps:
        x.account_id = acc
        x.save()

    # Dump Campaigns
    camp_ser = CampaignSchema(strict=True, many=True)
    camp, _ = camp_ser.dump(camps)
    pprint(camp)

    # camp, _ = advertisement.load(data)

    # Save
    #camp.account_id = acc
    #camp.save()

    # Dump campaign
    #res_data, _ = AdvertisementSchema().dump(camp)
    #pprint(res_data, indent=2)


def update_advertisement():

    with open('app/models/requests/advertisement-min.json') as json_file:
        data = json.load(json_file)

    camp_id = '5eb9445f0aa13ae31b591b2c'

    # Load data
    advertisement = AdvertisementSchema(strict=True)
    camp, _ = advertisement.load(data)

    new_data, _ = AdvertisementSchema(strict=True).dump(camp)

    old_camp = Campaign.objects(id=camp_id).first()
    if old_camp is not None:
        res = AdvertisementSchema(strict=True).update(old_camp, new_data)
        res.data.update_date = datetime.utcnow()
        assert res.data is old_camp
        old_camp.save()
        res_data, _ = AdvertisementSchema().dump(old_camp)
        pprint(res_data, indent=2)
    else:
        print("Campaign not exists")


def get_advertisement_all():

    with open('app/models/requests/advertisement.json') as json_file:
        data = json.load(json_file)

    camp = Campaign.objects.all()

    camp_data, _ = AdvertisementListSchema(many=True).dump(camp)
    pprint(camp_data, indent=2)


def get_advertisement():

    with open('app/models/requests/advertisement.json') as json_file:
        data = json.load(json_file)

    camp_id = "5eb2a5d6dfdad9af99f80b0a"

    camp = Campaign.objects.filter(id=camp_id).first()

    camp_data, _ = AdvertisementListSchema(many=False).dump(camp)
    pprint(camp_data, indent=2)


def idx_test(platform_name, apartment_id):

    from app.adplatform.idx.base_idx import IdxManager

    # Get user
    user = db.account.find_user_by_email("test@gmail.com")

    print(apartment_id)
    # Get active campaign
    active_camp = Campaign.objects(account_id=user).filter(apartment__reference_id=apartment_id)

    standard, config = user.getPlatformConfig(platform_name)

    # print(standard, config.to_json())
    manager = IdxManager(config, platform_name)
    records, camp_json = manager.getRecords(active_camp)

    pprint(camp_json)
    # if manager.push(records):
    #     pprint(camp_json)


def test():

    from mongoengine.queryset.visitor import Q
    from app.adplatform.idx.base_idx import IdxManager

    user = db.account.find_user_by_email("test@gmail.com")

    # Get active campaign
    camps = Campaign.objects(account_id=user).get_published()

    # Seperate campaigns by platform
    campaigns_map = {}
    for x in camps:
        for p in x.platforms:
            if p.name not in campaigns_map.keys():
                campaigns_map[p.name] = []
            campaigns_map[p.name].append(x)

    # Send campaign by platform
    for platform, campaigns in campaigns_map.items():
        standard = user.getStandard(platform)
        if standard == "idx":
            print("Sending {} campaigns to: '{}', via: '{}'".format(len(campaigns), platform, standard))
            config = user.getPlatformConfig(platform)
            manager = IdxManager(config)
            records = manager.getRecords(campaigns)
            manager.push(records)


def get_active_campaign():

    from app.models.requests.advertisement_schema import AdvertisementListSchema

    user = db.account.find_user_by_email("test@gmail.com")

    # Get active campaign
    camps = Campaign.objects(account_id=user).get_published()

    serializer = AdvertisementListSchema(strict=True, many=True)
    rez = serializer.dump(camps)

    pprint(rez.data)


def publish_ads():

    from app.adplatform.ad_manager import AdPlatformManager

    user = db.account.find_user_by_email("test@gmail.com")

    ad_manager = AdPlatformManager()
    ad_manager.publish(user)


def req_test():
    import requests
    HOST = "localhost"
    PORT = "5000"
    TOKEN = "AIeIc2_vM4gkR2Wocc362I2xvaqwMa2LyflbZ_oqiCY"

    headers = {'Authorization': 'Token {}'.format(TOKEN)}

    r = requests.get("http://{}:{}/advertisements/14".format(HOST, PORT), headers=headers)

    pprint(r.json())


def last():
    user = db.account.find_user_by_email("test@gmail.com")
    # Get active campaign
    x = Campaign.objects.get_last_campaign(user, "4")

    print("{} {}".format(x.id, x.number_active_platforms))

    # print("{} {} {} {}".format(x.id, x.create_date, x.apartment.reference_id, x.number_active_platforms))


def images():

    user = db.account.find_user_by_email("test@gmail.com")
    camps = Campaign.objects(account_id=user).get_published()

    # from app.adplatform.idx.base_idx import IdxManager
    # _, conf = user.getPlatformConfig("homegate")
    # manager = IdxManager(conf, "homegate")
    # manager.pushImages(camps)

    ids = []
    for x in camps:
        ids.append(str(x.id))

    from app.adplatform.ad_manager import AdPlatformManager
    manager = AdPlatformManager()
    manager.publishImages(user, ids)


def platform(platform_name):

    user = db.account.find_user_by_email("test@gmail.com")
    # camps = Campaign.objects(account_id=user).get_published()

    from app.adplatform.ad_manager import AdPlatformManager
    manager = AdPlatformManager()
    manager.publish_to_platform(user, platform_name)

    # from app.models.data.account_platform_settings import BasePlatformConfig

    # for x in user.account_platform_settings:
    #     tmp = getattr(user.account_platform_settings, x)
    #     if type(tmp) == BasePlatformConfig:
    #         if tmp.active is True:
    #             print("test --")


def img_socket():

    user = db.account.find_user_by_email("test@gmail.com")
    camps = Campaign.objects(account_id=user).get_published()

    from app.image_upload_service.pub import img_pub

    img_pub.send(user, camps)


def count():
    from mongoengine import Q

    campaigns = {
        "miete": ["4", "32432"],
        "dev": ["54", "32432"],
        "mimo": ["54", "32432"]
    }
    # filters = None
    # for project_name, campaign_ids in campaigns.items():
    #     if filters is None:
    #         filters = (Q(project_id=project_name) & Q(apartment__reference_id__in=campaign_ids))
    #     else:
    #         filters = filters | (Q(project_id=project_name) & Q(apartment__reference_id__in=campaign_ids))
    # camps = Campaign.objects.filter(filters)

    camps = Campaign.objects.get_campaigns({"miete": ["2"]}).get_published()
    # camps = Campaign.objects.get_published()

    for x in camps:
        print("id:{}, project:{}, apartment_id: {}, active: {}".format(x.id, x.project_id, x.apartment.reference_id, x.active_platforms_names))


if __name__ == '__main__':

    if len(sys.argv) == 1:
        sys.exit("Command not specified")

    # Init db
    mongo_setup.global_init()

    # db.update_user_token()
    start = time()

    if sys.argv[1] == "create":
        create_campaign()
    elif sys.argv[1] == "stat":
        if len(sys.argv) == 3:
            camp_id = sys.argv[2]
            get_statistic(camp_id)
        else:
            sys.exit("Campaign ID wasn't set")
    elif sys.argv[1] == "xml":
        print_xml()
    elif sys.argv[1] == "newhome":
        send_to_newhome()
    elif sys.argv[1] == "init":
        # Insert initial setup
        db.insert_initial_setup()
    elif sys.argv[1] == "prop":
        validate_propery()
    elif sys.argv[1] == "create_ad":
        create_advertisement()
    elif sys.argv[1] == "update_ad":
        update_advertisement()
    elif sys.argv[1] == "get_all":
        get_advertisement_all()
    elif sys.argv[1] == "get":
        get_advertisement()
    elif sys.argv[1] == "reset_token":
        db.reset_user_token()
    elif sys.argv[1] == "idx":
        if len(sys.argv) > 3:
            paltform = sys.argv[2]
            apartment_id = sys.argv[3]
            idx_test(paltform, apartment_id)
    elif sys.argv[1] == "test":
        test()
    elif sys.argv[1] == "get_ads":
        get_active_campaign()
    elif sys.argv[1] == "publish":
        publish_ads()
    elif sys.argv[1] == "request":
        req_test()
    elif sys.argv[1] == "last":
        last()
    elif sys.argv[1] == "img":
        images()
    elif sys.argv[1] == "platform":
        platform()
    elif sys.argv[1] == "socimage":
        img_socket()
    elif sys.argv[1] == "count":
        count()
    else:
        sys.exit("Command didn't recognized")

    end = time() - start
    print("Time {}".format(end))


#     db.getCollection('platform').find({}).forEach(function(a){
#     a.packages.forEach(function(b){
#         b.options.forEach(function(c){
#             c.duration = c.duraction;
#             delete c["duraction"];                   
#         });
#     });
#     db.getCollection('platform').save(a)  
# });