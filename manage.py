import sys
import argparse
from app import configure
from app.models.data.accounts import Account
from app.models.data.campaigns import Campaign
import app.models.data.common.mongo_setup as mongo_setup
from app.adplatform.ad_manager import AdPlatformManager
import app.database as db
from app.models.data.platforms import Platform


def publish_ads_with_images(username, platform_name=None, all_team=False):
    '''
        Publish ads with images
    '''

    user = Account.objects(username=username).first()

    if user.is_active:
        ad_manager = AdPlatformManager()
        if platform_name is None:
            print("Publishing ads with Images for user {}".format(user.email))
            ad_manager.publish(user, images=True, all_team=all_team)
        else:
            print("Publishing ads with Images for user {} to '{}'".format(user.email, platform_name))
            # Publish to platform
            ad_manager.publishToPlatform(user, platform_name, images=True, all_team=all_team)


def publish(username, platform_name=None, all_team=False):
    '''
        Publish ads only
    '''

    user = Account.objects(username=username).first()

    if user.is_active:

        ad_manager = AdPlatformManager()

        if platform_name is None:
            print("Publishing ads for user {}".format(user.email))
            ad_manager.publish(user, all_team=all_team)
        else:
            print("Publishing ads for user {} to '{}'".format(user.email, platform_name))
            ad_manager.publishToPlatform(user, platform_name, all_team=all_team)


def publish_images(username, campaigns):
    '''
        Publish images only
    '''

    user = Account.objects(username=username).first()
    active_platforms = list(Platform.objects.get_platforms())
    if user.is_active:
        ad_manager = AdPlatformManager()
        if campaigns:
            camp_queryset = Campaign.objects.filter(account_id=user, apartment__reference_id__in=campaigns).all()
        else:
            camp_queryset = Campaign.objects(account_id=user).get_published(active_platforms)
        ad_manager.publishImages(camp_queryset)


if __name__ == '__main__':

    # Create the parser
    my_parser = argparse.ArgumentParser(allow_abbrev=False)

    # Add the arguments
    my_parser.add_argument('publish', type=str)
    my_parser.add_argument('user', type=str)
    my_parser.add_argument('--platform', type=str, required=False)
    my_parser.add_argument('-all_teams', action='store_true')
    my_parser.add_argument('-i', action='store_true')
    my_parser.add_argument('-images', action='store_true')
    my_parser.add_argument('--campaigns', type=str, required=False, action="extend", nargs="+")

    args = my_parser.parse_args()

    configure()

    # Init db
    mongo_setup.global_init()

    if args.images:
        publish_images(args.user, args.campaigns)
    elif args.i:
        try:
            username = args.user
            platform_name = args.platform
            all_team = args.all_teams
            publish_ads_with_images(username, platform_name, all_team=all_team)
        except Exception as ex:
            print("Error {}".format(ex))

    elif args.i is False:
        try:
            username = args.user
            platform_name = args.platform
            all_team = args.all_teams
            publish(username, platform_name, all_team=all_team)
        except Exception as ex:
            print("Error {}".format(ex))

    elif sys.argv[1] == "init":
        db.insert_initial_setup()
