import os
from app import create_app

# import app.models.data.common.mongo_setup as mongo_setup

# Connect to DB
# mongo_setup.global_init()

# Create app
app = create_app()
