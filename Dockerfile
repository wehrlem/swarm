# Configuration
ARG PYTHON_VERSION=3.7
FROM python:${PYTHON_VERSION} as builder

# Install requirements
WORKDIR /build
COPY dev_requirements.txt /build/
COPY requirements.txt /build/
# RUN pip wheel -r dev_requirements.txt
RUN pip wheel -r requirements.txt

FROM python:${PYTHON_VERSION}

ARG USERNAME=appuser
ARG USER_UID=1000
ARG USER_GID=$USER_UID

# Configure apt and install packages
RUN apt-get update \
    && apt-get -y install --no-install-recommends apt-utils dialog 2>&1 \
    #
    # Verify git, process tools, lsb-release (common in install instructions for CLIs) installed
    && apt-get -y install git procps lsb-release \
    #
    # Install pylint
    && pip --disable-pip-version-check --no-cache-dir install pylint \
    #
    # Create a non-root user to use if preferred - see https://aka.ms/vscode-remote/containers/non-root-user.
    && groupadd --gid $USER_GID $USERNAME \
    && useradd -s /bin/bash --uid $USER_UID --gid $USER_GID -m $USERNAME \
    # [Optional] Add sudo support for non-root user
    && apt-get install -y sudo \
    && echo $USERNAME ALL=\(root\) NOPASSWD:ALL > /etc/sudoers.d/$USERNAME \
    && chmod 0440 /etc/sudoers.d/$USERNAME \
    #
    # Clean up
    && apt-get autoremove -y \
    && apt-get clean -y \
    && rm -rf /var/lib/apt/lists/* 

# Copy from builder
COPY --from=builder /build /build
RUN pip install -r /build/requirements.txt \
                -f /build \
                && rm -rf /build \
                && rm -rf /root/.cache/pip

WORKDIR /workspaces/swarm
USER appuser

#CMD ["gunicorn", "-b", "0.0.0.0:8000", "danijel:app"]
#ENTRYPOINT ["python", "run.py"]